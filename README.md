# Navigation

## Setup

This package uses submodules for some dependencies. Therefore, after cloning, also pull the submodules via:

```bash
cd .../navigation/
git submodule init
git submodule update
```
