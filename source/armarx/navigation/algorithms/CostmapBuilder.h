/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/VirtualRobot.h>

#include <armarx/navigation/algorithms/Costmap.h>

namespace armarx::navigation::algorithms
{

    class CostmapBuilder
    {
    public:
        CostmapBuilder(const VirtualRobot::RobotPtr& robot,
                       const VirtualRobot::SceneObjectSetPtr& obstacles,
                       const Costmap::Parameters& parameters,
                       const std::string& robotCollisonModelName);

        [[nodiscard]] Costmap create();

        static Eigen::MatrixXf createUniformGrid(const SceneBounds& sceneBounds,
                                                 const Costmap::Parameters& parameters);

    private:
        float computeCost(const Costmap::Position& position,
                          VirtualRobot::RobotPtr& collisionRobot,
                          const VirtualRobot::CollisionModelPtr& robotCollisionModel);

        void fillGridCosts(Costmap& costmap);

        void initializeMask(Costmap& costmap);

        const VirtualRobot::RobotPtr robot;
        const VirtualRobot::SceneObjectSetPtr obstacles;
        const Costmap::Parameters parameters;
        const std::string robotCollisionModelName;
    };

} // namespace armarx::navigation::algorithms
