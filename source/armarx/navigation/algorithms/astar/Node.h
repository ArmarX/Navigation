#pragma once

#include <memory>
#include <vector>

#include <Eigen/Geometry>

#include <VirtualRobot/VirtualRobot.h>

namespace armarx::navigation::algorithm::astar
{
    class Node;
    using NodePtr = std::shared_ptr<Node>;

    /**
    * A Node can store data to all valid neighbors (successors) and a precessor.
    * It offers methods to determine the complete path to the starting point.
    */
    class Node
    {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW

        Node(const Eigen::Vector2f& position, float obstacleDistance);

        Eigen::Vector2f position;
        /// All nodes that are adjacent to this one.
        std::vector<NodePtr> successors;
        /// For traversal.
        NodePtr predecessor;

        float obstacleDistance;

        /// Collects all predecessors in order to generate path to starting point.
        std::vector<Eigen::Vector2f> traversePredecessors() const;
    };

} // namespace armarx::navigation::algorithm::astar
