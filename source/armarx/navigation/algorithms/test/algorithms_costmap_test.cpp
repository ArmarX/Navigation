/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Navigation::ArmarXObjects::algorithms
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <armarx/navigation/algorithms/persistence.h>
#include <armarx/navigation/algorithms/util.h>
#define BOOST_TEST_MODULE Navigation::ArmarXLibraries::algorithms

#define ARMARX_BOOST_TEST

#include <filesystem>
#include <fstream>
#include <iostream>

#include <boost/test/tools/old/interface.hpp>

#include <Eigen/Core>

#include <opencv2/core/eigen.hpp>
#include <opencv2/imgcodecs.hpp>

#include <SimoxUtility/json/json.hpp>

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>

#include <armarx/navigation/Test.h>
#include <armarx/navigation/algorithms/Costmap.h>
#include <armarx/navigation/algorithms/spfa/ShortestPathFasterAlgorithm.h>
#include <armarx/navigation/algorithms/types.h>

BOOST_AUTO_TEST_CASE(testMergeAlignedCostmaps1)
{
    const char* navigationBuildDirVar = std::getenv("armarx_navigation_DIR");
    BOOST_REQUIRE(navigationBuildDirVar != nullptr);

    std::string navigationBuildDir(navigationBuildDirVar);

    const std::filesystem::path testCostmapDir =
        std::filesystem::path(navigationBuildDirVar) / ".." /
        "data/armarx_navigation/tests/algorithms/costmaps/spfa-navigation-costmap";

    const auto costmap = armarx::navigation::algorithms::load(testCostmapDir);

    const auto mergedCostmap =
        armarx::navigation::algorithms::mergeAligned({costmap}, std::vector<float>{1.F});

    // ensure that both costmaps provide a mask
    BOOST_REQUIRE(costmap.getMask().has_value());
    BOOST_REQUIRE(mergedCostmap.getMask().has_value());

    for (int r = 0; r < mergedCostmap.getGrid().rows(); r++)
    {
        for (int c = 0; c < mergedCostmap.getGrid().cols(); c++)
        {
            constexpr float eps = 0.1;

            BOOST_CHECK(std::abs(mergedCostmap.getGrid()(r, c) - costmap.getGrid()(r, c)) < eps);
            BOOST_CHECK_EQUAL(mergedCostmap.getMask().value()(r, c), costmap.getMask().value()(r, c));
        }
    }
}
