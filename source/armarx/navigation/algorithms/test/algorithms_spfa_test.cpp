/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Navigation::ArmarXObjects::algorithms
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#define BOOST_TEST_MODULE Navigation::ArmarXLibraries::algorithms

#define ARMARX_BOOST_TEST

#include <filesystem>
#include <fstream>
#include <iostream>

#include <boost/test/tools/old/interface.hpp>

#include <Eigen/Core>

#include <opencv2/core/eigen.hpp>
#include <opencv2/imgcodecs.hpp>

#include <SimoxUtility/json/json.hpp>

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>

#include <armarx/navigation/Test.h>
#include <armarx/navigation/algorithms/Costmap.h>
#include <armarx/navigation/algorithms/spfa/ShortestPathFasterAlgorithm.h>
#include <armarx/navigation/algorithms/types.h>

BOOST_AUTO_TEST_CASE(testSPFA)
{
    const int rows = 100;
    const int cols = 200;

    const int cellSize = 100;

    Eigen::Vector2i start{50, 130};
    Eigen::Vector2i goal{50, 100};

    Eigen::MatrixXf obstacleDistances(rows, cols);
    obstacleDistances.setOnes();

    const armarx::navigation::algorithms::SceneBounds sceneBounds{
        .min = Eigen::Vector2f::Zero(), .max = Eigen::Vector2f{rows * cellSize, cols * cellSize}};

    const armarx::navigation::algorithms::Costmap::Parameters params;
    const armarx::navigation::algorithms::Costmap costmap(obstacleDistances, params, sceneBounds);


    ARMARX_INFO << "computing ...";
    armarx::navigation::algorithms::spfa::ShortestPathFasterAlgorithm::Parameters spfaParams{
        .obstacleDistanceCosts = false};
    armarx::navigation::algorithms::spfa::ShortestPathFasterAlgorithm spfa(costmap, spfaParams);

    const auto result = spfa.spfa(obstacleDistances, start);
    ARMARX_INFO << "done.";

    cv::Mat1f distances(rows, cols);
    cv::eigen2cv(result.distances, distances);

    cv::Mat1b distancesB(rows, cols);


    double minVal;
    double maxVal;
    cv::Point minLoc;
    cv::Point maxLoc;

    cv::minMaxLoc(distances, &minVal, &maxVal, &minLoc, &maxLoc);


    distances.convertTo(distancesB, CV_8UC1, std::numeric_limits<uchar>::max() / maxVal);

    cv::imwrite("/tmp/distances.exr", distances);
    cv::imwrite("/tmp/distances.png", distancesB);


    BOOST_CHECK_EQUAL(true, true);
}


BOOST_AUTO_TEST_CASE(testSPFAPlan)
{
    const int rows = 100;
    const int cols = 200;

    const int cellSize = 100;

    Eigen::Vector2i startVertex{50, 130};
    Eigen::Vector2i goalVertex{50, 100};

    Eigen::Vector2f start = startVertex.cast<float>() * cellSize;
    Eigen::Vector2f goal = goalVertex.cast<float>() * cellSize;

    Eigen::MatrixXf obstacleDistances(rows, cols);
    obstacleDistances.setOnes();

    const armarx::navigation::algorithms::SceneBounds sceneBounds{
        .min = Eigen::Vector2f::Zero(), .max = Eigen::Vector2f{rows * cellSize, cols * cellSize}};

    const armarx::navigation::algorithms::Costmap::Parameters params{.cellSize = cellSize};
    const armarx::navigation::algorithms::Costmap costmap(obstacleDistances, params, sceneBounds);

    ARMARX_INFO << "computing ...";
    armarx::navigation::algorithms::spfa::ShortestPathFasterAlgorithm::Parameters spfaParams{
        .obstacleDistanceCosts = false};
    armarx::navigation::algorithms::spfa::ShortestPathFasterAlgorithm spfa(costmap, spfaParams);

    const auto path = spfa.plan(start, goal);
    ARMARX_INFO << "done.";

    BOOST_CHECK_EQUAL(path.path.size(), startVertex.y() - goalVertex.y() + 1);

    for (const auto& p : path.path)
    {
        BOOST_CHECK_LT(std::abs(p.x() - startVertex.x() * cellSize), cellSize / 2 + 0.001);
    }
}


BOOST_AUTO_TEST_CASE(testGrid)
{

    const char* navigationBuildDirVar = std::getenv("armarx_navigation_DIR");
    BOOST_REQUIRE(navigationBuildDirVar != nullptr);

    std::string navigationBuildDir(navigationBuildDirVar);

    const std::filesystem::path testDataDir = std::filesystem::path(navigationBuildDirVar) / ".." /
                                              "data/armarx_navigation/tests/algorithms/";
    const std::string testExrFilename = (testDataDir / "grid.exr").string();
    const std::string testExrMetaFilename = (testDataDir / "grid.meta.json").string();
    BOOST_REQUIRE(std::filesystem::exists(testExrMetaFilename));

    std::ifstream fs(testExrMetaFilename);
    auto j = nlohmann::json::parse(fs);

    const float cellSize = j["cell_size"]; // [mm]
    BOOST_REQUIRE_GT(cellSize, 0);

    std::vector<float> sceneBoundsMinV(j["scene_bounds"]["min"]);
    std::vector<float> sceneBoundsMaxV(j["scene_bounds"]["max"]);
    BOOST_REQUIRE_EQUAL(sceneBoundsMinV.size(), 2);
    BOOST_REQUIRE_EQUAL(sceneBoundsMaxV.size(), 2);

    const Eigen::Vector2f sceneBoundsMin{sceneBoundsMinV.at(0), sceneBoundsMinV.at(1)};
    const Eigen::Vector2f sceneBoundsMax{sceneBoundsMaxV.at(0), sceneBoundsMaxV.at(1)};

    const armarx::navigation::algorithms::SceneBounds sceneBounds{.min = sceneBoundsMin,
                                                                  .max = sceneBoundsMax};

    BOOST_REQUIRE(std::filesystem::exists(testExrFilename));

    const auto mat = cv::imread(testExrFilename, cv::IMREAD_ANYCOLOR | cv::IMREAD_ANYDEPTH);

    Eigen::MatrixXf obstacleDistances;
    cv::cv2eigen(mat, obstacleDistances);


    const armarx::navigation::algorithms::Costmap::Parameters params{.cellSize = cellSize};
    const armarx::navigation::algorithms::Costmap costmap(obstacleDistances, params, sceneBounds);


    Eigen::Vector2f start{1500, 0};

    const auto vertex = costmap.toVertex(start);
    const auto position = costmap.toPositionGlobal(vertex.index);

    BOOST_CHECK_LT(std::abs(start.x() - position.x()), cellSize);
    BOOST_CHECK_LT(std::abs(start.y() - position.y()), cellSize);
}


BOOST_AUTO_TEST_CASE(testSPFAPlanWObstacleDistance)
{
    // armarx::ArmarXDataPath::getAbsolutePath("./armarx_navigation/tests/algorithm/grid.exr");

    const char* navigationBuildDirVar = std::getenv("armarx_navigation_DIR");
    BOOST_REQUIRE(navigationBuildDirVar != nullptr);

    std::string navigationBuildDir(navigationBuildDirVar);

    const std::filesystem::path testDataDir = std::filesystem::path(navigationBuildDirVar) / ".." /
                                              "data/armarx_navigation/tests/algorithms/";
    const std::string testExrFilename = (testDataDir / "grid.exr").string();
    const std::string testExrMetaFilename = (testDataDir / "grid.meta.json").string();
    BOOST_REQUIRE(std::filesystem::exists(testExrMetaFilename));

    std::ifstream fs(testExrMetaFilename);
    const auto j = nlohmann::json::parse(fs);

    const float cellSize = j["cell_size"]; // [mm]
    BOOST_REQUIRE_GT(cellSize, 0);

    std::vector<float> sceneBoundsMinV(j["scene_bounds"]["min"]);
    std::vector<float> sceneBoundsMaxV(j["scene_bounds"]["max"]);
    BOOST_REQUIRE_EQUAL(sceneBoundsMinV.size(), 2);
    BOOST_REQUIRE_EQUAL(sceneBoundsMaxV.size(), 2);

    const Eigen::Vector2f sceneBoundsMin{sceneBoundsMinV.at(0), sceneBoundsMinV.at(1)};
    const Eigen::Vector2f sceneBoundsMax{sceneBoundsMaxV.at(0), sceneBoundsMaxV.at(1)};

    const armarx::navigation::algorithms::SceneBounds sceneBounds{.min = sceneBoundsMin,
                                                                  .max = sceneBoundsMax};

    BOOST_REQUIRE(std::filesystem::exists(testExrFilename));

    const auto mat = cv::imread(testExrFilename, cv::IMREAD_ANYCOLOR | cv::IMREAD_ANYDEPTH);

    Eigen::MatrixXf obstacleDistances;
    cv::cv2eigen(mat, obstacleDistances);
    obstacleDistances = obstacleDistances.transpose();

    const armarx::navigation::algorithms::Costmap::Parameters params{.cellSize = cellSize};
    const armarx::navigation::algorithms::Costmap costmap(obstacleDistances, params, sceneBounds);

    ARMARX_INFO << "computing ...";
    armarx::navigation::algorithms::spfa::ShortestPathFasterAlgorithm::Parameters spfaParams{
        .obstacleDistanceCosts = true};
    armarx::navigation::algorithms::spfa::ShortestPathFasterAlgorithm spfa(costmap, spfaParams);

    Eigen::Vector2f start{1500, 0};
    Eigen::Vector2f goal{1700, 1000};

    const auto path = spfa.plan(start, goal);
    ARMARX_INFO << "done.";

    BOOST_CHECK(path.success);
    BOOST_CHECK(not path.path.empty());
}
