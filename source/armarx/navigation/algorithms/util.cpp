/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "util.h"

#include <algorithm>
#include <iterator>

#include <range/v3/all.hpp>

#include <opencv2/core.hpp>
#include <opencv2/core/eigen.hpp>
#include <opencv2/imgproc.hpp>

#include <SimoxUtility/algorithm/apply.hpp>
#include <VirtualRobot/BoundingBox.h>
#include <VirtualRobot/CollisionDetection/CollisionModel.h>
#include <VirtualRobot/SceneObjectSet.h>
#include <VirtualRobot/Workspace/WorkspaceGrid.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <armarx/navigation/algorithms/persistence.h>
#include <armarx/navigation/algorithms/Costmap.h>
#include <armarx/navigation/algorithms/CostmapBuilder.h>
#include <armarx/navigation/algorithms/types.h>


namespace armarx::navigation::algorithms
{


    SceneBounds
    computeSceneBounds(const VirtualRobot::SceneObjectSetPtr& obstacles)
    {
        SceneBounds bounds;

        ARMARX_CHECK_NOT_NULL(obstacles);
        ARMARX_CHECK(not obstacles->getCollisionModels().empty());

        for (size_t i = 0; i < obstacles->getCollisionModels().size(); i++)
        {
            VirtualRobot::BoundingBox bb = obstacles->getCollisionModels()[i]->getBoundingBox();
            bounds.min.x() = std::min(bb.getMin().x(), bounds.min.x());
            bounds.min.y() = std::min(bb.getMin().y(), bounds.min.y());
            bounds.max.x() = std::max(bb.getMax().x(), bounds.max.x());
            bounds.max.y() = std::max(bb.getMax().y(), bounds.max.y());
        }

        return bounds;
    }


    SceneBounds
    toSceneBounds(const VirtualRobot::WorkspaceGrid::Extends& extends)
    {
        return {.min = Eigen::Vector2f{extends.minX, extends.minY},
                .max = Eigen::Vector2f{extends.maxX, extends.maxY}};
    }

    SceneBounds
    merge(const std::vector<SceneBounds>& sceneBounds)
    {
        SceneBounds bounds;

        const auto expandBounds = [&bounds](const SceneBounds& sb)
        {
            bounds.min.x() = std::min(bounds.min.x(), sb.min.x());
            bounds.min.y() = std::min(bounds.min.y(), sb.min.y());

            bounds.max.x() = std::max(bounds.max.x(), sb.max.x());
            bounds.max.y() = std::max(bounds.max.y(), sb.max.y());

            return bounds;
        };

        ranges::for_each(sceneBounds, expandBounds);

        return bounds;
    }


    Costmap
    toCostmap(const VirtualRobot::WorkspaceGrid& workspaceGrid)
    {
        const Costmap::Grid grid(workspaceGrid.getCells().x, workspaceGrid.getCells().y);
        const SceneBounds sceneBounds = toSceneBounds(workspaceGrid.getExtends());

        const Costmap::Parameters parameters{.binaryGrid = false,
                                             .cellSize = workspaceGrid.getDiscretizeSize()};

        return {grid, parameters, sceneBounds};
    }


    // Costmap
    // mergeUnaligned(const std::vector<Costmap>& costmaps,
    //                const std::vector<float>& weights,
    //                float cellSize,
    //                const Eigen::Array2f& offset)
    // {
    //     ARMARX_TRACE;

    //     ARMARX_CHECK_NOT_EMPTY(costmaps);
    //     ARMARX_CHECK_EQUAL(costmaps.size(), weights.size());

    //     // if unset, use the smallest cell size
    //     if (cellSize < 0)
    //     {
    //         const auto compCellSize = [](const Costmap& a, const Costmap& b) -> bool
    //         { return a.params().cellSize < b.params().cellSize; };

    //         cellSize = ranges::min_element(costmaps, compCellSize)->params().cellSize;
    //     }

    //     // scale each costmap
    //     std::vector<Costmap> scaledCostmaps =
    //         simox::alg::apply(costmaps,
    //                           [&cellSize](const Costmap& costmap) -> Costmap
    //                           { return scaleCostmap(costmap, cellSize); });

    //     // merge costmaps into one
    //     ARMARX_VERBOSE << "Merging " << scaledCostmaps.size() << " costmaps";

    //     // - combine scene bounds
    //     std::vector<SceneBounds> sceneBoundsAll = simox::alg::apply(
    //         scaledCostmaps, [](const Costmap& costmap) { return costmap.getSceneBounds(); });

    //     SceneBounds sceneBounds = merge(sceneBoundsAll);
    //     sceneBounds.min.x() -= 0.5 * cellSize;
    //     sceneBounds.max.x() -= 0.5 * cellSize;
    //     sceneBounds.min.y() -= cellSize;
    //     sceneBounds.max.y() -= cellSize;


    //     sceneBounds.min.x() += offset.x();
    //     sceneBounds.max.x() += offset.x();
    //     sceneBounds.min.y() += offset.y();
    //     sceneBounds.max.y() += offset.y();

    //     // - create grid
    //     const auto largeGrid = CostmapBuilder::createUniformGrid(
    //         sceneBounds, Costmap::Parameters{.binaryGrid = false, .cellSize = cellSize});

    //     Costmap largeCostmap(
    //         largeGrid, Costmap::Parameters{.binaryGrid = false, .cellSize = cellSize}, sceneBounds);

    //     // TODO save each combo individually

    //     static int i = 0;
    //     // - add all costmaps to this one
    //     ranges::for_each(ranges::views::zip(scaledCostmaps, weights),
    //                      [&largeCostmap](const auto& p)
    //                      {
    //                          ARMARX_INFO << "adding to costmap";
    //                          const auto& [costmap, weight] = p;
    //                          largeCostmap.add(costmap, weight);

    //                          armarx::navigation::algorithms::save(
    //                              costmap, "/tmp/costmap" + std::to_string(i++));
    //                      });

    //     armarx::navigation::algorithms::save(largeCostmap,
    //                                          "/tmp/large-costmap" + std::to_string(i++));

    //     return largeCostmap;
    // }

    void
    checkSameSize(const std::vector<Costmap>& costmaps)
    {
        ARMARX_CHECK_NOT_EMPTY(costmaps);

        const auto assertSameSize = [&costmaps](const Costmap& costmap)
        {
            ARMARX_CHECK_EQUAL(costmap.getGrid().rows(), costmaps.front().getGrid().rows());
            ARMARX_CHECK_EQUAL(costmap.getGrid().rows(), costmaps.front().getGrid().rows());
        };

        ranges::for_each(costmaps, assertSameSize);
    }


    Costmap
    mergeAligned(const std::vector<Costmap>& costmaps, CostmapMergeMode mode)
    {
        ARMARX_CHECK_NOT_EMPTY(costmaps);
        checkSameSize(costmaps);


        // average mode: fixed weight
        if (mode == CostmapMergeMode::AVERAGE)
        {
            const std::vector<float> weights(costmaps.size(), 1.F / costmaps.size());
            return mergeAligned(costmaps, weights);
        }

        const auto grid = CostmapBuilder::createUniformGrid(costmaps.front().getLocalSceneBounds(),
                                                            costmaps.front().params());

        Costmap mergedCostmap(grid, costmaps.front().params(), costmaps.front().getLocalSceneBounds());

        const auto addMode = [&]() -> Costmap::AddMode
        {
            Costmap::AddMode addMode{};

            switch (mode)
            {
                case CostmapMergeMode::AVERAGE:
                    ARMARX_CHECK(false) << "This case should already be handled.";
                    break;
                case CostmapMergeMode::MIN:
                    addMode = Costmap::AddMode::MIN;
                    break;
                case CostmapMergeMode::MAX:
                    addMode = Costmap::AddMode::MAX;
                    break;
            }

            return addMode;
        }();

        ranges::for_each(costmaps,
                         [&mergedCostmap, &addMode](const auto& costmap)
                         { mergedCostmap.add(costmap, addMode); });

        return mergedCostmap;
    }

    Costmap
    mergeAligned(const std::vector<Costmap>& costmaps, const std::vector<float>& weights)
    {
        ARMARX_CHECK_EQUAL(costmaps.size(), weights.size());
        ARMARX_CHECK_NOT_EMPTY(costmaps);
        checkSameSize(costmaps);


        const auto grid = CostmapBuilder::createUniformGrid(costmaps.front().getLocalSceneBounds(),
                                                            costmaps.front().params());

        Costmap mergedCostmap(grid, costmaps.front().params(), costmaps.front().getLocalSceneBounds());

        // foreach pair (costmap, weight): add it to there merged costmap
        ranges::for_each(ranges::views::zip(costmaps, weights),
                         [&mergedCostmap](const auto& p)
                         {
                             const auto& [costmap, weight] = p;
                             mergedCostmap.add(costmap, weight);
                         });

        return mergedCostmap;
    }

    Costmap
    scaleCostmap(const Costmap& costmap, float cellSize)
    {
        const float scale = costmap.params().cellSize / cellSize;
        ARMARX_VERBOSE << "Scaling grid by a factor of " << scale;

        cv::Mat src;
        cv::eigen2cv(costmap.getGrid(), src);

        cv::Mat dst;
        cv::resize(src, dst, cv::Size{0, 0}, scale, scale);

        Eigen::MatrixXf scaledGrid;
        cv::cv2eigen(dst, scaledGrid);


        std::optional<Costmap::Mask> scaledMask;
        if (costmap.getMask().has_value())
        {
            ARMARX_TRACE;
            ARMARX_VERBOSE << "Scaling mask by a factor of " << scale;

            // just an int large enough to check if resizing causes
            // issues on boundary to invalid regions
            constexpr int someInt = 100;

            Eigen::Matrix<std::uint8_t, Eigen::Dynamic, Eigen::Dynamic> mask =
                someInt * costmap.getMask().value().cast<std::uint8_t>();

            cv::Mat maskMat;
            cv::eigen2cv(mask, maskMat);

            cv::Mat maskScaled;
            cv::resize(maskMat, maskScaled, cv::Size{0, 0}, scale, scale);

            // only those points are valid where the original value '10' is preserved.
            // all other points are boundary points to the invalid area
            // cv::threshold(maskMat, maskMat, 10.9, 1, cv::THRESH_BINARY);

            Eigen::MatrixXi scaledMaskInt; //(scaledGrid.rows(), scaledGrid.cols());
            cv::cv2eigen(maskScaled, scaledMaskInt);

            Costmap::Mask m = scaledMaskInt.array() >= someInt;

            scaledMask = m;
        }

        ARMARX_VERBOSE << "Original size (" << costmap.getGrid().rows() << ", "
                       << costmap.getGrid().cols() << ")";
        ARMARX_VERBOSE << "Resized to (" << scaledGrid.rows() << ", " << scaledGrid.cols() << ")";

        return {scaledGrid,
                Costmap::Parameters{.binaryGrid = false, .cellSize = cellSize},
                costmap.getLocalSceneBounds(),
                scaledMask};
    }


} // namespace armarx::navigation::algorithms
