#pragma once

#include <armarx/navigation/client/ComponentPlugin.h>
#include <armarx/navigation/client/NavigationStackConfig.h>

namespace armarx::nav
{
    using namespace armarx::navigation::core;
    using namespace armarx::navigation::client;
} // namespace armarx::nav
