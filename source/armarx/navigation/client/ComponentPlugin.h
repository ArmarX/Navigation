#pragma once


#include <ArmarXCore/core/ComponentPlugin.h>
#include <ArmarXCore/core/ManagedIceObject.h>

#include <armarx/navigation/client/ComponentPlugin.h>
#include <armarx/navigation/client/Navigator.h>
#include <armarx/navigation/client/ice/NavigatorInterface.h>
#include <armarx/navigation/client/services/IceNavigator.h>
#include <armarx/navigation/client/services/SimpleEventHandler.h>

namespace armarx::armem::client
{
    class MemoryNameSystem;
}

namespace armarx::navigation::client
{

    class Navigator;

    class ComponentPlugin : public armarx::ComponentPlugin
    {
    public:
        ComponentPlugin(ManagedIceObject& parent, const std::string& prefix) :
            armarx::ComponentPlugin(parent, prefix){};

        ~ComponentPlugin() override;

        void postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties) override;

        void preOnInitComponent() override;

        void preOnConnectComponent() override;

        void configureNavigator(const client::NavigationStackConfig& stackConfig,
                                const std::string& configId);

    private:
        static constexpr const char* PROPERTY_NAME = "nav.NavigatorName";

        client::NavigatorInterfacePrx navigatorPrx;
        IceNavigator iceNavigator;

        std::unique_ptr<SimpleEventHandler> eventHandler;

    public:
        std::unique_ptr<Navigator> navigator;
    };


    class ComponentPluginUser : virtual public ManagedIceObject
    {

    public:
        ComponentPluginUser();

        void configureNavigator(const client::NavigationStackConfig& stackConfig);

        Navigator& getNavigator();

        // Non-API
        ~ComponentPluginUser() override;

    private:
        ComponentPlugin* plugin = nullptr;
    };

} // namespace armarx::navigation::client
