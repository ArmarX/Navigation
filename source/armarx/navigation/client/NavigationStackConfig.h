/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <memory>

#include <RobotAPI/interface/aron/Aron.h>
#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>

#include <armarx/navigation/core/types.h>

// TODO remove these headers once ARON types are used as params
#include <armarx/navigation/global_planning/GlobalPlanner.h>
#include <armarx/navigation/local_planning/LocalPlanner.h>
#include <armarx/navigation/safety_control/SafetyController.h>
#include <armarx/navigation/trajectory_control/TrajectoryController.h>


namespace armarx::navigation::client
{
    struct GeneralConfig
    {
        // if the desired velocity is below this threshold, the platform will not be moved
        core::VelocityLimits minVel{0.F, 0.F};

        //! max ...
        core::VelocityLimits maxVel;

        virtual ~GeneralConfig() = default;

        virtual aron::data::DictPtr toAron() const;
    };

    class NavigationStackConfig
    {
    public:
        NavigationStackConfig() = default;
        virtual ~NavigationStackConfig() = default;

        NavigationStackConfig& general(const GeneralConfig& cfg);

        NavigationStackConfig& globalPlanner(const global_planning::GlobalPlannerParams& params);

        NavigationStackConfig& localPlanner(const loc_plan::LocalPlannerParams& params);

        NavigationStackConfig&
        trajectoryController(const traj_ctrl::TrajectoryControllerParams& params);

        NavigationStackConfig& safetyController(const safe_ctrl::SafetyControllerParams& params);

        aron::data::dto::DictPtr toAron() const;

        //! checks if at least global planner and trajectory controller are set
        bool isValid() const;

    protected:
    private:
        aron::data::Dict dict;
    };
} // namespace armarx::navigation::client
