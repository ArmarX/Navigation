#include "PathBuilder.h"

#include <armarx/navigation/client/ice_conversions.h>

namespace armarx::navigation::client
{
    PathBuilder&
    PathBuilder::add(const std::string& locationId,
                     const GlobalPlanningStrategy& strategy,
                     const Config& config)
    {

        waypoints.push_back(WaypointTarget{.locationId = locationId, .strategy = strategy});

        return *this;
    }

    PathBuilder&
    PathBuilder::add(const core::Pose& pose,
                     const GlobalPlanningStrategy& strategy,
                     const Config& config)
    {

        waypoints.push_back(WaypointTarget{.pose = pose, .strategy = strategy});


        return *this;
    }


    const WaypointTargets&
    PathBuilder::path() const
    {
        return waypoints;
    }

} // namespace armarx::navigation::client
