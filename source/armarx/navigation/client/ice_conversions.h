/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <optional>
#include <string>
#include <vector>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <armarx/navigation/client/ice/NavigatorInterface.h>
#include <armarx/navigation/client/types.h>
#include <armarx/navigation/core/basic_types.h>

namespace armarx::navigation::client
{

    // TODO move type out of this header (e.g. to types.h)
    struct WaypointTarget
    {
        std::optional<std::string> locationId = std::nullopt;
        std::optional<core::Pose> pose = std::nullopt;

        client::GlobalPlanningStrategy strategy = client::GlobalPlanningStrategy::Free;

        //client::Config config;
    };

    inline std::ostream&
    operator<<(std::ostream& str, const WaypointTarget& res)
    {
        str << "WaypointTarget { ";

        if (res.locationId.has_value() and not res.locationId->empty())
        {
            str << "location: " << res.locationId.value();
        }
        else if (res.pose.has_value())
        {
            str << "pose :" << res.pose->matrix();
        }

        str << "}";

        return str;
    }


    // TODO move to util file
    inline void
    validate(const std::vector<WaypointTarget>& path)
    {
        std::for_each(path.begin(),
                      path.end(),
                      [](const WaypointTarget& wpTarget)
                      {
                          ARMARX_CHECK(wpTarget.locationId.has_value() or wpTarget.pose.has_value())
                              << "Either `locationId` or `pose` has to be provided.";
                      });
    }

    using WaypointTargets = std::vector<WaypointTarget>;

    client::GlobalPlanningStrategy defrost(const client::detail::GlobalPlanningStrategy& strategy);
    WaypointTarget defrost(const client::detail::Waypoint& waypoint);
    WaypointTargets defrost(const client::detail::Waypoints& waypoints);

    client::detail::GlobalPlanningStrategy freeze(const client::GlobalPlanningStrategy& strategy);
    client::detail::Waypoint freeze(const client::WaypointTarget& target);
    client::detail::Waypoints freeze(const client::WaypointTargets& targets);

} // namespace armarx::navigation::client
