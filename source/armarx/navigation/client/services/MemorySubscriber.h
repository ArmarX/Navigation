#pragma once


#include <mutex>

#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

#include <RobotAPI/libraries/armem/client/Reader.h>
#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem/core/Time.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>

#include <armarx/navigation/client/services/EventSubscriptionInterface.h>
#include <armarx/navigation/client/services/SimpleEventHandler.h>

namespace armarx::armem::client
{
    class MemoryNameSystem;
}

namespace armarx::navigation::client
{

    class MemorySubscriber : virtual public SimpleEventHandler
    {

    public:
        MemorySubscriber(const std::string& callerId, armem::client::MemoryNameSystem& mns);

        void handleEvent(const armem::wm::EntityInstance& memoryEntity);

        // Non-API.
    public:
        ~MemorySubscriber() override;


    protected:
        void runPollMemoryEvents();
        void handleEvents(const armem::wm::Memory& memory);


        void onEntityUpdate(const std::vector<armem::MemoryID>& snapshotIDs);

    private:
        const std::string callerId;
        armem::client::MemoryNameSystem& memoryNameSystem;

        armem::Time lastMemoryPoll;

        armem::client::Reader memoryReader;

        std::mutex eventHandlingMtx;

        armarx::PeriodicTask<MemorySubscriber>::pointer_type task;
    };

} // namespace armarx::navigation::client
