#pragma once

#include <SimoxUtility/meta/enum/EnumNames.hpp>

namespace armarx::navigation::common
{

    enum class ControllerType
    {
        PlatformTrajectory
    };

    constexpr const char* PlatformTrajectoryControllerName=  "PlatformTrajectory";

    inline const simox::meta::EnumNames<ControllerType> ControllerTypeNames{
        {ControllerType::PlatformTrajectory, PlatformTrajectoryControllerName}};

} // namespace armarx::navigation::common
