armarx_add_component(GraphImportExport
    DEPENDENCIES
        # ArmarXCore
        ArmarXCore
        ArmarXCoreComponentPlugins  # For DebugObserver plugin.
        # ArmarXGui
        ArmarXGuiComponentPlugins  # For RemoteGui plugin.
        # RobotAPI
        ## RobotAPICore
        ## RobotAPIInterfaces
        RobotAPIComponentPlugins  # For ArViz and other plugins.
        armem

        # MemoryX
        MemoryXCore
        MemoryXMemoryTypes

        # This project.
        armarx_navigation::location
        armarx_navigation::graph

        ## ${PROJECT_NAME}Interfaces  # For ice interfaces from this package.
        # This component
        ## GraphImportExportInterfaces  # If you defined a component ice interface above.

    SOURCES
        GraphImportExport.cpp

    HEADERS
        GraphImportExport.h
)
