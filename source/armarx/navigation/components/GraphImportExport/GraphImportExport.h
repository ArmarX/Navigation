/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::ArmarXObjects::GraphImportExport
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/libraries/ArmarXCoreComponentPlugins/DebugObserverComponentPlugin.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/armem/client/Writer.h>
#include <RobotAPI/libraries/armem/client/plugins.h>

#include <MemoryX/interface/components/GraphNodePoseResolverInterface.h>
#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>
#include <MemoryX/interface/memorytypes/MemorySegments.h>
#include <armarx/navigation/core/forward_declarations.h>


namespace armarx::navigation
{

    /**
     * @defgroup Component-GraphImportExport GraphImportExport
     * @ingroup MemoryX-Components
     * A description of the component GraphImportExport.
     *
     * @class GraphImportExport
     * @ingroup Component-GraphImportExport
     * @brief Brief description of class GraphImportExport.
     *
     * Detailed description of class GraphImportExport.
     */
    class GraphImportExport :
        virtual public armarx::Component,
        virtual public armarx::DebugObserverComponentPluginUser,
        virtual public armarx::LightweightRemoteGuiComponentPluginUser,
        virtual public armarx::ArVizComponentPluginUser,
        virtual public armarx::armem::ClientPluginUser
    {
    public:
        GraphImportExport();


        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


    protected:
        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;


        /// This function should be called once in onConnect() or when you
        /// need to re-create the Remote GUI tab.
        void createRemoteGuiTab(const std::vector<std::string>& sceneNames);

        /// After calling `RemoteGui_startRunningTask`, this function is
        /// called periodically in a separate thread. If you update variables,
        /// make sure to synchronize access to them.
        void RemoteGui_update() override;


    private:
        void refreshScenes();

        void locationsMemoryxToArmem(const std::string& sceneName);
        void locationsArmemToMemoryx(const std::string& sceneName);
        void graphMemoryxToArmem(const std::string& sceneName);
        void graphArmemToMemoryx(const std::string& sceneName);

        void clearArMemProviderSegment(armem::client::Writer& writer,
                                       const armem::MemoryID& providerSegmentID);

        armem::MemoryID getLocationProviderSegmentID();
        armem::MemoryID getGraphProviderSegmentID();

        navigation::core::Graph toArmemGraph(const memoryx::GraphNodeBaseList& graphNodes);


    private:
        struct Proxies
        {
            memoryx::PriorKnowledgeInterfacePrx priorKnowledge;
            memoryx::GraphNodePoseResolverInterfacePrx graphNodePoseResolver;
            memoryx::GraphMemorySegmentBasePrx graphSegment;

            armem::client::Writer locationWriter;
            armem::client::Writer graphWriter;
        };
        Proxies proxies;


        /// Properties shown in the Scenario GUI.
        struct Properties
        {
            armem::MemoryID locationCoreSegmentID;
            armem::MemoryID graphCoreSegmentID;
        };
        Properties properties;


        /// Tab shown in the Remote GUI.
        struct RemoteGuiTab : armarx::RemoteGui::Client::Tab
        {
            armarx::RemoteGui::Client::ComboBox sceneComboBox;
            armarx::RemoteGui::Client::Button sceneRefreshButton;

            armarx::RemoteGui::Client::LineEdit providerSegmentLine;

            armarx::RemoteGui::Client::Button locationsMemoryxToArmemButton;
            armarx::RemoteGui::Client::Button locationsArmemToMemoryxButton;
            armarx::RemoteGui::Client::Button locationsClearArMemButton;

            armarx::RemoteGui::Client::Button graphMemoryxToArmemButton;
            armarx::RemoteGui::Client::Button graphArmemToMemoryxButton;
            armarx::RemoteGui::Client::Button graphClearArMemButton;

            armarx::RemoteGui::Client::CheckBox dryRun;
            armarx::RemoteGui::Client::CheckBox visuEnabled;
        };
        RemoteGuiTab tab;
    };
} // namespace armarx::navigation
