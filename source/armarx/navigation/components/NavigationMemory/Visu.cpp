/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Navigation::ArmarXObjects::NavigationMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Visu.h"

#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>

#include <armarx/navigation/core/Graph.h>
#include <armarx/navigation/core/aron/Graph.aron.generated.h>
#include <armarx/navigation/core/aron/Location.aron.generated.h>
#include <armarx/navigation/graph/Visu.h>


namespace armarx::navigation::memory
{

    Visu::Visu(viz::Client arviz,
               const armem::server::wm::CoreSegment& locSegment,
               const armem::server::wm::CoreSegment& graphSegment) :
        arviz(arviz),
        locSegment(locSegment),
        graphSegment(graphSegment),
        visu(std::make_unique<graph::GraphVisu>())
    {
    }


    Visu::~Visu()
    {
    }


    void
    Visu::drawLocations(std::vector<viz::Layer>& layers, bool enabled)
    {
        using namespace armem::server;

        viz::Layer& layer = layers.emplace_back(arviz.layer(locSegment.id().str()));
        if (enabled)
        {
            std::map<armem::MemoryID, location::arondto::Location> locations;
            locSegment.doLocked(
                [&]()
                {
                    locSegment.forEachEntity(
                        [&](const wm::Entity& entity)
                        {
                            if (const wm::EntityInstance* instance = entity.findLatestInstance())
                            {
                                locations[entity.id()].fromAron(instance->data());
                            }
                        });
                });

            for (auto& [id, location] : locations)
            {
                visu->vertex->draw(layer, id.str(), core::Pose(location.globalRobotPose));
            }
        }
    }


    void
    Visu::drawGraphs(std::vector<viz::Layer>& layers, bool enabled)
    {
        using namespace armem::server;

        std::map<armem::MemoryID, core::Graph> graphs;
        graphSegment.doLocked(
            [&]()
            {
                graphSegment.forEachEntity(
                    [&](const wm::Entity& entity)
                    {
                        core::Graph& graph = graphs[entity.id()];
                        if (enabled)
                        {
                            if (const wm::EntityInstance* instance = entity.findLatestInstance())
                            {
                                navigation::core::arondto::Graph aron;
                                aron.fromAron(instance->data());
                                fromAron(aron, graph);
                            }
                        }
                        // else: empty layer
                    });
            });

        for (auto& [id, graph] : graphs)
        {
            viz::Layer& layer = layers.emplace_back(arviz.layer(id.str()));
            if (enabled)
            {
                core::resolveLocations(graph, locSegment);
                visu->draw(layer, graph);
            }
            // else: clear layer
        }
    }

} // namespace armarx::navigation::memory
