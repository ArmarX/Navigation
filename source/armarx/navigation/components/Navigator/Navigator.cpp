/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Navigation::ArmarXObjects::Navigator
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Navigator.h"

#include <algorithm>
#include <cmath>
#include <cstddef>
#include <cstdint>
#include <iterator>
#include <memory>
#include <utility>

#include <Eigen/Geometry>

#include <opencv2/core/eigen.hpp>
#include <opencv2/imgcodecs.hpp>

#include <Ice/Current.h>

#include <SimoxUtility/color/ColorMap.h>
#include <SimoxUtility/color/cmaps/colormaps.h>
#include <SimoxUtility/math/convert/rpy_to_mat3f.h>
#include <VirtualRobot/MathTools.h>
#include <VirtualRobot/Robot.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/exceptions/LocalException.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/LogSender.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXCore/core/time/Clock.h>
#include <ArmarXCore/core/time/Duration.h>
#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>
#include <ArmarXCore/util/CPPUtility/trace.h>

#include "ArmarXGui/libraries/RemoteGui/Client/Widgets.h"

#include <RobotAPI/components/ArViz/Client/Client.h>
#include <RobotAPI/components/ArViz/Client/Elements.h>
#include <RobotAPI/components/ArViz/Client/elements/Color.h>
#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointControllerRegistry.h>
#include <RobotAPI/interface/ArViz/Elements.h>
#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem/core/Time.h>
#include <RobotAPI/libraries/armem_vision/OccupancyGridHelper.h>
#include <RobotAPI/libraries/armem_vision/client/occupancy_grid/Reader.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#include "armarx/navigation/server/scene_provider/SceneProvider.h"
#include <armarx/navigation/algorithms/Costmap.h>
#include <armarx/navigation/algorithms/CostmapBuilder.h>
#include <armarx/navigation/algorithms/astar/util.h>
#include <armarx/navigation/algorithms/persistence.h>
#include <armarx/navigation/algorithms/spfa/ShortestPathFasterAlgorithm.h>
#include <armarx/navigation/algorithms/util.h>
#include <armarx/navigation/client/NavigationStackConfig.h>
#include <armarx/navigation/client/PathBuilder.h>
#include <armarx/navigation/client/ice/NavigatorInterface.h>
#include <armarx/navigation/client/ice_conversions.h>
#include <armarx/navigation/components/Navigator/RemoteGui.h>
#include <armarx/navigation/conversions/eigen.h>
#include <armarx/navigation/core/types.h>
#include <armarx/navigation/factories/NavigationStackFactory.h>
#include <armarx/navigation/server/Navigator.h>
#include <armarx/navigation/server/event_publishing/MemoryPublisher.h>
#include <armarx/navigation/server/introspection/MemoryIntrospector.h>
#include <armarx/navigation/util/util.h>

namespace armarx::navigation::components
{
    ARMARX_DECOUPLED_REGISTER_COMPONENT(Navigator);
}

namespace armarx::navigation
{

    std::vector<core::Pose>
    convert(const std::vector<Eigen::Matrix4f>& wps)
    {
        std::vector<core::Pose> p;
        p.reserve(wps.size());
        std::transform(wps.begin(),
                       wps.end(),
                       std::back_inserter(p),
                       [](const auto& p) { return core::Pose(p); });
        return p;
    }

} // namespace armarx::navigation

namespace armarx::navigation::components
{

    components::Navigator::Navigator() : parameterizationService(nullptr, nullptr)
    // publisher(&resultsWriter, &eventsWriter)
    {
        // scene().timeServer = &timeServer;

        addPlugin(parameterizationReaderPlugin);
        addPlugin(parameterizationWriterPlugin);
        addPlugin(eventsWriterPlugin);
        addPlugin(resultsWriterPlugin);
        addPlugin(graphReaderPlugin);
        addPlugin(costmapReaderPlugin);
        addPlugin(costmapWriterPlugin);

        addPlugin(virtualRobotReaderPlugin);

        parameterizationService = server::MemoryParameterizationService(
            &parameterizationReaderPlugin->get(), &parameterizationWriterPlugin->get());
    }


    components::Navigator::~Navigator() = default;

    void
    components::Navigator::onInitComponent()
    {
    }

    void
    components::Navigator::onConnectComponent()
    {
        ARMARX_TRACE;

        static bool initialized{false};

        // redirect to onReconnect to avoid any setup issues
        if (initialized)
        {
            ARMARX_INFO << "Reconnecting ...";
            onReconnectComponent();
            return;
        }


        virtualRobotReaderPlugin->get().setSyncTimeout(Duration::MilliSeconds(20));
        virtualRobotReaderPlugin->get().setSleepAfterSyncFailure(Duration::MilliSeconds(10));

        // initialize scene provider
        ARMARX_TRACE;
        {
            const server::scene_provider::SceneProvider::InjectedServices srv{
                .graphReader = &graphReaderPlugin->get(),
                .costmapReader = &costmapReaderPlugin->get(),
                .virtualRobotReader = &virtualRobotReaderPlugin->get(),
                .objectPoseClient = ObjectPoseClientPluginUser::getClient()};

            const std::string robotName = getControlComponentPlugin()
                                              .getRobotUnitPlugin()
                                              .getRobotUnit()
                                              ->getKinematicUnit()
                                              ->getRobotName();

            const server::scene_provider::SceneProvider::Config cfg{.robotName = robotName};
            sceneProvider = server::scene_provider::SceneProvider(srv, cfg);
        }

        initializeScene();

        ARMARX_TRACE;
        executor.emplace(server::PlatformControllerExecutor(getControlComponentPlugin()));

        ARMARX_TRACE;
        introspector = server::ArvizIntrospector(getArvizClient(), scene().robot);
        // memoryIntrospector = server::MemoryIntrospector(resultsWriterPlugin->get(), );


        navRemoteGui = std::make_unique<NavigatorRemoteGui>(remoteGui, *this);
        navRemoteGui->enable();

        initialized = true;

        ARMARX_INFO << "Initialized. Will now respond to navigation requests.";
    }

    void
    components::Navigator::onReconnectComponent()
    {
        ARMARX_TRACE;
        navRemoteGui->enable();
    }

    void
    components::Navigator::onDisconnectComponent()
    {
        ARMARX_TRACE;

        stopAll();
        navRemoteGui->disable();
    }

    void
    components::Navigator::onExitComponent()
    {
    }

    void
    components::Navigator::initializeScene()
    {
        ARMARX_TRACE;
        sceneProvider->initialize(armarx::Clock::Now());
    }

    std::string
    components::Navigator::getDefaultName() const
    {
        return "Navigator";
    }

    void
    components::Navigator::createConfig(const aron::data::dto::DictPtr& stackConfig,
                                        const std::string& callerId,
                                        const Ice::Current&)
    {
        // TODO: Not thread-safe.
        ARMARX_TRACE;
        ARMARX_INFO << "Creating config for caller '" << callerId << "'";

        parameterizationService.store(stackConfig, callerId, armarx::Clock::Now());

        server::NavigationStack stack =
            fac::NavigationStackFactory::create(stackConfig, sceneProvider->scene());

        memoryIntrospectors.emplace_back(
            std::make_unique<server::MemoryIntrospector>(resultsWriterPlugin->get(), callerId));

        // keep track of all memory publishers
        memoryPublishers.emplace(
            callerId,
            std::make_unique<server::MemoryPublisher>(
                &resultsWriterPlugin->get(), &eventsWriterPlugin->get(), callerId));

        navigators.emplace(
            std::piecewise_construct,
            std::forward_as_tuple(callerId),
            std::forward_as_tuple(
                server::Navigator::Config{.stack = std::move(stack),
                                          .general = server::Navigator::Config::General{}},
                server::Navigator::InjectedServices{.executor = &executor.value(),
                                                    .publisher =
                                                        memoryPublishers.at(callerId).get(),
                                                    .introspector = &(introspector.value()),
                                                    .sceneProvider = &sceneProvider.value()}));
    }

    void
    components::Navigator::moveTo(const std::vector<Eigen::Matrix4f>& waypoints,
                                  const std::string& navigationMode,
                                  const std::string& callerId,
                                  const Ice::Current&)
    {
        ARMARX_TRACE;

        ARMARX_INFO << "moveTo() requested by caller '" << callerId << "'";
        ARMARX_CHECK(navigators.count(callerId) > 0)
            << "Navigator config for caller `" << callerId << "` not registered!";

        try
        {
            navigators.at(callerId).moveTo(convert(waypoints),
                                           core::NavigationFrameNames.from_name(navigationMode));
        }
        catch (...)
        {
            ARMARX_WARNING << GetHandledExceptionString();

            // TODO: Error handling.
            ARMARX_WARNING << "Failed to execute moveTo()."
                           << "Movement will be paused.";
            stopAll(); // TODO pause movement must not throw
            throw;
        }
    }


    void
    Navigator::moveTo2(const client::detail::Waypoints& waypoints,
                       const std::string& navigationMode,
                       const std::string& callerId,
                       const Ice::Current& c)
    {
        ARMARX_TRACE;

        ARMARX_IMPORTANT << "moveTo2 requested by caller '" << callerId << "'";

        const std::vector<client::WaypointTarget> wps = client::defrost(waypoints);

        ARMARX_CHECK(navigators.count(callerId) > 0)
            << "Navigator config for caller `" << callerId << "` not registered!";

        navigators.at(callerId).moveTo(wps, core::NavigationFrameNames.from_name(navigationMode));
    }

    void
    components::Navigator::moveTowards(const Eigen::Vector3f& direction,
                                       const std::string& navigationMode,
                                       const std::string& callerId,
                                       const Ice::Current&)
    {
        // TODO: Error handling.
        ARMARX_TRACE;
        ARMARX_INFO << "MoveTowards requested by caller '" << callerId << "'";

        ARMARX_CHECK(navigators.count(callerId) > 0)
            << "Navigator config for caller `" << callerId << "` not registered!";

        navigators.at(callerId).moveTowards(direction,
                                            core::NavigationFrameNames.from_name(navigationMode));
    }

    void
    components::Navigator::pause(const std::string& configId, const Ice::Current&)
    {
        ARMARX_CHECK(navigators.count(configId) > 0)
            << "Navigator config for caller `" << configId << "` not registered!";
        navigators.at(configId).pause();
    }

    void
    components::Navigator::resume(const std::string& configId, const Ice::Current&)
    {
        ARMARX_TRACE;
        ARMARX_CHECK(navigators.count(configId) > 0)
            << "Navigator config for caller `" << configId << "` not registered!";
        navigators.at(configId).resume();
    }

    void
    components::Navigator::stop(const std::string& configId, const Ice::Current&)
    {
        // FIXME make sure that event is emitted
        ARMARX_TRACE;
        ARMARX_CHECK(navigators.count(configId) > 0)
            << "Navigator config for caller `" << configId << "` not registered!";
        navigators.at(configId).stop();

        // emit UserAbortTriggered event
        const core::UserAbortTriggeredEvent event{{armarx::Clock::Now()},
                                                  core::Pose(scene().robot->getGlobalPose())};
        memoryPublishers.at(configId)->userAbortTriggered(event);
    }

    void
    components::Navigator::stopAll(const Ice::Current&)
    {
        ARMARX_IMPORTANT << "stopAll()";

        ARMARX_TRACE;
        for (auto& [_, navigator] : navigators)
        {
            navigator.stop();
        }

        // emit UserAbortTriggered event
        const core::UserAbortTriggeredEvent event{{armarx::Clock::Now()},
                                                  core::Pose(scene().robot->getGlobalPose())};
        for (auto& [callerId, memoryPublisher] : memoryPublishers)
        {
            memoryPublisher->userAbortTriggered(event);
        }
    }

    bool
    components::Navigator::isPaused(const std::string& configId, const Ice::Current&)
    {
        ARMARX_TRACE;
        ARMARX_CHECK(navigators.count(configId) > 0)
            << "Navigator config for caller `" << configId << "` not registered!";
        return navigators.at(configId).isPaused();
    }

    const core::Scene&
    Navigator::scene() const
    {
        ARMARX_CHECK_NOT_NULL(sceneProvider);
        return sceneProvider->scene();
    }

    bool
    components::Navigator::isStopped(const std::string& configId, const Ice::Current&)
    {
        ARMARX_TRACE;
        ARMARX_CHECK(navigators.count(configId) > 0)
            << "Navigator config for caller `" << configId << "` not registered!";
        return navigators.at(configId).isStopped();
    }

    armarx::PropertyDefinitionsPtr
    components::Navigator::createPropertyDefinitions()
    {
        ARMARX_TRACE;

        PropertyDefinitionsPtr def = new ComponentPropertyDefinitions(getConfigIdentifier());

        // Publish to a topic (passing the TopicListenerPrx).
        // def->topic(myTopicListener);

        // Subscribe to a topic (passing the topic name).
        // def->topic<PlatformUnitListener>("MyTopic");

        // Use (and depend on) another component (passing the ComponentInterfacePrx).
        def->component(remoteGui, "RemoteGuiProvider");
        // Add a required property.
        // def->required(properties.boxLayerName, "p.box.LayerName", "Name of the box layer in ArViz.");

        // Add an optionalproperty.
        def->optional(params.occupiedGridThreshold,
                      "p.occupancy_grid.occopied_threshold",
                      "Threshold for each cell to be considered occupied. Increase this value to "
                      "reduce noise.");

        return def;
    }

    void
    visualize(const algorithms::Costmap& costmap, viz::Client& arviz, const std::string& name)
    {
        const auto cmap = simox::color::cmaps::viridis();
        const float vmax = costmap.getGrid().array().maxCoeff();

        const auto asColor = [&cmap, &vmax](const float distance) -> viz::data::Color
        {
            const auto color = cmap.at(distance, 0.F, vmax);
            return {color.a, color.r, color.g, color.b};
        };

        auto layer = arviz.layer("costmap_" + name);

        const std::int64_t cols = costmap.getGrid().cols();
        const std::int64_t rows = costmap.getGrid().rows();

        auto mesh = viz::Mesh("");

        std::vector<std::vector<Eigen::Vector3f>> vertices;
        std::vector<std::vector<viz::data::Color>> colors;

        for (int r = 0; r < rows; r++)
        {
            auto& verticesRow = vertices.emplace_back(cols);
            auto& colorsRow = colors.emplace_back(cols);

            for (int c = 0; c < cols; c++)
            {
                verticesRow.at(c) = conv::to3D(costmap.toPositionGlobal({r, c}));
                colorsRow.at(c) = asColor(costmap.getGrid()(r, c));
            }
        }

        mesh.grid2D(vertices, colors);

        layer.add(mesh);

        arviz.commit(layer);
    }

    server::Navigator*
    components::Navigator::activeNavigator()
    {
        ARMARX_TRACE;
        // We define the active navigator to be the one whose movement is enabled.
        const auto isActive = [](auto& nameNavPair) -> bool
        {
            server::Navigator& navigator = nameNavPair.second;
            return not navigator.isPaused();
        };

        auto it = std::find_if(navigators.begin(), navigators.end(), isActive);

        // no navigator active?
        if (it == navigators.end())
        {
            return nullptr;
        }

        return &it->second;
    }

} // namespace armarx::navigation::components
