#include "RemoteGui.h"

#include <cmath>
#include <mutex>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/time/CycleUtil.h>
#include <ArmarXCore/statechart/ParameterMapping.h>
#include <ArmarXCore/util/CPPUtility/trace.h>

#include "ArmarXGui/libraries/RemoteGui/Client/Widgets.h"
#include "ArmarXGui/libraries/RemoteGui/WidgetBuilder/IntegerWidgets.h"
#include "ArmarXGui/libraries/RemoteGui/WidgetBuilder/LayoutWidgets.h"
#include "ArmarXGui/libraries/RemoteGui/WidgetBuilder/StaticWidgets.h"
#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>

#include "Navigator.h"
#include <armarx/navigation/global_planning/SPFA.h>
#include <armarx/navigation/client/NavigationStackConfig.h>
#include <armarx/navigation/core/types.h>
#include <armarx/navigation/factories/NavigationStackFactory.h>
#include <armarx/navigation/global_planning/AStar.h>
#include <armarx/navigation/global_planning/Point2Point.h>
#include <armarx/navigation/server/Navigator.h>
#include <armarx/navigation/trajectory_control/TrajectoryFollowingController.h>
#include <armarx/navigation/util/util.h>

namespace armarx::navigation::components
{
    namespace gui = RemoteGui;

    NavigatorRemoteGui::NavigatorRemoteGui(const RemoteGuiInterfacePrx& remoteGui,
                                           Navigator& navigator) :
        remoteGui(remoteGui),
        runningTask(new RunningTask<NavigatorRemoteGui>(this, &NavigatorRemoteGui::run)),
        navigator(navigator)
    {
        createRemoteGuiTab();

        tabPrx = gui::TabProxy(remoteGui, REMOTE_GUI_TAB_MAME);

        runningTask->start();
    }

    NavigatorRemoteGui::~NavigatorRemoteGui()
    {
        shutdown();
    }

    void
    NavigatorRemoteGui::createRemoteGuiTab()
    {
        ARMARX_TRACE;

        ARMARX_CHECK_NOT_NULL(remoteGui); // createRemoteGui() has to be called first

        ARMARX_TRACE;
        using namespace armarx::RemoteGui::Client;

        // Setup the widgets.

        // every widget needs a unique id.
        int id = 0;

        {
            tab.targetPoseGroup.group.setLabel("Target pose");

            {
                float max = 10'000;
                tab.targetPoseGroup.targetPoseX.setRange(-max, max);
                tab.targetPoseGroup.targetPoseX.setDecimals(0);
                tab.targetPoseGroup.targetPoseX.setSteps(int(max / 10));
                tab.targetPoseGroup.targetPoseX.setValue(0.F);
            }

            {
                float max = 10'000;
                tab.targetPoseGroup.targetPoseY.setRange(-max, max);
                tab.targetPoseGroup.targetPoseY.setDecimals(0);
                tab.targetPoseGroup.targetPoseY.setSteps(int(max / 10));
                tab.targetPoseGroup.targetPoseY.setValue(0.F);
            }

            {
                float max = 10'000;
                tab.targetPoseGroup.targetPoseZ.setRange(-max, max);
                tab.targetPoseGroup.targetPoseZ.setDecimals(0);
                tab.targetPoseGroup.targetPoseZ.setSteps(int(max / 10));
                tab.targetPoseGroup.targetPoseZ.setValue(0.F);
            }

            {
                float max = 180;
                tab.targetPoseGroup.targetPoseRoll.setRange(-max, max);
                tab.targetPoseGroup.targetPoseRoll.setDecimals(0);
                tab.targetPoseGroup.targetPoseRoll.setSteps(2 * max);
                tab.targetPoseGroup.targetPoseRoll.setValue(0.F);
            }

            {
                float max = 180;
                tab.targetPoseGroup.targetPosePitch.setRange(-max, max);
                tab.targetPoseGroup.targetPosePitch.setDecimals(0);
                tab.targetPoseGroup.targetPosePitch.setSteps(2 * max);
                tab.targetPoseGroup.targetPosePitch.setValue(0.F);
            }

            {
                float max = 180;
                tab.targetPoseGroup.targetPoseYaw.setRange(-max, max);
                tab.targetPoseGroup.targetPoseYaw.setDecimals(0);
                tab.targetPoseGroup.targetPoseYaw.setSteps(2 * max);
                tab.targetPoseGroup.targetPoseYaw.setValue(0.F);
            }

            // Setup the layout.

            GridLayout grid;
            {

                grid.add(Label("X"), {id, 0}).add(tab.targetPoseGroup.targetPoseX, {id, 1});
                ++id;

                grid.add(Label("Y"), {id, 0}).add(tab.targetPoseGroup.targetPoseY, {id, 1});
                ++id;

                grid.add(Label("Z"), {id, 0}).add(tab.targetPoseGroup.targetPoseZ, {id, 1});
                ++id;

                grid.add(armarx::RemoteGui::Client::VSpacer(), {id, 1});
                ++id;

                grid.add(Label("Roll"), {id, 0}).add(tab.targetPoseGroup.targetPoseRoll, {id, 1});
                ++id;

                grid.add(Label("Pitch"), {id, 0}).add(tab.targetPoseGroup.targetPosePitch, {id, 1});
                ++id;

                grid.add(Label("Yaw"), {id, 0}).add(tab.targetPoseGroup.targetPoseYaw, {id, 1});
                ++id;
            }

            VBoxLayout root = {grid, VSpacer()};

            tab.targetPoseGroup.group.addChild(root);
        }

        {
            tab.controlGroup.group.setLabel("Control");

            tab.controlGroup.moveToButton.setLabel("MoveTo");
            tab.controlGroup.continueButton.setLabel("Continue");
            tab.controlGroup.pauseButton.setLabel("Pause");
            tab.controlGroup.stopButton.setLabel("Stop");
            tab.controlGroup.stopAllButton.setLabel("StopAll");

            GridLayout grid;
            {
                grid.add(tab.controlGroup.moveToButton, {id, 1});
                ++id;

                grid.add(armarx::RemoteGui::Client::VSpacer(), {id, 1});
                ++id;

                grid.add(tab.controlGroup.pauseButton, {id, 1});
                ++id;

                grid.add(tab.controlGroup.continueButton, {id, 1});
                ++id;

                grid.add(tab.controlGroup.stopButton, {id, 1});
                ++id;

                grid.add(armarx::RemoteGui::Client::VSpacer(), {id, 1});
                ++id;

                grid.add(tab.controlGroup.stopAllButton, {id, 1});
                ++id;
            }

            VBoxLayout root = {grid, VSpacer()};
            tab.controlGroup.group.addChild(root);
        }

        ARMARX_TRACE;

        tab.hbox.addChild(tab.targetPoseGroup.group);
        tab.hbox.addChild(tab.controlGroup.group);

        tab.create(REMOTE_GUI_TAB_MAME, remoteGui, tab.hbox);
    }

    void
    NavigatorRemoteGui::run()
    {
        constexpr int kCycleDurationMs = 100;

        CycleUtil c(kCycleDurationMs);
        while (!runningTask->isStopped())
        {
            {

                {
                    std::lock_guard g{mtx};

                    ARMARX_TRACE;
                    tab.receiveUpdates();
                    tabPrx.receiveUpdates();
                }

                ARMARX_TRACE;
                handleEvents(tabPrx);

                {
                    std::lock_guard g{mtx};

                    ARMARX_TRACE;
                    tab.sendUpdates();
                    tabPrx.sendUpdates();
                }
            }

            c.waitForCycleDuration();
        }
    }

    void
    NavigatorRemoteGui::handleEvents(RemoteGui::TabProxy& tab3)
    {
        ARMARX_TRACE;

        const std::string configId = "NavigatorRemoteGui";

        if (tab.controlGroup.moveToButton.wasClicked())
        {
            ARMARX_INFO << "MoveTo from RemoteGUI requested";

            // const auto& scene = navigator.getScene();

            // server::NavigationStack stack
            // {
            // .globalPlanner =
            // std::make_shared<global_planning::Point2Point>(global_planning::Point2PointParams(), scene),
            // .trajectoryControl = std::make_shared<traj_ctrl::TrajectoryFollowingController>(
            //     traj_ctrl::TrajectoryFollowingControllerParams(), scene)};

            // server::NavigationStack stack
            // {
            //     .globalPlanner =
            //     std::make_shared<global_planning::AStar>(global_planning::AStarParams(), scene),
            //     .trajectoryControl = std::make_shared<traj_ctrl::TrajectoryFollowingController>(
            //         traj_ctrl::TrajectoryFollowingControllerParams(), scene)};

            client::NavigationStackConfig cfg;
            cfg.globalPlanner(global_planning::SPFAParams());
            cfg.trajectoryController(traj_ctrl::TrajectoryFollowingControllerParams());

            const auto stackConfig = cfg.toAron();

            ARMARX_TRACE;

            navigator.createConfig(stackConfig, "NavigatorRemoteGui");

            const Eigen::Vector3f targetPos{tab.targetPoseGroup.targetPoseX.getValue(),
                                            tab.targetPoseGroup.targetPoseY.getValue(),
                                            tab.targetPoseGroup.targetPoseZ.getValue()};

            const Eigen::Vector3f targetOri{tab.targetPoseGroup.targetPoseRoll.getValue(),
                                            tab.targetPoseGroup.targetPosePitch.getValue(),
                                            tab.targetPoseGroup.targetPoseYaw.getValue()};

            const core::Pose targetPose = core::Pose(Eigen::Translation3f(targetPos)) *
                                          core::Pose(simox::math::rpy_to_mat3f(targetOri));

            std::vector<Eigen::Matrix4f> waypoints;
            waypoints.emplace_back(targetPose.matrix());

            ARMARX_TRACE;

            try
            {
                navigator.moveTo(
                    waypoints,
                    core::NavigationFrameNames.to_name(core::NavigationFrame::Absolute),
                    configId);
            }
            catch (const armarx::LocalException& ex)
            {
                ARMARX_WARNING << LogSender::eYellow
                               << "Failed moving to target. Reason: " << GetHandledExceptionString()
                               << LogSender::eReset;
            }
        };

        if (tab.controlGroup.pauseButton.wasClicked())
        {
            ARMARX_INFO << "pauseMovement() from RemoteGUI requested";
            navigator.pause(configId);
        }

        if (tab.controlGroup.continueButton.wasClicked())
        {
            ARMARX_INFO << "resumeMovement() from RemoteGUI requested";
            navigator.resume(configId);
        }

        if (tab.controlGroup.stopButton.wasClicked())
        {
            ARMARX_INFO << "stop() from RemoteGUI requested";
            navigator.stop(configId);
        }

        if (tab.controlGroup.stopAllButton.wasClicked())
        {
            ARMARX_INFO << "stop() from RemoteGUI requested";
            navigator.stopAll();
        }
    }

    void
    NavigatorRemoteGui::shutdown()
    {
        // std::lock_guard g{mtx};

        if (!runningTask->isStopped())
        {
            runningTask->stop();
        }

        ARMARX_DEBUG << "Removing tab: " << REMOTE_GUI_TAB_MAME;
        remoteGui->removeTab(REMOTE_GUI_TAB_MAME);
    }

    void
    NavigatorRemoteGui::enable()
    {
        ARMARX_TRACE;

        // std::lock_guard g{mtx};
        tabPrx.internalSetDisabled(tab.controlGroup.moveToButton.getName(), false);
        tabPrx.internalSetDisabled(tab.controlGroup.pauseButton.getName(), false);
        tabPrx.internalSetDisabled(tab.controlGroup.continueButton.getName(), false);
        tabPrx.internalSetDisabled(tab.controlGroup.stopButton.getName(), false);
        tabPrx.internalSetDisabled(tab.controlGroup.stopAllButton.getName(), false);
    }

    void
    NavigatorRemoteGui::disable()
    {
        ARMARX_TRACE;

        // std::unique_lock g{mtx};

        // it might happen, that this function is triggered from handleEvents
        // std::unique_lock ul{handleEventsMtx};
        // ul.try_lock();

        tabPrx.internalSetDisabled(tab.controlGroup.moveToButton.getName(), true);
        tabPrx.internalSetDisabled(tab.controlGroup.pauseButton.getName(), true);
        tabPrx.internalSetDisabled(tab.controlGroup.continueButton.getName(), true);
        tabPrx.internalSetDisabled(tab.controlGroup.stopButton.getName(), true);
        tabPrx.internalSetDisabled(tab.controlGroup.stopAllButton.getName(), true);
    }

    void
    NavigatorRemoteGui::reset()
    {
        ARMARX_TRACE;

        // std::lock_guard g{mtx};

        tab.targetPoseGroup.targetPoseX.setValue(0.F);
        tab.targetPoseGroup.targetPoseY.setValue(0.F);
        tab.targetPoseGroup.targetPoseZ.setValue(0.F);
        tab.targetPoseGroup.targetPoseRoll.setValue(0.F);
        tab.targetPoseGroup.targetPosePitch.setValue(0.F);
        tab.targetPoseGroup.targetPoseYaw.setValue(0.F);

        tab.sendUpdates();
        tab.sendUpdates();
    }

} // namespace armarx::navigation::components
