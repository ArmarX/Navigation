#pragma once

#include <armarx/navigation/client/ice_conversions.h>
#include <armarx/navigation/core/types.h>

namespace armarx::navigation::core
{

    class NavigatorInterface
    {

    public:
        virtual void moveTo(const std::vector<core::Pose>& waypoints,
                            core::NavigationFrame navigationFrame) = 0;

        virtual void moveTowards(const core::Direction& direction,
                                 core::NavigationFrame navigationFrame) = 0;

        virtual void moveTo(const std::vector<client::WaypointTarget>& targets,
                            core::NavigationFrame navigationFrame) = 0;


        virtual void pause() = 0;

        virtual void resume() = 0;

        virtual void stop() = 0;

        virtual bool isPaused() const noexcept = 0;

        virtual bool isStopped() const noexcept = 0;

        // Non-API
    public:
        virtual ~NavigatorInterface() = default;
    };

} // namespace armarx::navigation::core
