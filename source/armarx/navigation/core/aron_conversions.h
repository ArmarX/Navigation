/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <armarx/navigation/core/Trajectory.h>
#include <armarx/navigation/core/aron/Events.aron.generated.h>
#include <armarx/navigation/core/aron/PIDParams.aron.generated.h>
#include <armarx/navigation/core/aron/Trajectory.aron.generated.h>
#include <armarx/navigation/core/aron/Twist.aron.generated.h>
#include <armarx/navigation/core/aron/TwistLimits.aron.generated.h>
#include <armarx/navigation/core/events.h>

namespace armarx::navigation::core
{
    // TODO fix ADL and remove this function
    template <class DtoT, class BoT>
    DtoT
    toAron(const BoT& bo)
    {
        DtoT dto;
        toAron(dto, bo);
        return dto;
    }

    void toAron(arondto::TrajectoryPoint& dto, const TrajectoryPoint& bo);
    void fromAron(const arondto::TrajectoryPoint& dto, TrajectoryPoint& bo);

    void toAron(arondto::Trajectory& dto, const Trajectory& bo);
    void fromAron(const arondto::Trajectory& dto, Trajectory& bo);

    void toAron(arondto::Twist& dto, const Twist& bo);
    void fromAron(const arondto::Twist& dto, Twist& bo);

    void toAron(armarx::navigation::core::arondto::PIDParams& dto,
                const armarx::navigation::core::PIDParams& bo);
    void fromAron(const armarx::navigation::core::arondto::PIDParams& dto,
                  armarx::navigation::core::PIDParams& bo);

    void toAron(armarx::navigation::core::arondto::TwistLimits& dto,
                const armarx::navigation::core::TwistLimits& bo);
    void fromAron(const armarx::navigation::core::arondto::TwistLimits& dto,
                  armarx::navigation::core::TwistLimits& bo);

    // Events
    void toAron(armarx::navigation::core::arondto::GoalReachedEvent& dto,
                const armarx::navigation::core::GoalReachedEvent& bo);

    void fromAron(const armarx::navigation::core::arondto::GoalReachedEvent& dto,
                  armarx::navigation::core::GoalReachedEvent& bo);

    void toAron(armarx::navigation::core::arondto::MovementStartedEvent& dto,
                const armarx::navigation::core::MovementStartedEvent& bo);

    void fromAron(const armarx::navigation::core::arondto::GlobalPlanningFailedEvent& dto,
                  armarx::navigation::core::GlobalPlanningFailedEvent& bo);

    void toAron(armarx::navigation::core::arondto::GlobalPlanningFailedEvent& dto,
                const armarx::navigation::core::GlobalPlanningFailedEvent& bo);

    void fromAron(const armarx::navigation::core::arondto::MovementStartedEvent& dto,
                  armarx::navigation::core::MovementStartedEvent& bo);

    void toAron(armarx::navigation::core::arondto::WaypointReachedEvent& dto,
                const armarx::navigation::core::WaypointReachedEvent& bo);

    void fromAron(const armarx::navigation::core::arondto::WaypointReachedEvent& dto,
                  armarx::navigation::core::WaypointReachedEvent& bo);

    void toAron(armarx::navigation::core::arondto::InternalErrorEvent& dto,
                const armarx::navigation::core::InternalErrorEvent& bo);

    void fromAron(const armarx::navigation::core::arondto::InternalErrorEvent& dto,
                  armarx::navigation::core::InternalErrorEvent& bo);

    void toAron(armarx::navigation::core::arondto::UserAbortTriggeredEvent& dto,
                const armarx::navigation::core::UserAbortTriggeredEvent& bo);

    void fromAron(const armarx::navigation::core::arondto::UserAbortTriggeredEvent& dto,
                  armarx::navigation::core::UserAbortTriggeredEvent& bo);

    void toAron(armarx::navigation::core::arondto::SafetyThrottlingTriggeredEvent& dto,
                const armarx::navigation::core::SafetyThrottlingTriggeredEvent& bo);

    void fromAron(const armarx::navigation::core::arondto::SafetyThrottlingTriggeredEvent& dto,
                  armarx::navigation::core::SafetyThrottlingTriggeredEvent& bo);

    void toAron(armarx::navigation::core::arondto::SafetyStopTriggeredEvent& dto,
                const armarx::navigation::core::SafetyStopTriggeredEvent& bo);

    void fromAron(const armarx::navigation::core::arondto::SafetyStopTriggeredEvent& dto,
                  armarx::navigation::core::SafetyStopTriggeredEvent& bo);


} // namespace armarx::navigation::core
