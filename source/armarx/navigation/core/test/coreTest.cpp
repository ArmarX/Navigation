/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Navigation::ArmarXObjects::core
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <algorithm>
#include <vector>

#include <range/v3/view/zip.hpp>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <SemanticObjectRelations/Shapes/Shape.h>
#include <armarx/navigation/core/Graph.h>
#include <armarx/navigation/core/Trajectory.h>
#include <armarx/navigation/core/types.h>

// test includes and other stuff
#define BOOST_TEST_MODULE Navigation::ArmarXLibraries::core
#define ARMARX_BOOST_TEST

#include <armarx/navigation/Test.h>

BOOST_AUTO_TEST_CASE(testPathLength)
{
    armarx::navigation::core::Path path{
        armarx::navigation::core::Pose(Eigen::Translation3f(0, 0, 0)),
        armarx::navigation::core::Pose(Eigen::Translation3f(0, 2000, 0)),
        armarx::navigation::core::Pose(Eigen::Translation3f(0, 4000, 0))};

    const auto traj = armarx::navigation::core::Trajectory::FromPath(path, 100);
    BOOST_CHECK_CLOSE(traj.length(), 4000, 0.01);
}

BOOST_AUTO_TEST_CASE(testResampleAlongLine)
{
    armarx::navigation::core::Path path{
        armarx::navigation::core::Pose(Eigen::Translation3f(0, 0, 0)),
        armarx::navigation::core::Pose(Eigen::Translation3f(0, 2000, 0))};

    const auto traj = armarx::navigation::core::Trajectory::FromPath(path, 100);
    BOOST_CHECK_EQUAL(traj.points().size(), 2);

    const auto resampledTraj = traj.resample(500);

    for (const auto& pt : resampledTraj.positions())
    {
        ARMARX_DEBUG << VAROUT(pt);
    }

    BOOST_CHECK_EQUAL(resampledTraj.points().size(), 4);
}

BOOST_AUTO_TEST_CASE(testResampleAlongLineWithWaypoint)
{
    armarx::navigation::core::Path path{
        armarx::navigation::core::Pose(Eigen::Translation3f(0, 0, 0)),
        armarx::navigation::core::Pose(Eigen::Translation3f(0, 1050, 0)),
        armarx::navigation::core::Pose(Eigen::Translation3f(0, 2100, 0))};

    const auto traj = armarx::navigation::core::Trajectory::FromPath(path, 100);
    BOOST_CHECK_EQUAL(traj.points().size(), 3);

    const auto resampledTraj = traj.resample(500);

    for (const auto& pt : resampledTraj.positions())
    {
        ARMARX_DEBUG << VAROUT(pt);
    }

    BOOST_CHECK_EQUAL(resampledTraj.points().size(), 5);
}


BOOST_AUTO_TEST_CASE(testGraphRoutes)
{
    using namespace armarx::navigation;

    /*
    *
    *   (0) -> (2) -> (3)
    *           ^
    *           |
    *          (1)
    */

    core::Graph graph;
    graph.addVertex(semrel::ShapeID(0));
    graph.addVertex(semrel::ShapeID(1));
    graph.addVertex(semrel::ShapeID(2));
    graph.addVertex(semrel::ShapeID(3));

    graph.addEdge(semrel::ShapeID(0), semrel::ShapeID(2));
    graph.addEdge(semrel::ShapeID(1), semrel::ShapeID(2));
    graph.addEdge(semrel::ShapeID(2), semrel::ShapeID(3));

    const auto paths =
        armarx::navigation::core::findPathsTo(graph.vertex(semrel::ShapeID(3)), graph);

    // GT: paths
    // - {3}
    // - {2,3}
    // - {0,2,3}
    // - {1,2,3}

    BOOST_CHECK_EQUAL(paths.size(), 4);

    std::vector<std::vector<int>> gtPaths{{3}, {2, 3}, {0, 2, 3}, {1, 2, 3}};

    const auto isMatchingPath = [](const auto& path, const auto& gtPath)
    {
        if (path.size() != gtPath.size())
        {
            return false;
        }

        // check that sequences match
        for (const auto& [v, gtId] : ranges::views::zip(path, gtPath))
        {
            if (v.objectID().t != gtId)
            {
                return false;
            }
        }

        ARMARX_INFO << "Found matching path: " << VAROUT(path) << ", " << VAROUT(gtPath);
        return true;
    };

    // check that all required paths are available
    for (const auto& gtPath : gtPaths)
    {
        BOOST_CHECK(std::any_of(paths.begin(),
                                paths.end(),
                                [&gtPath, &isMatchingPath](const auto& path)
                                { return isMatchingPath(path, gtPath); }));
    }
}
