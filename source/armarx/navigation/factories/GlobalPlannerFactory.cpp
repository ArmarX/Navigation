#include "GlobalPlannerFactory.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>
#include <RobotAPI/libraries/aron/core/data/variant/primitive/String.h>

#include <armarx/navigation/core/constants.h>
#include <armarx/navigation/global_planning/AStar.h>
#include <armarx/navigation/global_planning/Point2Point.h>
#include <armarx/navigation/global_planning/SPFA.h>
#include <armarx/navigation/global_planning/core.h>

namespace armarx::navigation::fac
{
    global_planning::GlobalPlannerPtr
    GlobalPlannerFactory::create(const aron::data::DictPtr& params, const core::Scene& ctx)
    {
        namespace layer = global_planning;

        if (not params)
        {
            return nullptr;
        }

        // algo name
        const auto algoName = aron::data::String::DynamicCast(params->getElement(core::NAME_KEY));
        ARMARX_CHECK_NOT_NULL(algoName);
        const layer::Algorithms algo = layer::AlgorithmNames.from_name(algoName->getValue());

        // algo params
        const auto algoParams = aron::data::Dict::DynamicCast(params->getElement(core::PARAMS_KEY));
        ARMARX_CHECK_NOT_NULL(algoParams);

        global_planning::GlobalPlannerPtr globalPlanner;
        switch (algo)
        {
            case global_planning::Algorithms::AStar:
                globalPlanner = std::make_shared<global_planning::AStar>(
                    global_planning::AStarParams::FromAron(algoParams), ctx);
                break;
            case global_planning::Algorithms::SPFA:
                globalPlanner = std::make_shared<global_planning::SPFA>(
                    global_planning::SPFAParams::FromAron(algoParams), ctx);
                break;
            case global_planning::Algorithms::Point2Point:
                globalPlanner = std::make_shared<global_planning::Point2Point>(
                    global_planning::Point2PointParams::FromAron(algoParams), ctx);
                break;
        }

        return globalPlanner;
    }

} // namespace armarx::navigation::fac
