#include "SafetyControllerFactory.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>
#include <RobotAPI/libraries/aron/core/data/variant/primitive/String.h>

#include <armarx/navigation/core/constants.h>
#include <armarx/navigation/safety_control/LaserBasedProximity.h>
#include <armarx/navigation/safety_control/SafetyController.h>
#include <armarx/navigation/safety_control/core.h>

namespace armarx::navigation::fac
{
    safe_ctrl::SafetyControllerPtr
    SafetyControllerFactory::create(const aron::data::DictPtr& params, const core::Scene& ctx)
    {
        namespace layer = safe_ctrl;

        if (not params)
        {
            return nullptr;
        }

        // algo name
        const auto algoName = aron::data::String::DynamicCast(params->getElement(core::NAME_KEY));
        ARMARX_CHECK_NOT_NULL(algoName);
        const layer::Algorithms algo = layer::AlgorithmNames.from_name(algoName->getValue());

        // algo params
        const auto algoParams = aron::data::Dict::DynamicCast(params->getElement(core::PARAMS_KEY));
        ARMARX_CHECK_NOT_NULL(algoParams);

        safe_ctrl::SafetyControllerPtr controller;
        switch (algo)
        {
            case safe_ctrl::Algorithms::LaserBasedProximity:
                controller = std::make_shared<safe_ctrl::LaserBasedProximity>(
                    safe_ctrl::LaserBasedProximityParams::FromAron(algoParams), ctx);
                break;
        }

        return controller;
    }
} // namespace armarx::navigation::fac
