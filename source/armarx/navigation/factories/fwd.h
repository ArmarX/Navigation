/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <memory>

namespace armarx::navigation
{

    namespace global_planning
    {
        class GlobalPlanner;
        using GlobalPlannerPtr = std::shared_ptr<GlobalPlanner>;
    } // namespace global_planning

    namespace loc_plan
    {
        class LocalPlanner;
        using LocalPlannerPtr = std::shared_ptr<LocalPlanner>;
    } // namespace loc_plan

    namespace traj_ctrl
    {
        class TrajectoryController;
        using TrajectoryControllerPtr = std::shared_ptr<TrajectoryController>;
    } // namespace traj_ctrl

    namespace safe_ctrl
    {
        class SafetyController;
        using SafetyControllerPtr = std::shared_ptr<SafetyController>;
    } // namespace safe_ctrl

} // namespace armarx::navigation
