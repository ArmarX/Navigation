/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/MathTools.h>
#include "GlobalPlanner.h"
#include <armarx/navigation/core/types.h>

namespace armarx::navigation::global_planning
{

    /**
     * @brief Parameters for AStar
     *
     */
    struct SPFAParams : public GlobalPlannerParams
    {
        float linearVelocity{500};
        float angularVelocity{2.F * M_PIf32 / 30.F };

        //  core::TwistLimits limits{
        //     .linear = 500.F,                // [mm/s]
        //     .angular = 2.F * M_PIf32 / 30.F // [rad/s]
        // };

        float resampleDistance{-1};



        Algorithms algorithm() const override;
        aron::data::DictPtr toAron() const override;

        static SPFAParams FromAron(const aron::data::DictPtr& dict);
    };

    /**
    * @class AStar
    * @ingroup Library-GlobalPlanner
    *
    * Implements the A* algorithm
    */
    class SPFA : public GlobalPlanner
    {
    public:
        using Params = SPFAParams;

        SPFA(const Params& params, const core::Scene& ctx);
        ~SPFA() override = default;

        std::optional<GlobalPlannerResult> plan(const core::Pose& goal) override;
        std::optional<GlobalPlannerResult> plan(const core::Pose& start,
                                                const core::Pose& goal) override;

    protected:
        std::vector<Eigen::Vector2f> postProcessPath(const std::vector<Eigen::Vector2f>& path);

    private:
        Params params;
    };

} // namespace armarx::navigation::global_planning
