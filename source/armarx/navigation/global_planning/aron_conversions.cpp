#include "aron_conversions.h"

#include <RobotAPI/libraries/aron/common/aron_conversions.h>
#include <RobotAPI/libraries/aron/common/aron_conversions/core.h>
#include <RobotAPI/libraries/aron/common/aron_conversions/stl.h>

#include <armarx/navigation/global_planning/AStar.h>
#include <armarx/navigation/global_planning/GlobalPlanner.h>
#include <armarx/navigation/global_planning/Point2Point.h>
#include <armarx/navigation/global_planning/SPFA.h>
#include <armarx/navigation/global_planning/aron/AStarParams.aron.generated.h>
#include <armarx/navigation/global_planning/aron/GlobalPlannerParams.aron.generated.h>
#include <armarx/navigation/global_planning/aron/Point2PointParams.aron.generated.h>
#include <armarx/navigation/global_planning/aron/SPFAParams.aron.generated.h>

namespace armarx::navigation::global_planning::aron_conv
{
    void
    toAron(arondto::GlobalPlannerParams& dto, const GlobalPlannerParams& bo)
    {
    }

    void
    fromAron(const arondto::GlobalPlannerParams& dto, GlobalPlannerParams& bo)
    {
    }

    void
    toAron(arondto::Point2PointParams& dto, const Point2PointParams& bo)
    {
        // fromAron(static_cast<const arondto::GlobalPlannerParams&>(dto), static_cast<const GlobalPlannerParams&>(bo));
        aron::toAron(dto.includeStartPose, bo.includeStartPose);
        aron::toAron(dto.velocity, bo.velocity);
    }

    void
    fromAron(const arondto::Point2PointParams& dto, Point2PointParams& bo)
    {
        // fromAron(static_cast<arondto::GlobalPlannerParams&>(dto), static_cast<GlobalPlannerParams&>(bo));
        aron::fromAron(dto.includeStartPose, bo.includeStartPose);
        aron::fromAron(dto.velocity, bo.velocity);
    }

    void
    toAron(arondto::AStarParams& dto, const AStarParams& bo)
    {
        // toAron(static_cast<arondto::GlobalPlannerParams&>(dto),
        //        static_cast<const GlobalPlannerParams&>(bo));

        aron::toAron(dto.linearVelocity, bo.linearVelocity);
        aron::toAron(dto.resampleDistance, bo.resampleDistance);
    }

    void
    fromAron(const arondto::AStarParams& dto, AStarParams& bo)
    {
        // fromAron(static_cast<const arondto::GlobalPlannerParams&>(dto),
        //          static_cast<GlobalPlannerParams&>(bo));

        aron::fromAron(dto.linearVelocity, bo.linearVelocity);
        aron::fromAron(dto.resampleDistance, bo.resampleDistance);
    }

    void
    toAron(arondto::SPFAParams& dto, const SPFAParams& bo)
    {
        // toAron(static_cast<arondto::GlobalPlannerParams&>(dto),
        //        static_cast<const GlobalPlannerParams&>(bo));

        aron::toAron(dto.linearVelocity, bo.linearVelocity);
        aron::toAron(dto.resampleDistance, bo.resampleDistance);
    }

    void
    fromAron(const arondto::SPFAParams& dto, SPFAParams& bo)
    {
        // fromAron(static_cast<const arondto::GlobalPlannerParams&>(dto),
        //          static_cast<GlobalPlannerParams&>(bo));

        aron::fromAron(dto.linearVelocity, bo.linearVelocity);
        aron::fromAron(dto.resampleDistance, bo.resampleDistance);
    }

} // namespace armarx::navigation::global_planning::aron_conv
