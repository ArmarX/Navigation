#include "OrientationOptimizer.h"

#include <algorithm>
#include <cmath>

#include <range/v3/range/conversion.hpp>
#include <range/v3/view/drop.hpp>
#include <range/v3/view/drop_last.hpp>
#include <range/v3/view/transform.hpp>
#include <range/v3/view/zip.hpp>

#include <ceres/ceres.h>
#include <ceres/cost_function.h>
#include <ceres/jet.h>
#include <ceres/rotation.h>
#include <ceres/sized_cost_function.h>

#include <Eigen/src/Geometry/Rotation2D.h>

#include <SimoxUtility/math/convert/mat4f_to_xyyaw.h>
#include <SimoxUtility/math/convert/rpy_to_mat3f.h>
#include <SimoxUtility/math/periodic/periodic_clamp.h>
#include <SimoxUtility/math/periodic/periodic_diff.h>
#include <VirtualRobot/Random.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <armarx/navigation/core/Trajectory.h>
#include <armarx/navigation/global_planning/optimization/cost_functions.h>

namespace armarx::navigation::global_planning::optimization
{
    OrientationOptimizer::OrientationOptimizer(const core::Trajectory& trajectory,
                                               const Params& params) :
        trajectory(trajectory), params(params)
    {
    }


    OrientationOptimizer::OptimizationResult
    OrientationOptimizer::optimize()
    {

        namespace r = ::ranges;
        namespace rv = ::ranges::views;

        const auto toYaw = [](const core::TrajectoryPoint& pt) -> double
        { return simox::math::mat4f_to_xyyaw(pt.waypoint.pose.matrix()).z(); };

        std::vector<double> orientations =
            trajectory.points() | ranges::views::transform(toYaw) | ranges::to_vector;

        const std::vector<double> orientationsPrior = orientations;

        for (std::size_t i = 1; i < orientations.size() - 1; i++)
        {
            const float orientationPrior = orientationsPrior.at(i);

            const float startAttractorFactor = 1 - static_cast<float>(i) / orientations.size();
            const float goalAttractorFactor = static_cast<float>(i) / orientations.size();

            const Eigen::Vector2f vPrior =
                0.8 * (Eigen::Rotation2Df(orientationPrior) * Eigen::Vector2f::UnitX());
            const Eigen::Vector2f vStart =
                0.1 * startAttractorFactor *
                (Eigen::Rotation2Df(orientations.front()) * Eigen::Vector2f::UnitX());
            const Eigen::Vector2f vGoal =
                0.1 * goalAttractorFactor *
                (Eigen::Rotation2Df(orientations.back()) * Eigen::Vector2f::UnitX());

            const Eigen::Vector2f vCombined = vPrior + vStart + vGoal;

            orientations.at(i) = std::atan2(vCombined.y(), vCombined.x());
        }

        ARMARX_INFO << "orientations before: " << orientations;

        // const float yawStart = orientations.front();
        // const float yawEnd = orientations.back();

        // //
        // const auto yawsVariable =
        //     orientations | ranges::views::drop(1) | ranges::views::drop_last(1) | ranges::to_vector;

        ceres::Problem problem;

        ARMARX_INFO << orientations.size() - 2 << " orientations to optimize";

        // TODO https://ceres-solver.googlesource.com/ceres-solver/+/master/examples/slam/pose_graph_2d/pose_graph_2d.cc
        //     ceres::LocalParameterization* angle_local_parameterization =
        //   ceres::AngleLocalParameterization::Create();

        // prior for all waypoints
        if (params.priorWeight > 0.F)
        {
            for (size_t i = 1; i < (orientations.size() - 1); i++)
            {
                ceres::CostFunction* priorCostFunction =
                    new OrientationPriorCostFunctor(orientationsPrior.at(i), params.priorWeight);

                problem.AddResidualBlock(priorCostFunction, nullptr, &orientations.at(i));
            }
        }


        // smooth waypoint orientation, no start and goal nodes involved!
        if (params.smoothnessWeight > 0.F)
        {
            ARMARX_VERBOSE << "Enabled SmoothOrientationCost";
            for (size_t i = 2; i < (orientations.size() - 2); i++)
            {
                ceres::CostFunction* smoothCostFunction =
                    new SmoothOrientationCostFunctor(params.smoothnessWeight);

                problem.AddResidualBlock(smoothCostFunction,
                                         nullptr,
                                         &orientations.at(i - 1),
                                         &orientations.at(i),
                                         &orientations.at(i + 1));
            }
        }

        // within a certain range close to the start, the robot shouldn't change its orientation
        if (params.priorStartWeight > 0.F)
        {
            const auto connectedPointsInRangeStart =
                trajectory.allConnectedPointsInRange(0, params.startGoalDistanceThreshold);

            for (const size_t i : connectedPointsInRangeStart)
            {
                // skip the points that belong to the second half of the trajectory
                if (i > orientations.size() / 2)
                {
                    continue;
                }

                ceres::CostFunction* priorCostFunction = new OrientationPriorCostFunctor(
                    orientationsPrior.front(), params.priorStartWeight);

                problem.AddResidualBlock(priorCostFunction, nullptr, &orientations.at(i));
            }
        }

        // within a certain range close to the end, the robot shouldn't change its orientation
        if (params.priorEndWeight > 0.F)
        {
            const auto connectedPointsInRangeGoal = trajectory.allConnectedPointsInRange(
                trajectory.poses().size() - 1, params.startGoalDistanceThreshold);

            for (const size_t i : connectedPointsInRangeGoal)
            {
                // skip the points that belong to the first half of the trajectory
                if (i < orientations.size() / 2)
                {
                    continue;
                }

                ceres::CostFunction* priorCostFunction = new OrientationPriorCostFunctor(
                    orientationsPrior.back(), params.priorEndWeight);

                problem.AddResidualBlock(priorCostFunction, nullptr, &orientations.at(i));
            }
        }


        // smooth waypoint orientation, involving start
        if (params.smoothnessWeightStartGoal > 0.F)
        {
            ARMARX_VERBOSE << "Enabled SmoothOrientationFixedPreCost";

            ceres::CostFunction* smoothCostFunction = new SmoothOrientationFixedPreCostFunctor(
                orientations.front(), params.smoothnessWeightStartGoal);

            problem.AddResidualBlock(
                smoothCostFunction, nullptr, &orientations.at(1), &orientations.at(2));
        }

        // smooth waypoint orientation, involving goal
        if (params.smoothnessWeightStartGoal > 0.F)
        {
            ARMARX_VERBOSE << "Enabled SmoothOrientationFixedNextCost";

            ceres::CostFunction* smoothCostFunction = new SmoothOrientationFixedNextCostFunctor(
                orientations.back(), params.smoothnessWeightStartGoal);

            problem.AddResidualBlock(smoothCostFunction,
                                     nullptr,
                                     &orientations.at(orientations.size() - 3),
                                     &orientations.at(orientations.size() - 2));
        }


        // Run the solver!
        ceres::Solver::Options options;
        options.linear_solver_type = ceres::DENSE_QR;
        options.minimizer_progress_to_stdout = true;
        options.max_num_iterations = 100;
        options.function_tolerance = 0.01;

        ceres::Solver::Summary summary;
        Solve(options, &problem, &summary);

        std::cout << summary.FullReport() << "\n";
        // std::cout << summary.BriefReport() << "\n";

        const auto clampInPlace = [](auto& val)
        { val = simox::math::periodic_clamp(val, -M_PI, M_PI); };

        std::for_each(orientations.begin(), orientations.end(), clampInPlace);

        ARMARX_INFO << "orientations after: " << orientations;
        ARMARX_INFO << "Optimization: " << summary.iterations.size() << " iterations";

        if (not summary.IsSolutionUsable())
        {
            ARMARX_ERROR << "Orientation optimization failed!";
            // TODO write to file
        }

        const auto applyOrientation = [](const auto& p) -> core::TrajectoryPoint
        {
            core::TrajectoryPoint tp = p.first;
            const float yaw = p.second;

            tp.waypoint.pose.linear() = simox::math::rpy_to_mat3f(0.F, 0.F, yaw);

            return tp;
        };

        // TODO(fabian.reister): could also be in-place
        const auto modifiedTrajectory = rv::zip(trajectory.points(), orientations) |
                                        rv::transform(applyOrientation) | r::to_vector;

        return OrientationOptimizer::OptimizationResult{.trajectory = modifiedTrajectory};
    }


} // namespace armarx::navigation::global_planning::optimization
