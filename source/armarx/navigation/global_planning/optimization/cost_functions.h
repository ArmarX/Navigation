/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ceres/ceres.h>

#include <armarx/navigation/global_planning/optimization/math.h>

namespace armarx::navigation::global_planning::optimization
{

    struct SmoothOrientationFixedPreCostFunctor : ceres::CostFunction
    {

        SmoothOrientationFixedPreCostFunctor(const double yawPre, const double weight) :
            yawPre(yawPre), weight(weight)
        {
            set_num_residuals(2);

            mutable_parameter_block_sizes()->push_back(1);
            mutable_parameter_block_sizes()->push_back(1);
        }


        bool
        Evaluate(double const* const* parameters,
                 double* residuals,
                 double** jacobians) const override
        {
            const double yaw = parameters[0][0];
            const double yawNext = parameters[0][1];

            residuals[0] = weight * periodicDiff(yawPre, yaw);
            residuals[1] = weight * periodicDiff(yaw, yawNext);

            const auto periodicDiffJ0 = periodicDiffJacobian(yawPre, yaw);
            const auto periodicDiffJ1 = periodicDiffJacobian(yaw, yawNext);

            if (jacobians == nullptr)
            {
                return true;
            }
            double* jacobian0 = jacobians[0];
            if (jacobian0 == nullptr)
            {
                return true;
            }

            double* jacobian1 = jacobians[1];
            if (jacobian1 == nullptr)
            {
                return true;
            }


            // d r_i / d p_0
            jacobian0[0] = weight * periodicDiffJ0.at(1);
            jacobian0[1] = weight * periodicDiffJ1.at(0);
            // jacobian0[2] = weight * periodicDiffJ1.at(0);

            // d r_i / d p_1
            jacobian1[0] = 0.;
            jacobian1[1] = weight * periodicDiffJ1.at(1);

            return true;
        }

    private:
        const double yawPre;
        const double weight;
    };

    struct SmoothOrientationFixedNextCostFunctor : ceres::CostFunction
    {

        SmoothOrientationFixedNextCostFunctor(const double yawNext, const double weight) :
            yawNext(yawNext), weight(weight)
        {
            set_num_residuals(2);

            mutable_parameter_block_sizes()->push_back(1);
            mutable_parameter_block_sizes()->push_back(1);
        }


        bool
        Evaluate(double const* const* parameters,
                 double* residuals,
                 double** jacobians) const override
        {
            const double yawPre = parameters[0][0];
            const double yaw = parameters[0][1];

            residuals[0] = weight * periodicDiff(yawPre, yaw);
            residuals[1] = weight * periodicDiff(yaw, yawNext);

            const auto periodicDiffJ0 = periodicDiffJacobian(yawPre, yaw);
            const auto periodicDiffJ1 = periodicDiffJacobian(yaw, yawNext);

            if (jacobians == nullptr)
            {
                return true;
            }
            double* jacobian0 = jacobians[0];
            if (jacobian0 == nullptr)
            {
                return true;
            }

            double* jacobian1 = jacobians[1];
            if (jacobian1 == nullptr)
            {
                return true;
            }

            // d r_i / d p_0
            jacobian0[0] = weight * periodicDiffJ0.at(0);
            jacobian0[1] = 0;

            // d r_i / d p_1
            jacobian1[0] = weight * periodicDiffJ0.at(1);
            jacobian1[1] = weight * periodicDiffJ1.at(0);

            return true;
        }

    private:
        const double yawNext;
        const double weight;
    };

    struct OrientationPriorCostFunctor : ceres::SizedCostFunction<1, 1>
    {

        OrientationPriorCostFunctor(const double prior, const double weight) :
            prior(prior), weight(weight)
        {
        }

        bool
        Evaluate(double const* const* parameters,
                 double* residuals,
                 double** jacobians) const override
        {
            const double yaw = parameters[0][0];

            residuals[0] = weight * periodicDiff(prior, yaw);

            const auto periodicDiffJ = periodicDiffJacobian(prior, yaw);

            if (jacobians == nullptr)
            {
                return true;
            }
            double* jacobian = jacobians[0];
            if (jacobian == nullptr)
            {
                return true;
            }


            jacobian[0] = weight * periodicDiffJ.at(1);
            return true;
        }

    private:
        const double prior;
        const double weight;
    };


    struct SmoothOrientationCostFunctor : ceres::CostFunction
    {

        SmoothOrientationCostFunctor(const double weight) : weight(weight)
        {
            set_num_residuals(2);

            mutable_parameter_block_sizes()->push_back(1);
            mutable_parameter_block_sizes()->push_back(1);
            mutable_parameter_block_sizes()->push_back(1);
        }

        bool
        Evaluate(double const* const* parameters,
                 double* residuals,
                 double** jacobians) const override
        {
            const double yawPrev = parameters[0][0];
            const double yaw = parameters[0][1];
            const double yawNext = parameters[0][2];

            residuals[0] = weight * periodicDiff(yawPrev, yaw);
            residuals[1] = weight * periodicDiff(yaw, yawNext);

            const auto periodicDiffJ0 = periodicDiffJacobian(yawPrev, yaw);
            const auto periodicDiffJ1 = periodicDiffJacobian(yaw, yawNext);

            if (jacobians == nullptr)
            {
                return true;
            }

            double* jacobian0 = jacobians[0];
            if (jacobian0 == nullptr)
            {
                return true;
            }

            double* jacobian1 = jacobians[1];
            if (jacobian1 == nullptr)
            {
                return true;
            }

            double* jacobian2 = jacobians[2];
            if (jacobian2 == nullptr)
            {
                return true;
            }

            // d r_i / d p_0
            jacobian0[0] = weight * periodicDiffJ0.at(0);
            jacobian0[1] = 0.;

            // d r_i / d p_2
            jacobian1[0] = weight * periodicDiffJ0.at(1);
            jacobian1[1] = weight * periodicDiffJ1.at(0);

            // d r_i / d p_1
            jacobian2[0] = 0.;
            jacobian2[1] = weight * periodicDiffJ1.at(1);

            return true;
        }

    private:
        const double weight;
    };

} // namespace armarx::navigation::global_planning::optimization
