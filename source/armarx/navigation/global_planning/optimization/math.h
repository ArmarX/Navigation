/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <array>

#include <ceres/ceres.h>

namespace armarx::navigation::global_planning::optimization
{

    auto
    periodicDiff(const auto a, const auto b)
    {
        // angle to corresponding vector
        const auto a1 = ceres::cos(a);
        const auto a2 = ceres::sin(a);

        // angle to corresponding vector
        const auto b1 = ceres::cos(b);
        const auto b2 = ceres::sin(b);

        // cosine similarity
        const auto cosSim = a1 * b1 + a2 * b2;

        const auto angleDiff = ceres::acos(cosSim);

        return angleDiff;
    }

    std::array<double, 2> inline periodicDiffJacobian(const auto a, const auto b)
    {
        const auto derivative = [&a, &b]() -> double
        {
            if (a == 0. and b == 0.)
            {
                return 0.;
            }


            const double denomSquared =
                1 - ceres::pow(ceres::sin(a) * ceres::sin(b) + ceres::cos(a) * ceres::cos(b), 2);

            // prevent division by 0
            if (denomSquared < 0.001)
            {
                return 0.;
            }

            return (ceres::sin(a) * ceres::cos(b) - ceres::cos(a) * ceres::sin(b)) /
                   (ceres::sqrt(
                       1 - ceres::pow(ceres::sin(a) * ceres::sin(b) + ceres::cos(a) * ceres::cos(b),
                                      2)));
        }();


        std::array<double, 2> J;

        // the partial derivative for a and b is similar
        J.at(0) = derivative;
        J.at(1) = -derivative;

        return J;
    }
} // namespace armarx::navigation::global_planning::optimization
