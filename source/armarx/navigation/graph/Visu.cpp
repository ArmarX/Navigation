/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Visu.h"

#include <SimoxUtility/color/Color.h>
#include <SimoxUtility/math/pose.h>
#include <VirtualRobot/VirtualRobot.h>

#include <RobotAPI/components/ArViz/Client/Client.h>


namespace armarx::navigation::graph
{

    viz::Pose
    VertexVisu::Pose::draw(const core::VertexAttribs& attribs) const
    {
        return draw(attribs.getName(), attribs.getPose());
    }

    viz::Pose
    VertexVisu::Pose::draw(const std::string& name, const core::Pose& pose) const
    {
        return viz::Pose(name).pose(pose).scale(scale);
    }


    viz::Arrow
    VertexVisu::ForwardArrow::draw(const core::VertexAttribs& attribs) const
    {
        return draw(attribs.getName(), attribs.getPose());
    }


    viz::Arrow
    VertexVisu::ForwardArrow::draw(const std::string& name, const core::Pose& pose) const
    {
        return viz::Arrow(name + " forward")
            .fromTo(
                pose.translation(),
                simox::math::transform_position(pose.matrix(), length * Eigen::Vector3f::UnitY()))
            .color(color)
            .width(width);
    }


    void
    VertexVisu::draw(viz::Layer& layer, core::Graph::ConstVertex vertex) const
    {
        draw(layer, vertex.attrib());
    }


    void
    VertexVisu::draw(viz::Layer& layer, const core::VertexAttribs& attribs) const
    {
        draw(layer, attribs.getName(), attribs.getPose());
    }


    void
    VertexVisu::draw(viz::Layer& layer, const std::string& name, const core::Pose& pose) const
    {
        if (this->pose.has_value())
        {
            layer.add(this->pose->draw(name, pose));
        }
        if (forwardArrow.has_value())
        {
            layer.add(forwardArrow->draw(name, pose));
        }
    }


    viz::Arrow
    EdgeVisu::Arrow::draw(core::Graph::ConstEdge edge) const
    {
        return draw(edge.attrib(), edge.source().attrib(), edge.target().attrib());
    }


    viz::Arrow
    EdgeVisu::Arrow::draw(const core::EdgeAttribs& edge,
                          const core::VertexAttribs& source,
                          const core::VertexAttribs& target) const
    {
        (void)edge;
        return viz::Arrow(source.getName() + " -> " + target.getName())
            .fromTo(source.getPose().translation(), target.getPose().translation())
            .width(width)
            .color(color);
    }


    void
    EdgeVisu::draw(viz::Layer& layer, core::Graph::ConstEdge edge) const
    {
        if (arrow.has_value())
        {
            layer.add(arrow->draw(edge));
        }
    }


    void
    GraphVisu::draw(viz::Layer& layer, const core::Graph& graph) const
    {
        if (vertex.has_value())
        {
            for (core::Graph::ConstVertex v : graph.vertices())
            {
                vertex->draw(layer, v);
            }
        }
        if (edge.has_value())
        {
            for (core::Graph::ConstEdge e : graph.edges())
            {
                edge->draw(layer, e);
            }
        }
    }
} // namespace armarx::navigation::graph
