/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <optional>

#include <SimoxUtility/color/Color.h>

#include <armarx/navigation/core/Graph.h>


namespace armarx::viz
{
    class Arrow;
    class Layer;
    class Pose;
} // namespace armarx::viz

namespace armarx::navigation::graph
{

    struct VertexVisu
    {
        struct Pose
        {
            float scale = 1.0;

            viz::Pose draw(const core::VertexAttribs& attribs) const;
            viz::Pose draw(const std::string& name, const core::Pose& pose) const;
        };
        std::optional<Pose> pose = Pose{};

        struct ForwardArrow
        {
            float width = 7.5;
            float length = 100.0;
            simox::Color color = simox::Color::green();

            viz::Arrow draw(const core::VertexAttribs& attribs) const;
            viz::Arrow draw(const std::string& name, const core::Pose& pose) const;
        };
        std::optional<ForwardArrow> forwardArrow = ForwardArrow{};


        void draw(viz::Layer& layer, core::Graph::ConstVertex vertex) const;
        void draw(viz::Layer& layer, const core::VertexAttribs& attribs) const;
        void draw(viz::Layer& layer, const std::string& name, const core::Pose& pose) const;
    };


    struct EdgeVisu
    {
        struct Arrow
        {
            float width = 5.0;
            simox::Color color = simox::Color::azure(196);

            viz::Arrow draw(core::Graph::ConstEdge edge) const;
            viz::Arrow draw(const core::EdgeAttribs& edge,
                            const core::VertexAttribs& source,
                            const core::VertexAttribs& target) const;
        };
        std::optional<Arrow> arrow = Arrow{};


        void draw(viz::Layer& layer, core::Graph::ConstEdge edge) const;
    };


    struct GraphVisu
    {
        std::optional<VertexVisu> vertex = VertexVisu{};
        std::optional<EdgeVisu> edge = EdgeVisu{};


        void draw(viz::Layer& layer, const core::Graph& graph) const;
    };

} // namespace armarx::navigation::graph
