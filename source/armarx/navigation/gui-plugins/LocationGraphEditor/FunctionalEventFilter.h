#pragma once

#include <QObject>
#include <functional>

class QEvent;


namespace simox::gui
{

    class FunctionalEventFilter : public QObject
    {
    public:
        using Function = std::function<bool(QObject* obj, QEvent* event)>;


        FunctionalEventFilter(Function&& function);


    protected:
        bool eventFilter(QObject* obj, QEvent* event) override;

        Function function;
    };

} // namespace simox::gui
