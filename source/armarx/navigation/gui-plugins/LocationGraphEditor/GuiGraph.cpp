/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::ArmarXObjects::GraphImportExport
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GuiGraph.h"

#include <SimoxUtility/math/convert/mat4f_to_rpy.h>
#include <SimoxUtility/math/convert/rad_to_deg.h>


namespace armarx::navigation::qt_plugins::location_graph_editor
{

    bool
    GuiGraph::hasChanged() const
    {
        if (attrib().edgesChanged)
        {
            return true;
        }
        for (auto vertex : vertices())
        {
            if (vertex.attrib().changed)
            {
                return true;
            }
        }
        return false;
    }

    std::optional<GuiGraph::Vertex>
    GuiGraph::getVertexFromTableItem(QTableWidgetItem* item)
    {
        for (auto vertex : vertices())
        {
            if (vertex.attrib().tableWidgetItem == item)
            {
                return vertex;
            }
        }
        return std::nullopt;
    }


    std::map<QTableWidgetItem*, GuiGraph::Vertex>
    GuiGraph::getTableItemToVertexMap()
    {
        std::map<QTableWidgetItem*, GuiGraph::Vertex> map;

        for (auto vertex : vertices())
        {
            if (vertex.attrib().tableWidgetItem != nullptr)
            {
                map[vertex.attrib().tableWidgetItem] = vertex;
            }
        }

        return map;
    }


    std::map<QTableWidgetItem*, GuiGraph::Edge>
    GuiGraph::getTableItemToEdgeMap()
    {
        std::map<QTableWidgetItem*, GuiGraph::Edge> map;

        for (auto edge : edges())
        {
            if (edge.attrib().tableWidgetItem != nullptr)
            {
                map[edge.attrib().tableWidgetItem] = edge;
            }
        }

        return map;
    }

    float
    getYawAngleDegree(const core::Pose& pose)
    {
        return simox::math::rad_to_deg(simox::math::mat4f_to_rpy(pose.matrix())(2));
    }


    double
    getYawAngleDegree(const core::Pose_d& pose)
    {
        return simox::math::rad_to_deg(simox::math::mat4f_to_rpy(pose.matrix())(2));
    }


    auto
    toGuiGraph(const core::Graph& nav) -> GuiGraph
    {
        GuiGraph gui;
        for (auto v : nav.vertices())
        {
            gui.addVertex(v.objectID(), {v.attrib()});
        }
        for (auto e : nav.edges())
        {
            gui.addEdge(e.sourceObjectID(), e.targetObjectID(), {e.attrib()});
        }
        return gui;
    }


    navigation::core::Graph
    fromGuiGraph(const GuiGraph& gui)
    {
        core::Graph nav;
        for (auto v : gui.vertices())
        {
            nav.addVertex(v.objectID(), {v.attrib()});
        }
        for (auto e : gui.edges())
        {
            nav.addEdge(e.sourceObjectID(), e.targetObjectID(), {e.attrib()});
        }
        return nav;
    }

} // namespace armarx::navigation::qt_plugins::location_graph_editor
