/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Visu.h"

#include <SimoxUtility/color/Color.h>
#include <SimoxUtility/math/pose.h>
#include <VirtualRobot/VirtualRobot.h>

#include <RobotAPI/components/ArViz/Client/Client.h>


namespace armarx::navigation::qt_plugins::location_graph_editor
{

    const simox::Color visu::defaultColorHighlighted = simox::Color::orange();


    viz::Pose
    VertexVisu::Pose::draw(const VertexData& attribs) const
    {
        viz::Pose pose = navigation::graph::VertexVisu::Pose::draw(attribs);
        if (attribs.highlighted)
        {
            pose.scale(scale * scaleFactorHighlighted);
        }
        return pose;
    }


    viz::Arrow
    VertexVisu::ForwardArrow::draw(const VertexData& attribs) const
    {
        viz::Arrow arrow = navigation::graph::VertexVisu::ForwardArrow::draw(attribs);
        if (attribs.highlighted)
        {
            arrow.color(colorHighlighted);
            arrow.width(width * 1.5f);
        }
        return arrow;
    }


    void
    VertexVisu::draw(viz::Layer& layer, GuiGraph::ConstVertex vertex) const
    {
        draw(layer, vertex.attrib());
    }


    void
    VertexVisu::draw(viz::Layer& layer, const VertexData& attribs) const
    {
        if (pose.has_value())
        {
            layer.add(pose->draw(attribs));
        }
        if (forwardArrow.has_value())
        {
            layer.add(forwardArrow->draw(attribs));
        }
    }


    viz::Arrow
    EdgeVisu::Arrow::draw(GuiGraph::ConstEdge edge) const
    {
        return draw(edge.attrib(), edge.source().attrib(), edge.target().attrib());
    }


    viz::Arrow
    EdgeVisu::Arrow::draw(const EdgeData& edge,
                          const VertexData& source,
                          const VertexData& target) const
    {
        viz::Arrow arrow = navigation::graph::EdgeVisu::Arrow::draw(edge, source, target);
        if (edge.highlighted)
        {
            arrow.color(colorHighlighted);
            arrow.width(width * 1.5f);
        }
        return arrow;
    }


    void
    EdgeVisu::draw(viz::Layer& layer, GuiGraph::ConstEdge edge) const
    {
        if (arrow.has_value())
        {
            layer.add(arrow->draw(edge));
        }
    }


    void
    GraphVisu::draw(viz::Layer& layer, const GuiGraph& graph) const
    {
        if (vertex.has_value())
        {
            for (GuiGraph::ConstVertex v : graph.vertices())
            {
                vertex->draw(layer, v);
            }
        }
        if (edge.has_value())
        {
            for (GuiGraph::ConstEdge e : graph.edges())
            {
                edge->draw(layer, e);
            }
        }
    }

} // namespace armarx::navigation::qt_plugins::location_graph_editor
