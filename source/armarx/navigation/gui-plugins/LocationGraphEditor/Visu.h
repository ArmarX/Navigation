/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <optional>

#include <SimoxUtility/color/Color.h>

#include "GuiGraph.h"
#include <armarx/navigation/graph/Visu.h>


namespace armarx::navigation::qt_plugins::location_graph_editor::visu
{
    extern const simox::Color defaultColorHighlighted;
}
namespace armarx::navigation::qt_plugins::location_graph_editor
{

    struct VertexVisu
    {
        struct Pose : public navigation::graph::VertexVisu::Pose
        {
            float scaleFactorHighlighted = 1.5;

            viz::Pose draw(const VertexData& attribs) const;
        };
        std::optional<Pose> pose = Pose{};

        struct ForwardArrow : public navigation::graph::VertexVisu::ForwardArrow
        {
            simox::Color colorHighlighted = visu::defaultColorHighlighted;

            viz::Arrow draw(const VertexData& attribs) const;
        };
        std::optional<ForwardArrow> forwardArrow = ForwardArrow{};


        void draw(viz::Layer& layer, GuiGraph::ConstVertex vertex) const;
        void draw(viz::Layer& layer, const VertexData& attribs) const;
    };


    struct EdgeVisu
    {
        struct Arrow : public navigation::graph::EdgeVisu::Arrow
        {
            simox::Color colorHighlighted = visu::defaultColorHighlighted;

            viz::Arrow draw(GuiGraph::ConstEdge edge) const;
            viz::Arrow
            draw(const EdgeData& edge, const VertexData& source, const VertexData& target) const;
        };
        std::optional<Arrow> arrow = Arrow{};


        void draw(viz::Layer& layer, GuiGraph::ConstEdge edge) const;
    };


    struct GraphVisu
    {
        std::optional<VertexVisu> vertex = VertexVisu{};
        std::optional<EdgeVisu> edge = EdgeVisu{};


        void draw(viz::Layer& layer, const GuiGraph& graph) const;
    };


    template <class VisuT, class RangeT>
    void
    applyVisu(viz::Layer& layer, const std::optional<VisuT>& visu, RangeT&& range)
    {
        if (visu.has_value())
        {
            for (auto element : range)
            {
                visu->draw(layer, element);
            }
        }
    }

} // namespace armarx::navigation::qt_plugins::location_graph_editor
