/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <QDialog>
#include <QDialogButtonBox>
#include <QVBoxLayout>

#include <ArmarXCore/core/ManagedIceObject.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>


namespace armarx::navigation::qt_plugins::location_graph_editor
{
    template <class ProxyT>
    class ConnectDialog : public QDialog
    {
    public:
        ConnectDialog(QString searchMask, ManagedIceObject& component, QWidget* parent = nullptr) :
            QDialog(parent), component(component)
        {
            finder = new armarx::IceProxyFinder<ProxyT>();
            finder->setIceManager(component.getIceManager());
            finder->setSearchMask(searchMask);

            QDialogButtonBox* buttonBox =
                new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, this);

            setLayout(new QVBoxLayout);
            layout()->addWidget(finder);
            layout()->addWidget(buttonBox);

            connect(buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
            connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
        }

        virtual ~ConnectDialog() override
        {
        }


        ProxyT
        getProxy()
        {
            QString name = finder->getSelectedProxyName();
            if (name.size() > 0)
            {
                return component.getProxy<ProxyT>(name.toStdString());
            }
            else
            {
                return nullptr;
            }
        }


    public:
        ManagedIceObject& component;
        armarx::IceProxyFinder<ProxyT>* finder = nullptr;
    };


} // namespace armarx::navigation::qt_plugins::location_graph_editor
