/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <QDialog>
#include <memory>

#include <RobotAPI/libraries/armem/core/forward_declarations.h>


class QLineEdit;
class QDialogButtonBox;


namespace armarx::navigation::qt_plugins::location_graph_editor
{
    class NewEntityIdDialog : public QDialog
    {
    public:
        NewEntityIdDialog(const armem::MemoryID& coreSegmentID, QWidget* parent = nullptr);
        virtual ~NewEntityIdDialog() override;

        QString providerSegmentName() const;
        QString entityName() const;


        armem::MemoryID entityIDWithoutCoreSegmentID() const;
        armem::MemoryID entityID() const;


    private:
        QLineEdit* _providerSegmentName = nullptr;
        QLineEdit* _entityName = nullptr;

        QDialogButtonBox* _buttonBox = nullptr;


        std::unique_ptr<armem::MemoryID> coreSegmentID;
    };

} // namespace armarx::navigation::qt_plugins::location_graph_editor
