/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <QWidget>
#include <map>
#include <memory>
#include <optional>
#include <string>

#include <Eigen/Core>

#include <armarx/navigation/core/types.h>


class QCheckBox;
class QLabel;
class QPushButton;


namespace VirtualRobot
{
    using RobotPtr = std::shared_ptr<class Robot>;
}
namespace IceInternal
{
    template <typename T>
    class ProxyHandle;
}
namespace IceProxy::armarx
{
    class RobotStateComponentInterface;
}
namespace armarx
{
    class ManagedIceObject;
    using RobotStateComponentInterfacePrx =
        ::IceInternal::ProxyHandle<::IceProxy::armarx::RobotStateComponentInterface>;
} // namespace armarx
namespace armarx::viz
{
    class Robot;
}


namespace armarx::navigation::qt_plugins::location_graph_editor::robotvisu
{

    class SettingsWidget : public QWidget
    {
        Q_OBJECT
        using This = SettingsWidget;

    public:
        SettingsWidget(QWidget* parent = nullptr);


        bool isEnabled() const;
        bool useCollisionModel() const;


    signals:

        void changed();


    private:
        QCheckBox* _enabled = nullptr;
        QCheckBox* _collisionModel = nullptr;
    };


    class Connection
    {
    public:
        Connection(RobotStateComponentInterfacePrx robotStateComponent,
                   robotvisu::SettingsWidget* settings);
        ~Connection();


        RobotStateComponentInterfacePrx getRobotStateComponent() const;
        std::string getConnectedName() const;


        core::Pose getGlobalPose(bool synchronize = true);
        std::map<std::string, float> getJointValues(bool synchronize = true);

        viz::Robot vizRobot(const std::string& id, bool synchronize = true);


    private:
        void _synchronize(bool synchronize);

        std::unique_ptr<RobotStateComponentInterfacePrx> _robotStateComponent;
        robotvisu::SettingsWidget* _settings = nullptr;

        std::string _filename;
        VirtualRobot::RobotPtr _robot;
    };

} // namespace armarx::navigation::qt_plugins::location_graph_editor::robotvisu

namespace armarx::navigation::qt_plugins::location_graph_editor
{

    class RobotVisuWidget : public QWidget
    {
        Q_OBJECT
        using This = RobotVisuWidget;

    public:
        RobotVisuWidget(ManagedIceObject& component, QWidget* parent = nullptr);


        /// Indicates whether the connection is established and visu is enabled.
        bool isEnabled() const;

        bool isConnected() const;
        robotvisu::Connection& connection();


    signals:

        void connected();
        void settingsChanged();


    public slots:

        void connectToRobot();


    private:
        ManagedIceObject& _component;
        std::optional<robotvisu::Connection> _connection;

        QLabel* _statusLabel = nullptr;
        QPushButton* _connectButton = nullptr;
        robotvisu::SettingsWidget* _settings = nullptr;
    };

} // namespace armarx::navigation::qt_plugins::location_graph_editor
