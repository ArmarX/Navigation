/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MemoryX::ArmarXObjects::GraphImportExport
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "VertexDataWidget.h"

#include <QDoubleSpinBox>
#include <QFormLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QList>
#include <QPushButton>
#include <QRadioButton>
#include <QSignalBlocker>

#include <SimoxUtility/math/convert/deg_to_rad.h>
#include <SimoxUtility/math/convert/mat4f_to_rpy.h>
#include <SimoxUtility/math/convert/rad_to_deg.h>
#include <SimoxUtility/math/convert/rpy_to_mat4f.h>
#include <SimoxUtility/math/pose/pose.h>

#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem/core/aron_conversions.h>
#include <RobotAPI/libraries/aron/common/aron_conversions/core.h>

#include "RobotVisuWidget.h"


namespace armarx::navigation::qt_plugins::location_graph_editor
{

    VertexDataWidget::VertexDataWidget()
    {
        setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);

        name = new QLineEdit(this);
        name->setReadOnly(true);
        locationID = new QLineEdit(this);
        locationID->setReadOnly(true);

        frame = new QLineEdit(this);

        x = new QDoubleSpinBox(this);
        y = new QDoubleSpinBox(this);
        z = new QDoubleSpinBox(this);

        roll = new QDoubleSpinBox(this);
        pitch = new QDoubleSpinBox(this);
        yaw = new QDoubleSpinBox(this);

        angleUnitDeg = new QRadioButton("Degrees");
        angleUnitRad = new QRadioButton("Radians");

        _useCurrentRobotPose = new QPushButton("Use Current Robot Pose");
        _useCurrentRobotPose->setEnabled(false);
        _useCurrentRobotPose->setToolTip(
            "Use the robot's current global pose (requires connection to robot state).");


        for (QDoubleSpinBox* pos : _positionSpinBoxes())
        {
            pos->setSuffix(" mm");
            pos->setMinimum(-1e6);
            pos->setMaximum(+1e6);
            pos->setSingleStep(50);
        }
        for (QDoubleSpinBox* spinBox : _allSpinBoxes())
        {
            spinBox->setValue(0);
            spinBox->setAlignment(Qt::AlignRight);
        }

        QHBoxLayout* angleUnitLayout = new QHBoxLayout();
        for (QRadioButton* angleUnit : {angleUnitDeg, angleUnitRad})
        {
            angleUnitLayout->addWidget(angleUnit);
        }

        QFormLayout* layout = new QFormLayout();
        this->setLayout(layout);

        layout->addRow("Name", name);
        layout->addRow("Entity ID", locationID);
        layout->addRow("Frame", frame);
        layout->addRow("X", x);
        layout->addRow("Y", y);
        layout->addRow("Z", z);
        layout->addRow("Roll", roll);
        layout->addRow("Pitch", pitch);
        layout->addRow("Yaw", yaw);
        layout->addRow("Angle Units", angleUnitLayout);
        layout->addRow(_useCurrentRobotPose);


        // Connect

        for (QRadioButton* angleUnit : {angleUnitDeg, angleUnitRad})
        {
            connect(angleUnit, &QRadioButton::toggled, this, &This::_updateAngleUnit);
        }

        connect(frame, &QLineEdit::editingFinished, this, &This::_updateVertexAttribs);
        for (QDoubleSpinBox* spinBox : _allSpinBoxes())
        {
            connect(spinBox,
                    QOverload<qreal>::of(&QDoubleSpinBox::valueChanged),
                    this,
                    &This::_updateVertexAttribs);
        }

        connect(_useCurrentRobotPose, &QPushButton::pressed, this, &This::_setFromCurrentRobotPose);


        angleUnitDeg->click();
    }


    std::optional<GuiGraph::Vertex>
    VertexDataWidget::vertex()
    {
        return _vertex;
    }


    void
    VertexDataWidget::setVertex(GuiGraph::Vertex vertex)
    {
        QSignalBlocker blocker(this);

        _vertex = vertex;
        _setFromVertex(vertex);
        setEnabled(true);
    }


    void
    VertexDataWidget::clearVertex()
    {
        _vertex = std::nullopt;
        setEnabled(false);
    }


    Eigen::Vector3d
    VertexDataWidget::xyz() const
    {
        return {x->value(), y->value(), z->value()};
    }


    Eigen::Vector3d
    VertexDataWidget::rpyDeg() const
    {
        Eigen::Vector3d raw = _rpyRaw();
        if (angleUnitRad->isChecked())
        {
            return {}; // return simox::math::rad_to_deg(raw);
        }
        else
        {
            return raw;
        }
    }


    Eigen::Vector3d
    VertexDataWidget::rpyRad() const
    {
        Eigen::Vector3d raw = _rpyRaw();
        if (angleUnitDeg->isChecked())
        {
            return simox::math::deg_to_rad(raw);
        }
        else
        {
            return raw;
        }
    }


    void
    VertexDataWidget::setRobotConnection(robotvisu::Connection* connection)
    {
        this->_robotConnection = connection;
        _useCurrentRobotPose->setEnabled(_robotConnection != nullptr);
    }


    void
    VertexDataWidget::_setFromVertex(const GuiGraph::Vertex& vertex)
    {
        const VertexData& attrib = vertex.attrib();

        name->setText(QString::fromStdString(attrib.getName()));
        locationID->setText(
            QString::fromStdString(aron::fromAron<armem::MemoryID>(attrib.aron.locationID).str()));
        frame->setText("<WIP>");
        _setPose(attrib.getPose().cast<qreal>());
    }


    void
    VertexDataWidget::_getToVertex(GuiGraph::Vertex& vertex)
    {
        VertexData& attrib = vertex.attrib();

        // name is read-only
        // locationID is read-only
        // frame->setText("<WIP>");  // WIP

        core::Pose_d pose(simox::math::pose(xyz(), simox::math::rpy_to_mat3f(rpyRad())));
        attrib.setPose(pose.cast<float>());
    }


    void
    VertexDataWidget::_updateAngleUnit()
    {
        std::function<double(double)> convertValue;
        QString suffix = " \u00b0";
        double min = -360;
        double max = +360;
        double step = 5.;
        int decimals = 2;

        if (angleUnitRad->isChecked())
        {
            convertValue = [](double deg) { return simox::math::deg_to_rad(deg); };
            suffix = " rad";
            min = simox::math::deg_to_rad(min);
            max = simox::math::deg_to_rad(max);
            step = simox::math::deg_to_rad(step);
            decimals = 3;
        }
        else
        {
            ARMARX_CHECK(angleUnitDeg->isChecked());
            convertValue = [](double rad) { return simox::math::rad_to_deg(rad); };
        }

        std::vector<QSignalBlocker> blockers;
        for (QDoubleSpinBox* angle : _angleSpinBoxes())
        {
            blockers.emplace_back(angle);

            angle->setSuffix(suffix);
            angle->setMinimum(min);
            angle->setMaximum(max);
            angle->setSingleStep(step);
            angle->setDecimals(decimals);
        }
        if (_vertex.has_value())
        {
            _setRpyRad(
                simox::math::mat4f_to_rpy(_vertex->attrib().getPose().cast<qreal>().matrix()));
        }

        // blockers will disable blocking for all spin boxes on destruction.
    }


    void
    VertexDataWidget::_updateVertexAttribs()
    {
        if (not signalsBlocked() and _vertex.has_value())
        {
            _getToVertex(_vertex.value());
            _vertex->attrib().changed = true;
            emit vertexDataChanged();
        }
    }


    std::vector<QDoubleSpinBox*>
    VertexDataWidget::_positionSpinBoxes()
    {
        return {x, y, z};
    }


    std::vector<QDoubleSpinBox*>
    VertexDataWidget::_angleSpinBoxes()
    {
        return {roll, pitch, yaw};
    }


    std::vector<QDoubleSpinBox*>
    VertexDataWidget::_allSpinBoxes()
    {
        return {x, y, z, roll, pitch, yaw};
    }


    void
    VertexDataWidget::_setPose(const core::Pose_d& pose)
    {
        _setXyz(simox::math::position(pose.matrix()));
        _setRpyRad(simox::math::mat4f_to_rpy(pose.matrix()));
    }


    void
    VertexDataWidget::_setXyz(const Eigen::Vector3d& xyz)
    {
        x->setValue(xyz.x());
        y->setValue(xyz.y());
        z->setValue(xyz.z());
    }


    Eigen::Vector3d
    VertexDataWidget::_rpyRaw() const
    {
        return {roll->value(), pitch->value(), yaw->value()};
    }


    void
    VertexDataWidget::_setRpyRaw(const Eigen::Vector3d& rpy) const
    {
        roll->setValue(rpy(0));
        pitch->setValue(rpy(1));
        yaw->setValue(rpy(2));
    }


    void
    VertexDataWidget::_setRpyDeg(const Eigen::Vector3d& rpyDeg) const
    {
        if (angleUnitDeg->isChecked())
        {
            _setRpyRaw(rpyDeg);
        }
        else if (angleUnitRad->isChecked())
        {
            _setRpyRaw(simox::math::deg_to_rad(rpyDeg));
        }
    }


    void
    VertexDataWidget::_setRpyRad(const Eigen::Vector3d& rpyRad) const
    {
        if (angleUnitDeg->isChecked())
        {
            _setRpyRaw(simox::math::rad_to_deg(rpyRad));
        }
        else if (angleUnitRad->isChecked())
        {
            _setRpyRaw(rpyRad);
        }
    }


    void
    VertexDataWidget::_setFromCurrentRobotPose()
    {
        ARMARX_CHECK_NOT_NULL(_robotConnection);
        {
            QSignalBlocker blocker(this);
            _setPose(_robotConnection->getGlobalPose().cast<qreal>());
        }
        _updateVertexAttribs();
    }

} // namespace armarx::navigation::qt_plugins::location_graph_editor
