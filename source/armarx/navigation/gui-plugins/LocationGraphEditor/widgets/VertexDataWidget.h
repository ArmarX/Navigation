/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <QWidget>

#include <Eigen/Core>

#include <armarx/navigation/gui-plugins/LocationGraphEditor/GuiGraph.h>


class QLineEdit;
class QDoubleSpinBox;
class QPushButton;
class QRadioButton;


namespace armarx::navigation::qt_plugins::location_graph_editor::robotvisu
{
    class Connection;
}
namespace armarx::navigation::qt_plugins::location_graph_editor
{

    class VertexDataWidget : public QWidget
    {
        Q_OBJECT
        using This = VertexDataWidget;


    public:
        VertexDataWidget();


        std::optional<GuiGraph::Vertex> vertex();
        void setVertex(GuiGraph::Vertex vertex);
        void clearVertex();

        Eigen::Vector3d xyz() const;
        Eigen::Vector3d rpyDeg() const;
        Eigen::Vector3d rpyRad() const;


        void setRobotConnection(robotvisu::Connection* connection);


    signals:

        void vertexDataChanged();


    private slots:

        void _updateAngleUnit();
        void _updateVertexAttribs();


    private:
        void _setFromVertex(const GuiGraph::Vertex& vertex);
        void _getToVertex(GuiGraph::Vertex& vertex);

        std::vector<QDoubleSpinBox*> _positionSpinBoxes();
        std::vector<QDoubleSpinBox*> _angleSpinBoxes();
        std::vector<QDoubleSpinBox*> _allSpinBoxes();

        void _setPose(const core::Pose_d& pose);
        void _setXyz(const Eigen::Vector3d& xyz);

        Eigen::Vector3d _rpyRaw() const;
        void _setRpyRaw(const Eigen::Vector3d& rpy) const;
        void _setRpyDeg(const Eigen::Vector3d& rpyDeg) const;
        void _setRpyRad(const Eigen::Vector3d& rpyRad) const;

        void _setFromCurrentRobotPose();


    private:
        std::optional<GuiGraph::Vertex> _vertex;


        QLineEdit* name = nullptr;
        QLineEdit* locationID = nullptr;
        QLineEdit* frame = nullptr;

        QDoubleSpinBox* x = nullptr;
        QDoubleSpinBox* y = nullptr;
        QDoubleSpinBox* z = nullptr;

        QDoubleSpinBox* roll = nullptr;
        QDoubleSpinBox* pitch = nullptr;
        QDoubleSpinBox* yaw = nullptr;

        QRadioButton* angleUnitDeg = nullptr;
        QRadioButton* angleUnitRad = nullptr;


        robotvisu::Connection* _robotConnection = nullptr;
        QPushButton* _useCurrentRobotPose = nullptr;
    };

} // namespace armarx::navigation::qt_plugins::location_graph_editor
