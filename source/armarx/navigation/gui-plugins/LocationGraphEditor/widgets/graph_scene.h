#pragma once


namespace armarx::navigation::qt_plugins::location_graph_editor::graph_scene
{
    class Scene;
    class Widget;
} // namespace armarx::navigation::qt_plugins::location_graph_editor::graph_scene

namespace armarx::navigation::qt_plugins::location_graph_editor
{

    using GraphScene = graph_scene::Scene;
    using GraphSceneWidget = graph_scene::Widget;

} // namespace armarx::navigation::qt_plugins::location_graph_editor
