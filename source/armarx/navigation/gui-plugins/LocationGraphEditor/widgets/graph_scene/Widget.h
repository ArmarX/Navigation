#pragma once

#include <QWidget>

class QGraphicsView;


namespace armarx::navigation::qt_plugins::location_graph_editor::graph_scene
{
    class ControlWidget;
    class Scene;


    class Widget : public QWidget
    {
        Q_OBJECT
        using This = Widget;


    public:
        Widget();

        QGraphicsView* view();
        Scene* scene();


    public slots:

        void transform();
        void autoAdjustZoom();


    private:
        /// Some buttons and input fields.
        ControlWidget* _control = nullptr;

        /// The "canvas".
        QGraphicsView* _view = nullptr;

        /**
         * @brief The scene displayed in the widget.
         *
         * For y coordinates (-y) is used to mirror the scene on the y axis.
         * If (+y) would be used the graph displayed in the scene would not
         * match the graph drawn to the debug layer.
         */
        Scene* _scene;


        /// The view's rotation angle.
        qreal _angle = 0;
    };

} // namespace armarx::navigation::qt_plugins::location_graph_editor::graph_scene
