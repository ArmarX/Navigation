/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "utils.h"

#include <QTableWidget>
#include <set>


namespace armarx::navigation::qt_plugins::location_graph_editor
{

    // <- https://unicode-table.com/de/2190/
    const QString utils::arrowLeft = QStringLiteral("\u2190");
    // -> https://unicode-table.com/de/2192/
    const QString utils::arrowRight = QStringLiteral("\u2192");
    // <-> https://unicode-table.com/de/21C4/
    const QString utils::arrowBoth = QStringLiteral("\u21C4");


    QList<QTableWidgetItem*>
    utils::getSelectedItemsOfColumn(QTableWidget* widget, int column)
    {
        std::set<QTableWidgetItem*> set;
        for (QTableWidgetItem* selected : widget->selectedItems())
        {
            if (widget->column(selected) != column)
            {
                selected = widget->item(widget->row(selected), 0);
            }
            set.insert(selected);
        }

        QList<QTableWidgetItem*> list;
        for (auto i : set)
        {
            list.append(i);
        }
        return list;
    }

} // namespace armarx::navigation::qt_plugins::location_graph_editor
