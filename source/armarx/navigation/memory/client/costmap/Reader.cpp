#include "Reader.h"

#include <ArmarXCore/core/exceptions/LocalException.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <RobotAPI/interface/armem/mns/MemoryNameSystemInterface.h>
#include <RobotAPI/interface/armem/server/ReadingMemoryInterface.h>

#include <RobotAPI/libraries/aron/core/Exception.h>
#include <RobotAPI/libraries/aron/core/data/variant/complex/NDArray.h>

#include <RobotAPI/libraries/armem/client/Query.h>
#include <RobotAPI/libraries/armem/client/Reader.h>
#include <RobotAPI/libraries/armem/client/query/Builder.h>
#include <RobotAPI/libraries/armem/client/query/selectors.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/util/util.h>

#include <armarx/navigation/algorithms/Costmap.h>
#include <armarx/navigation/algorithms/aron_conversions.h>
#include <armarx/navigation/memory/constants.h>

namespace armarx::navigation::memory::client::costmap
{
    Reader::~Reader() = default;

    armarx::armem::client::query::Builder
    Reader::buildQuery(const Query& query) const
    {
        armarx::armem::client::query::Builder qb;

        // clang-format off
        qb
        .coreSegments().withName(properties().coreSegmentName)
        .providerSegments().withName(query.providerName)
        .entities().all()
        .snapshots().beforeOrAtTime(query.timestamp);
        // clang-format on

        return qb;
    }

    std::string
    Reader::propertyPrefix() const
    {
        return "mem.nav.costmap.";
    }

    armarx::armem::client::util::SimpleReaderBase::Properties
    Reader::defaultProperties() const
    {
        return {.memoryName = memory::constants::NavigationMemoryName,
                .coreSegmentName = memory::constants::CostmapCoreSegmentName};
    }

    algorithms::Costmap
    asCostmap(const armem::wm::ProviderSegment& providerSegment)
    {
        ARMARX_CHECK(not providerSegment.empty()) << "No entities";
        ARMARX_CHECK(providerSegment.size() == 1) << "There should be only one entity!";

        const  armem::wm::EntityInstance* entityInstance = nullptr;
        providerSegment.forEachEntity(
            [&entityInstance](const  armem::wm::Entity& entity)
            {
                const auto& entitySnapshot = entity.getLatestSnapshot();
                ARMARX_CHECK(not entitySnapshot.empty()) << "No entity snapshot instances";

                entityInstance = &entitySnapshot.getInstance(0);
                return false;
            });
        ARMARX_CHECK_NOT_NULL(entityInstance);
        return algorithms::fromAron(*entityInstance);
    }

    Reader::Result
    Reader::query(const Query& query) const
    {
        const auto qb = buildQuery(query);

        ARMARX_DEBUG << "[MappingDataReader] query ... ";

        const armem::client::QueryResult qResult = memoryReader().query(qb.buildQueryInput());

        ARMARX_DEBUG << "[MappingDataReader] result: " << qResult;

        if (not qResult.success)
        {
            ARMARX_WARNING << "Failed to query data from memory: " << qResult.errorMessage;
            return {.costmap = std::nullopt,
                    .status = Result::Status::Error,
                    .errorMessage = qResult.errorMessage};
        }

        const auto coreSegment = qResult.memory.getCoreSegment(properties().coreSegmentName);

        if (not coreSegment.hasProviderSegment(query.providerName))
        {
            ARMARX_WARNING << "Provider segment `" << query.providerName
                           << "` does not exist (yet).";
            return {.costmap = std::nullopt, .status = Result::Status::NoData};
        }

        const armem::wm::ProviderSegment& providerSegment =
            coreSegment.getProviderSegment(query.providerName);

        if (providerSegment.empty())
        {
            ARMARX_WARNING << "No entities.";
            return {.costmap = std::nullopt,
                    .status = Result::Status::NoData,
                    .errorMessage = "No entities"};
        }

        try
        {
            return Result{.costmap = asCostmap(providerSegment), .status = Result::Status::Success};
        }
        catch (...)
        {
            return Result{.status = Result::Status::Error,
                          .errorMessage = GetHandledExceptionString()};
        }
    }

} // namespace armarx::navigation::memory::client::costmap
