#include "Reader.h"

#include <SimoxUtility/algorithm/get_map_keys_values.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <RobotAPI/interface/armem/memory.h>
#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client/query/Builder.h>
#include <RobotAPI/libraries/armem/client/util/SimpleReaderBase.h>
#include <RobotAPI/libraries/armem/core/error/mns.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>

#include <armarx/navigation/core/Graph.h>
#include <armarx/navigation/core/aron/Graph.aron.generated.h>
#include <armarx/navigation/core/aron/Location.aron.generated.h>
#include <armarx/navigation/graph/constants.h>
#include <armarx/navigation/location/constants.h>

namespace armarx::navigation::memory::client::graph
{
    std::string
    Reader::propertyPrefix() const
    {
        return "mem.nav.graph.";
    }

    Reader::Properties
    Reader::defaultProperties() const
    {
        return Properties{.memoryName = navigation::location::coreSegmentID.memoryName,
                          .coreSegmentName = navigation::location::coreSegmentID.coreSegmentName};
    }

    std::map<std::string, core::Pose>
    Reader::locations()
    {
        std::map<std::string, core::Pose> locations;

        const auto locs = queryLocations();
        for (const auto& [id, location] : locs)
        {
            locations.emplace(id.entityName, core::Pose(location.globalRobotPose));
        }

        return locations;
    }

    std::vector<armarx::navigation::core::Graph>
    Reader::graphs()
    {
        ARMARX_TRACE;

        std::map<armem::MemoryID, armarx::navigation::core::Graph> graphs;

        const auto graphSegment = allGraphs();
        graphSegment.forEachEntity(
            [&](const armem::wm::Entity& entity)
            {
                navigation::core::Graph& graph = graphs[entity.id()];
                if (const armem::wm::EntityInstance* instance = entity.findLatestInstance())
                {
                    navigation::core::arondto::Graph aron;
                    aron.fromAron(instance->data());
                    fromAron(aron, graph);
                }
                // else: empty layer
            });


        const auto locSegment = allLocations();

        for (auto& [_, graph] : graphs)
        {
            armarx::navigation::core::resolveLocations(graph, locSegment);
        }

        return simox::get_values(graphs);
    }

    armem::wm::CoreSegment
    Reader::allLocations()
    {
        ARMARX_TRACE;
        // Query all entities from provider.
        armem::client::query::Builder qb;

        // clang-format off
        qb
        .coreSegments().withName(navigation::location::coreSegmentID.coreSegmentName)
        .providerSegments().all()
        .entities().all()
        .snapshots().latest();
        // clang-format on

        const armem::client::QueryResult qResult =
            memoryReaderLocations.query(qb.buildQueryInput());
        ARMARX_DEBUG << "Lookup result in reader: " << qResult;

        if (not qResult.success)
        {
            ARMARX_WARNING << "Failure ...";
            return {};
        }

        const auto& locSegment =
            qResult.memory.getCoreSegment(navigation::location::coreSegmentID.coreSegmentName);
        return locSegment;
    }


    armem::wm::CoreSegment
    Reader::allGraphs()
    {
        ARMARX_TRACE;

        // Query all entities from provider.
        armem::client::query::Builder qb;

        // clang-format off
        qb
        .coreSegments().withName(navigation::graph::coreSegmentID.coreSegmentName)
        .providerSegments().all()
        .entities().all()
        .snapshots().latest();
        // clang-format on

        const armem::client::QueryResult qResult = memoryReaderGraphs.query(qb.buildQueryInput());
        ARMARX_DEBUG << "Lookup result in reader: " << qResult;

        if (not qResult.success)
        {
            ARMARX_WARNING << "Failure ...";
            return {};
        }

        const auto& graphSegment =
            qResult.memory.getCoreSegment(navigation::graph::coreSegmentID.coreSegmentName);
        return graphSegment;
    }

    std::map<armem::MemoryID, location::arondto::Location>
    Reader::queryLocations()
    {
        std::map<armem::MemoryID, location::arondto::Location> locations;

        // Query all entities from provider.
        armem::client::query::Builder qb;

        // clang-format off
        qb
        .coreSegments().withName(navigation::location::coreSegmentID.coreSegmentName)
        .providerSegments().all()
        .entities().all()
        .snapshots().latest();
        // clang-format on

        const armem::client::QueryResult qResult =
            memoryReaderLocations.query(qb.buildQueryInput());
        ARMARX_DEBUG << "Lookup result in reader: " << qResult;

        if (not qResult.success)
        {
            return {};
        }

        const auto& locSegment =
            qResult.memory.getCoreSegment(navigation::location::coreSegmentID.coreSegmentName);
        locSegment.forEachEntity(
            [&](const armem::wm::Entity& entity)
            {
                if (const armem::wm::EntityInstance* instance = entity.findLatestInstance())
                {
                    locations[entity.id()].fromAron(instance->data());
                }
            });

        ARMARX_INFO << "Retrieved " << locations.size() << " locations";
        return locations;
    }

    core::Pose
    Reader::resolveLocationId(const std::string& locationId)
    {
        const auto locs = locations();
        return locs.at(locationId);
    }

    void
    Reader::connect()
    {
        // for the sake of completion ...
        // armem::client::util::SimpleReaderBase::connect();

        // Wait for the memory to become available and add it as dependency.
        ARMARX_IMPORTANT << "Reader: Waiting for memory '"
                         << navigation::graph::coreSegmentID.memoryName << "' ...";

        try
        {
            ARMARX_INFO << "graphs ...";
            memoryReaderGraphs = memoryNameSystem.useReader(
                armem::MemoryID().withMemoryName(navigation::graph::coreSegmentID.memoryName));

            ARMARX_INFO << "locations ...";
            memoryReaderLocations = memoryNameSystem.useReader(
                armem::MemoryID().withMemoryName(navigation::location::coreSegmentID.memoryName));

            ARMARX_IMPORTANT << "Reader: Connected to memory '"
                             << navigation::graph::coreSegmentID.memoryName << "'";
        }
        catch (const armem::error::CouldNotResolveMemoryServer& e)
        {
            ARMARX_ERROR << e.what();
            return;
        }
    }

} // namespace armarx::navigation::memory::client::graph
