/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/libraries/armem/client/util/SimpleWriterBase.h>
#include <RobotAPI/libraries/armem/core/Commit.h>

#include <armarx/navigation/global_planning/GlobalPlanner.h>
#include <armarx/navigation/server/StackResult.h>
#include <armarx/navigation/trajectory_control/TrajectoryController.h>

namespace armarx::navigation::memory::client::stack_result
{

    class Writer : virtual public armem::client::util::SimpleWriterBase
    {
    public:
        using armem::client::util::SimpleWriterBase::SimpleWriterBase;

        bool store(const server::StackResult& result, const std::string& clientID);

        bool store(const armarx::navigation::global_planning::GlobalPlannerResult& result,
                   const std::string& clientID);
        bool store(const armarx::navigation::traj_ctrl::TrajectoryControllerResult& result,
                   const std::string& clientID);


    private:
        bool storePrepare(const armarx::navigation::global_planning::GlobalPlannerResult& result,
                          const std::string& clientID,
                          armem::Commit& commit);

        // bool storePrepare(const server::StackResult::LocalPlannerResult& result,
        //                   const std::string& clientID,
        //                   armem::Commit& commit);

        bool storePrepare(const armarx::navigation::traj_ctrl::TrajectoryControllerResult& result,
                          const std::string& clientID,
                          armem::Commit& commit);

        // bool storePrepare(const server::StackResult::SafetyControllerResult& result,
        //                   const std::string& clientID,
        //                   armem::Commit& commit);

    protected:
        std::string propertyPrefix() const override;
        Properties defaultProperties() const override;

    private:
    };
} // namespace armarx::navigation::memory::client::stack_result
