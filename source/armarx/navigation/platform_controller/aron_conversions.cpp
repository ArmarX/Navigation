/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "aron_conversions.h"

#include <RobotAPI/libraries/aron/common/aron_conversions/eigen.h>

#include <armarx/control/common/aron_conversions.h>
#include <armarx/navigation/platform_controller/PlatformTrajectoryController.h>
#include <armarx/navigation/core/aron_conversions.h>
#include <armarx/navigation/trajectory_control/aron_conversions.h>
#include <armarx/navigation/platform_controller/aron/PlatformTrajectoryControllerConfig.aron.generated.h>



namespace armarx::navigation::platform_controller::platform_trajectory
{

    void
    fromAron(const arondto::Targets& dto, Targets& bo)
    {
        fromAron(dto.trajectory, bo.trajectory);
    }

    void
    fromAron(const arondto::Config& dto, Config& bo)
    {
        fromAron(dto.params, bo.params);
        fromAron(dto.targets, bo.targets);
    }

} // namespace armarx::navigation::platform_controller::platform_trajectory
