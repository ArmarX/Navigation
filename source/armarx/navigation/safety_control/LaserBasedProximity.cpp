#include "LaserBasedProximity.h"

#include <armarx/navigation/core/types.h>
#include <armarx/navigation/safety_control/SafetyController.h>

namespace armarx::navigation::safe_ctrl
{
    Algorithms
    LaserBasedProximityParams::algorithm() const
    {
        return Algorithms::LaserBasedProximity;
    }

    aron::data::DictPtr
    LaserBasedProximityParams::toAron() const
    {
        return nullptr; // TODO implement
    }

    LaserBasedProximityParams
    LaserBasedProximityParams::FromAron(const aron::data::DictPtr& dict)
    {
        return LaserBasedProximityParams(); // TODO implement
    }

    LaserBasedProximity::LaserBasedProximity(const Params& params, const core::Scene& ctx) :
        SafetyController(ctx), params(params)
    {
    }

    SafetyControllerResult
    LaserBasedProximity::control(const core::Twist& twist)
    {
        return {}; // TODO implement
    }
} // namespace armarx::navigation::safe_ctrl
