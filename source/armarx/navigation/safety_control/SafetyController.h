/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <memory>

#include <VirtualRobot/VirtualRobot.h>

#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>

#include "core.h"
#include <armarx/navigation/core/DynamicScene.h>
#include <armarx/navigation/core/StaticScene.h>
#include <armarx/navigation/core/Trajectory.h>
#include <armarx/navigation/core/types.h>

namespace armarx::navigation::safe_ctrl
{
    struct SafetyControllerResult
    {
        core::Twist twist;
    };

    struct SafetyControllerParams
    {
        virtual ~SafetyControllerParams() = default;

        virtual Algorithms algorithm() const = 0;
        virtual aron::data::DictPtr toAron() const = 0;
    };

    class SafetyController
    {
    public:
        SafetyController(const core::Scene& context);
        virtual ~SafetyController() = default;

        virtual SafetyControllerResult control(const core::Twist& twist) = 0;

    protected:
    private:
        const core::Scene& context;
    };

    using SafetyControllerPtr = std::shared_ptr<SafetyController>;
} // namespace armarx::navigation::safe_ctrl
