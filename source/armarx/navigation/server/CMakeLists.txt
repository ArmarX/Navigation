armarx_add_library(server
    SOURCES
        Navigator.cpp
        StackResult.cpp
        GraphBuilder.cpp
        # Executors.
        # ./execution/PlatformUnitExecutor.cpp
        execution/PlatformControllerExecutor.cpp
        # Event publishing.
        event_publishing/MemoryPublisher.cpp
        # Introspection
        introspection/ArvizIntrospector.cpp
        introspection/MemoryIntrospector.cpp
        # monitoring
        monitoring/GoalReachedMonitor.cpp
        # parameterization
        parameterization/MemoryParameterizationService.cpp
        # scene provider
        scene_provider/SceneProvider.cpp
    HEADERS
        Navigator.h
        NavigationStack.h
        StackResult.h
        GraphBuilder.h
        # Executors.
        execution/ExecutorInterface.h
        # ./execution/PlatformUnitExecutor.h
        execution/PlatformControllerExecutor.h
        execution/DummyExecutor.h
        # Event publishing.
        event_publishing/EventPublishingInterface.h
        event_publishing/MemoryPublisher.h
        # Introspection
        introspection/IntrospectorInterface.h
        introspection/ArvizIntrospector.h
        introspection/MemoryIntrospector.h
        # monitoring
        monitoring/GoalReachedMonitor.h
        # parameterization
        parameterization/MemoryParameterizationService.h
        # scene provider
        scene_provider/SceneProviderInterface.h
        scene_provider/SceneProvider.h
    DEPENDENCIES_PUBLIC
        ArmarXCoreInterfaces
        ArmarXCore
        # RobotAPI
        ArViz 
        armem_robot
        armem_robot_state
        # armarx_navigation
        armarx_navigation::core
        armarx_navigation::util
        armarx_navigation::global_planning
        armarx_navigation::local_planning
        armarx_navigation::trajectory_control
        armarx_navigation::safety_control
        armarx_navigation::memory
        armarx_control::client
        armarx_control::interface
    DEPENDENCIES_PRIVATE
        range-v3::range-v3
)

armarx_add_test(server_test
    TEST_FILES
        test/serverTest.cpp
    DEPENDENCIES
        ArmarXCore
        armarx_navigation::client
        armarx_navigation::factories
        armarx_navigation::server
)
