/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <armarx/navigation/global_planning/GlobalPlanner.h>
#include <armarx/navigation/local_planning/LocalPlanner.h>
#include <armarx/navigation/safety_control/SafetyController.h>
#include <armarx/navigation/trajectory_control/TrajectoryController.h>

namespace armarx::navigation::server
{

    struct NavigationStack
    {
        global_planning::GlobalPlannerPtr globalPlanner;
        loc_plan::LocalPlannerPtr localPlanner = nullptr;
        // traj_ctrl::TrajectoryControllerPtr trajectoryControl;
        // safe_ctrl::SafetyControllerPtr safetyControl = nullptr;
    };
} // namespace armarx::navigation::server
