#include "StackResult.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <armarx/navigation/core/Trajectory.h>

namespace armarx::navigation::server
{
    // Stack result
    // core::TrajectoryPtr
    // StackResult::trajectory() const
    // {
    //     if (localTrajectory)
    //     {
    //         return localTrajectory;
    //     }

    //     ARMARX_CHECK_NOT_NULL(globalPlannerResult);
    //     return globalPlannerResult;
    // }

    // core::Twist
    // StackResult::velocity() const
    // {
    //     if (safeVelocity.has_value())
    //     {
    //         return safeVelocity.value();
    //     }

    //     ARMARX_CHECK(controlVelocity.has_value());
    //     return *controlVelocity;
    // }

    // bool
    // StackResult::isValid() const
    // {
    //     return globalPlannerResult != nullptr and controlVelocity.has_value();
    // }

} // namespace armarx::navigation::server
