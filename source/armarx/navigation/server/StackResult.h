/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <optional>

#include <armarx/navigation/core/Trajectory.h>
#include <armarx/navigation/global_planning/GlobalPlanner.h>
#include <armarx/navigation/trajectory_control/TrajectoryController.h>


namespace armarx::navigation::server
{

    struct StackResult
    {
        // TODO make struct, add timestamp
        using LocalPlannerResult = core::TrajectoryPtr;
        using SafetyControllerResult = std::optional<core::Twist>;

        global_planning::GlobalPlannerResult globalPlan;
        LocalPlannerResult localTrajectory;
        traj_ctrl::TrajectoryControllerResult controlVelocity;
        SafetyControllerResult safeVelocity;

        // core::TrajectoryPtr trajectory() const;
        // core::Twist velocity() const;

        /**
         * @brief Simple check if both calls to trajectory() and velocity() can be performed
         *        without throwing an exception.
         *
         * Note: This might not be sufficient, e.g. when a local planner is active but no local
         *       planner has written its result
         *
         */
        // bool isValid() const;
    };
} // namespace armarx::navigation::server
