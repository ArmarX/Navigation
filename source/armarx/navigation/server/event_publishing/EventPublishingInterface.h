#pragma once


#include <armarx/navigation/core/events.h>
#include <armarx/navigation/global_planning/GlobalPlanner.h>
#include <armarx/navigation/local_planning/LocalPlanner.h>
#include <armarx/navigation/trajectory_control/TrajectoryController.h>


namespace armarx::navigation::server
{

    /**
     * @brief A publisher the server navigator will use to notify others about events.
     */
    class EventPublishingInterface
    {

    public:
        // TODO(fabian.reister): fwd
        virtual void globalTrajectoryUpdated(const global_planning::GlobalPlannerResult& res) = 0;
        virtual void localTrajectoryUpdated(const loc_plan::LocalPlannerResult& res) = 0;
        virtual void
        trajectoryControllerUpdated(const traj_ctrl::TrajectoryControllerResult& res) = 0;

        virtual void globalPlanningFailed(const core::GlobalPlanningFailedEvent& event) = 0;


        virtual void movementStarted(const core::MovementStartedEvent& event) = 0;


        /**
         * @brief Will be called whenever the navigator reached the goal.
         */
        virtual void goalReached(const core::GoalReachedEvent& event) = 0;

        /**
         * @brief Will be called whenever the navigator reached a user-defined waypoint.
         */
        virtual void waypointReached(const core::WaypointReachedEvent& event) = 0;

        /**
         * @brief Will be called whenever safety throttling is triggered to a certain degree (configurable).
         */
        virtual void
        safetyThrottlingTriggered(const core::SafetyThrottlingTriggeredEvent& event) = 0;

        /**
         * @brief Will be called whenever a safety stop is triggered.
         */
        virtual void safetyStopTriggered(const core::SafetyStopTriggeredEvent& event) = 0;

        /**
         * @brief Will be called whenever the user aborts the current navigation.
         */
        virtual void userAbortTriggered(const core::UserAbortTriggeredEvent& event) = 0;

        /**
         * @brief Will be called whenever an internal error occurs.
         */
        virtual void internalError(const core::InternalErrorEvent& event) = 0;

        // Non-API.
    public:
        virtual ~EventPublishingInterface() = default;
    };

} // namespace armarx::navigation::server
