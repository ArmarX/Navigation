#include <armarx/navigation/server/event_publishing/MemoryPublisher.h>


namespace armarx::navigation::server
{

    void
    MemoryPublisher::goalReached(const core::GoalReachedEvent& event)
    {
        eventsWriter->store(event, clientId);
    }


    void
    MemoryPublisher::waypointReached(const core::WaypointReachedEvent& event)
    {
        eventsWriter->store(event, clientId);
    }


    void
    MemoryPublisher::safetyThrottlingTriggered(const core::SafetyThrottlingTriggeredEvent& event)
    {
        eventsWriter->store(event, clientId);
    }


    void
    MemoryPublisher::safetyStopTriggered(const core::SafetyStopTriggeredEvent& event)
    {
        eventsWriter->store(event, clientId);
    }


    void
    MemoryPublisher::userAbortTriggered(const core::UserAbortTriggeredEvent& event)
    {
        eventsWriter->store(event, clientId);
    }


    void
    MemoryPublisher::internalError(const core::InternalErrorEvent& event)
    {
        eventsWriter->store(event, clientId);
    }

    void
    MemoryPublisher::globalTrajectoryUpdated(const global_planning::GlobalPlannerResult& res)
    {
        resultWriter->store(res, clientId);
    }

    void
    MemoryPublisher::localTrajectoryUpdated(const loc_plan::LocalPlannerResult& res)
    {
        // resultWriter->store(res, clientId);
    }

    void
    MemoryPublisher::trajectoryControllerUpdated(const traj_ctrl::TrajectoryControllerResult& res)
    {
        resultWriter->store(res, clientId);
    }

    // TODO(fabian.reister): event with message or reason (enum)
    void
    MemoryPublisher::globalPlanningFailed(const core::GlobalPlanningFailedEvent& event)
    {
        eventsWriter->store(event, clientId);
    }

    void
    MemoryPublisher::movementStarted(const core::MovementStartedEvent& event)
    {
        eventsWriter->store(event, clientId);
    }

    MemoryPublisher::MemoryPublisher(
        armarx::navigation::memory::client::stack_result::Writer* resultWriter,
        armarx::navigation::memory::client::events::Writer* eventsWriter,
        const std::string& clientId) :
        resultWriter(resultWriter), eventsWriter(eventsWriter), clientId(clientId)
    {
    }

} // namespace armarx::navigation::server
