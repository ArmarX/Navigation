#pragma once

// STD/STL
#include <chrono>

// Eigen
#include <Eigen/Geometry>

// Simox
#include <VirtualRobot/Robot.h>

// Navigation
#include <armarx/navigation/core/types.h>
#include <armarx/navigation/server/execution/ExecutorInterface.h>

namespace armarx::navigation::server
{

    class DummyExecutor : virtual public ExecutorInterface
    {

    public:
        struct Params
        {
        };

        DummyExecutor(const VirtualRobot::RobotPtr& robot, const Params& params) : robot(robot)
        {
        }

        ~DummyExecutor() override
        {
        }

        void
        execute(const core::Trajectory& trajectory) override
        {
        }

        void start() override{};
        void stop() override{};


    private:
        VirtualRobot::RobotPtr robot;
    };

} // namespace armarx::navigation::server
