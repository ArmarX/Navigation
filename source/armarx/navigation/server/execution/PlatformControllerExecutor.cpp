#include "PlatformControllerExecutor.h"

#include <ArmarXCore/core/PackagePath.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>

#include <armarx/control/client/ComponentPlugin.h>
#include <armarx/control/common/type.h>
#include <armarx/control/memory/config/util.h>
#include <armarx/navigation/common/controller_types.h>
#include <armarx/navigation/core/aron_conversions.h>
#include <armarx/navigation/platform_controller/aron/PlatformTrajectoryControllerConfig.aron.generated.h>
#include <armarx/navigation/platform_controller/aron_conversions.h>
#include <armarx/navigation/platform_controller/controller_descriptions.h>
#include <armarx/navigation/platform_controller/json_conversions.h>


namespace armarx::navigation::server
{

    PlatformControllerExecutor::PlatformControllerExecutor(
        ControllerComponentPlugin& controllerComponentPlugin) :
        controllerPlugin(controllerComponentPlugin)
    {
        ARMARX_TRACE;
        controllerComponentPlugin.getRobotUnitPlugin().getRobotUnit()->loadLibFromPackage(
            "armarx_navigation", "libarmarx_navigation_platform_controller.so");

        // make default configs available to the memory
        ARMARX_INFO << "Loading default configs";
        {
            // const std::filesystem::path configBasePath =
            //     PackagePath("armarx_navigation", "controller_config").toSystemPath();

            ARMARX_TRACE;
            // armarx::control::memory::config::parseAndStoreDefaultConfigs<
            //     armarx::navigation::common::ControllerType::PlatformTrajectory>(
            // "" /*configBasePath*/, controllerComponentPlugin.configMemoryWriter());

            ARMARX_TRACE;
            ARMARX_INFO << "asdlfasfdlh";
        }

        // initialize controller
        ARMARX_INFO << "Initializing controller";
        {
            ARMARX_TRACE;
            auto builder = controllerPlugin.createControllerBuilder<
                armarx::navigation::common::ControllerType::PlatformTrajectory>();

            ARMARX_TRACE;

            const armarx::PackagePath configPath("armarx_navigation", "controller_config/PlatformTrajectory/default.json");

            auto ctrlWrapper =
                builder.withNodeSet("PlatformPlanning")
                    .withConfig(configPath.toSystemPath())
                    .create();

            ARMARX_TRACE;
            ARMARX_CHECK(ctrlWrapper.has_value());
            ctrl.emplace(std::move(ctrlWrapper.value()));
        }

        ARMARX_TRACE;
        ARMARX_CHECK(ctrl.has_value());
        ARMARX_INFO << "PlatformControllerExecutor: init done.";
    }

    PlatformControllerExecutor::~PlatformControllerExecutor() = default;


    void
    PlatformControllerExecutor::execute(const core::Trajectory& trajectory)
    {
        ARMARX_INFO << "Received trajectory for execution with " << trajectory.points().size()
                    << " points";

        toAron(ctrl->config.targets.trajectory, trajectory);

        // sends the updated config to the controller and stores it in the memory
        ctrl->updateConfig();
    }

    void
    PlatformControllerExecutor::start()
    {
        // TODO check whether the controller must be resetted (trajectory)
        ctrl->activate();
    }

    void
    PlatformControllerExecutor::stop()
    {
        ctrl->deactivate();
    }
} // namespace armarx::navigation::server
