#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <armarx/navigation/server/execution/PlatformUnitExecutor.h>

namespace armarx::navigation::server
{

    PlatformUnitExecutor::PlatformUnitExecutor(PlatformUnitInterfacePrx platformUnit) :
        platformUnit{platformUnit}
    {
        // pass
    }

    PlatformUnitExecutor::~PlatformUnitExecutor()
    {
        // pass
    }

    void
    PlatformUnitExecutor::move(const core::Twist& twist)
    {
        ARMARX_CHECK_NOT_NULL(platformUnit) << "PlatformUnit is not available";
        platformUnit->move(twist.linear.x(), twist.linear.y(), twist.angular.z());
    }

} // namespace armarx::navigation::server
