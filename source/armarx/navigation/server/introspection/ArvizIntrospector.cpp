#include "ArvizIntrospector.h"

#include <iterator>
#include <string>

#include <range/v3/view/enumerate.hpp>

#include <Eigen/Geometry>

#include <SimoxUtility/color/Color.h>
#include <SimoxUtility/color/ColorMap.h>
#include <SimoxUtility/color/cmaps/colormaps.h>
#include <VirtualRobot/Robot.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/components/ArViz/Client/Elements.h>
#include <RobotAPI/components/ArViz/Client/elements/Color.h>
#include <RobotAPI/components/ArViz/Client/elements/Path.h>

#include "range/v3/algorithm/max.hpp"
#include "range/v3/algorithm/max_element.hpp"
#include <armarx/navigation/core/basic_types.h>
#include <armarx/navigation/core/types.h>
#include <armarx/navigation/server/StackResult.h>
#include <armarx/navigation/util/Visualization.h>

namespace armarx::navigation::server
{
    ArvizIntrospector::ArvizIntrospector(armarx::viz::Client arviz,
                                         const VirtualRobot::RobotPtr& robot) :
        arviz(arviz),
        robot(robot),
        visualization(arviz,
                      util::Visualization::Params{.robotModelFileName = robot->getFilename()})
    {
    }

    // TODO maybe run with predefined frequency instead of

    void
    ArvizIntrospector::onGlobalPlannerResult(const global_planning::GlobalPlannerResult& result)
    {
        std::lock_guard g{mtx};

        ARMARX_DEBUG << "ArvizIntrospector::onGlobalPlannerResult";

        drawGlobalTrajectory(result.trajectory);
        arviz.commit(layers);


        visualization.visualize(result.trajectory);
    }

    void
    ArvizIntrospector::onLocalPlannerResult(const loc_plan::LocalPlannerResult& result)
    {
        std::lock_guard g{mtx};

        drawLocalTrajectory(result.trajectory);
    }

    // void
    // ArvizIntrospector::onTrajectoryControllerResult(
    //     const traj_ctrl::TrajectoryControllerResult& result)
    // {
    //     std::lock_guard g{mtx};

    //     drawRawVelocity(result.twist);
    // }

    // void
    // ArvizIntrospector::onSafetyControllerResult(const safe_ctrl::SafetyControllerResult& result)
    // {
    //     std::lock_guard g{mtx};

    //     drawSafeVelocity(result.twist);
    // }

    void
    ArvizIntrospector::onGoal(const core::Pose& goal)
    {
        std::lock_guard g{mtx};

        auto layer = arviz.layer("goal");
        layer.add(viz::Pose("goal").pose(goal).scale(3));
        arviz.commit(layer);

        visualization.setTarget(goal);
    }

    void
    ArvizIntrospector::onGlobalShortestPath(const std::vector<core::Pose>& path)
    {
        std::lock_guard g{mtx};

        auto layer = arviz.layer("graph_shortest_path");

        const auto toPosition = [](const core::Pose& pose) -> core::Position
        { return pose.translation(); };


        std::vector<core::Position> pts;
        pts.reserve(path.size());
        std::transform(path.begin(), path.end(), std::back_inserter(pts), toPosition);


        // layer.add(viz::Path("shortest_path").points(pts).color(viz::Color::purple()));

        for (size_t i = 0; i < (pts.size() - 1); i++)
        {
            layer.add(viz::Arrow("segment_" + std::to_string(i))
                          .fromTo(pts.at(i), pts.at(i + 1))
                          .color(viz::Color::purple()));
        }


        arviz.commit(layer);
    }

    // private methods

    void
    ArvizIntrospector::drawGlobalTrajectory(const core::Trajectory& trajectory)
    {
        auto layer = arviz.layer("global_planner");

        layer.add(
            viz::Path("path").points(trajectory.positions()).color(simox::color::Color::blue()));

        const auto cmap = simox::color::cmaps::viridis();
       
       const float maxVelocity = ranges::max_element(trajectory.points(), [](const auto& a, const auto& b){
           return a.velocity < b.velocity;
       })->velocity;


        for (const auto& [idx, tp] : trajectory.points() | ranges::views::enumerate)
        {
            const float scale = tp.velocity;

            const Eigen::Vector3f target =
                scale * tp.waypoint.pose.linear() * Eigen::Vector3f::UnitY();

            layer.add(
                viz::Arrow("velocity_" + std::to_string(idx))
                    .fromTo(tp.waypoint.pose.translation(), tp.waypoint.pose.translation() + target)
                    .color(cmap.at(tp.velocity / maxVelocity)));
        }

        layers.emplace_back(std::move(layer));
    }

    void
    ArvizIntrospector::drawLocalTrajectory(const core::Trajectory& trajectory)
    {
        auto layer = arviz.layer("local_planner");

        layer.add(viz::Path("path").points(trajectory.positions()).color(simox::Color::blue()));

        layers.emplace_back(std::move(layer));
    }

    void
    ArvizIntrospector::drawRawVelocity(const core::Twist& twist)
    {
        auto layer = arviz.layer("trajectory_controller");

        layer.add(viz::Arrow("linear_velocity")
                      .fromTo(robot->getGlobalPosition(),
                              core::Pose(robot->getGlobalPose()) * twist.linear)
                      .color(simox::Color::orange()));

        layers.emplace_back(std::move(layer));
    }

    void
    ArvizIntrospector::drawSafeVelocity(const core::Twist& twist)
    {
        auto layer = arviz.layer("safety_controller");

        layer.add(viz::Arrow("linear_velocity")
                      .fromTo(robot->getGlobalPosition(),
                              core::Pose(robot->getGlobalPose()) * twist.linear)
                      .color(simox::Color::green()));

        layers.emplace_back(std::move(layer));
    }

    ArvizIntrospector::ArvizIntrospector(ArvizIntrospector&& other) noexcept :
        arviz{other.arviz},
        robot{other.robot},
        layers(std::move(other.layers)),
        visualization(std::move(other.visualization))
    {
    }

    ArvizIntrospector&
    ArvizIntrospector::operator=(ArvizIntrospector&&) noexcept
    {
        return *this;
    }

    void
    ArvizIntrospector::onGlobalGraph(const core::Graph& graph)
    {
        auto layer = arviz.layer("global_planning_graph");


        for (const auto& edge : graph.edges())
        {

            const auto sourcePose = graph.vertex(edge.sourceDescriptor()).attrib().getPose();
            const auto targetPose = graph.vertex(edge.targetDescriptor()).attrib().getPose();


            const viz::Color color = [&]()
            {
                if (edge.attrib().cost() > 100'000) // "NaN check"
                {
                    return viz::Color::red();
                }

                return viz::Color::green();
            }();


            layer.add(viz::Arrow(std::to_string(edge.sourceObjectID().t) + " -> " +
                                 std::to_string(edge.targetObjectID().t))
                          .fromTo(sourcePose.translation(), targetPose.translation())
                          .color(color));
        }

        arviz.commit(layer);
    }

    void
    ArvizIntrospector::success()
    {
        visualization.success();
    }
    void
    ArvizIntrospector::failure()
    {
        visualization.failed();
    }


} // namespace armarx::navigation::server
