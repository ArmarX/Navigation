/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <VirtualRobot/VirtualRobot.h>

#include <RobotAPI/components/ArViz/Client/Layer.h>
#include <RobotAPI/components/ArViz/Client/ScopedClient.h>

#include "IntrospectorInterface.h"
#include <armarx/navigation/core/types.h>
#include <armarx/navigation/global_planning/GlobalPlanner.h>
#include <armarx/navigation/local_planning/LocalPlanner.h>
#include <armarx/navigation/safety_control/SafetyController.h>
#include <armarx/navigation/trajectory_control/TrajectoryController.h>
#include <armarx/navigation/util/Visualization.h>

// forward declaration
namespace armarx::navigation::core
{
    struct Trajectory;
}

namespace armarx::navigation::server
{

    class ArvizIntrospector : virtual public IntrospectorInterface
    {
    public:
        ArvizIntrospector(armarx::viz::Client arviz, const VirtualRobot::RobotPtr& robot);
        ~ArvizIntrospector() override = default;

        void onGlobalPlannerResult(const global_planning::GlobalPlannerResult& result) override;
        void onLocalPlannerResult(const loc_plan::LocalPlannerResult& result) override;
        
        void onGoal(const core::Pose& goal) override;

        void success();
        void failure();

        void onGlobalShortestPath(const std::vector<core::Pose>& path) override;

        void onGlobalGraph(const core::Graph& graph) override;


        // // Non-API
        ArvizIntrospector(ArvizIntrospector&& other) noexcept;
        ArvizIntrospector& operator=(ArvizIntrospector&&) noexcept;

    private:
        void drawGlobalTrajectory(const core::Trajectory& trajectory);
        void drawLocalTrajectory(const core::Trajectory& trajectory);
        void drawRawVelocity(const core::Twist& twist);
        void drawSafeVelocity(const core::Twist& twist);

        viz::ScopedClient arviz;
        const VirtualRobot::RobotPtr robot;

        std::vector<viz::Layer> layers;

        std::mutex mtx;

        util::Visualization visualization;
    };

} // namespace armarx::navigation::server
