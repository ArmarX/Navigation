#include "MemoryIntrospector.h"

#include <armarx/navigation/memory/client/stack_result/Writer.h>

namespace armarx::navigation::server
{
    MemoryIntrospector::MemoryIntrospector(memory::client::stack_result::Writer& globPlanWriter,
                                           const std::string& clientID) :
        globPlanWriter(globPlanWriter), clientID(clientID)
    {
    }

    void
    MemoryIntrospector::onGlobalPlannerResult(const global_planning::GlobalPlannerResult& result)
    {
        globPlanWriter.store(result, clientID);
    }

    void
    MemoryIntrospector::onLocalPlannerResult(const loc_plan::LocalPlannerResult& result)
    {
        // TODO(fabian.reister): implement
    }


    void
    MemoryIntrospector::onGoal(const core::Pose& goal)
    {
        // TODO(fabian.reister): implement
    }

    // void
    // MemoryIntrospector::onStackResult(const StackResult& result)
    // {
    //     globPlanWriter.store(result, clientID);
    //     // TODO(fabian.reister): add more
    // }
} // namespace armarx::navigation::server
