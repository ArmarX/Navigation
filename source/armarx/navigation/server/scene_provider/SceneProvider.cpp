#include "SceneProvider.h"

#include <VirtualRobot/SceneObjectSet.h>
#include "ArmarXCore/core/exceptions/local/ExpressionException.h"

#include <armarx/navigation/algorithms/CostmapBuilder.h>
#include <armarx/navigation/util/util.h>

namespace armarx::navigation::server::scene_provider
{

    SceneProvider::SceneProvider(const InjectedServices& srv, const Config& config) :
        srv(srv), config(config)
    {
    }

    const core::Scene&
    SceneProvider::scene() const
    {
        return scn;
    }

    bool
    SceneProvider::initialize(const DateTime& timestamp)
    {
        scn.timestamp = timestamp;

        scn.robot = srv.virtualRobotReader->getRobot(
            config.robotName, timestamp, VirtualRobot::RobotIO::RobotDescription::eCollisionModel);
        ARMARX_CHECK_NOT_NULL(scn.robot);

        scn.staticScene = getStaticScene(timestamp);
        scn.dynamicScene = getDynamicScene(timestamp);
        scn.graph = getSceneGraph(timestamp);

        return true; // TODO(fabian.reister): return false if sync fails
    }

    bool
    SceneProvider::synchronize(const DateTime& timestamp)
    {
        scn.timestamp = timestamp;

        ARMARX_CHECK_NOT_NULL(srv.virtualRobotReader);
        srv.virtualRobotReader->synchronizeRobot(*scn.robot, timestamp);

        scn.dynamicScene = getDynamicScene(timestamp);
        // scn.graph = getSceneGraph(timestamp);

        return true; // TODO(fabian.reister): return false if sync fails
    }

    core::StaticScene
    SceneProvider::getStaticScene(const DateTime& timestamp) const
    {
        ARMARX_TRACE;

        const objpose::ObjectPoseSeq objectPoses = srv.objectPoseClient.fetchObjectPoses();

        // remove those objects that belong to an object dataset. the manipulation object / distance computation is broken
        const auto objectPosesStatic =
            armarx::navigation::util::filterObjects(objectPoses, {"KIT", "HOPE", "MDB", "YCB"});

        const auto objects = armarx::navigation::util::asSceneObjects(objectPosesStatic);

        ARMARX_CHECK_NOT_NULL(objects);
        ARMARX_INFO << objects->getSize() << " objects in the scene";

        ARMARX_INFO << "Creating costmap";
        ARMARX_CHECK_NOT_NULL(scn.robot);

        // FIXME: move costmap creation out of this component
        // FIXME create costmap writer enum: type of costmaps
        algorithms::CostmapBuilder costmapBuilder(
            scn.robot,
            objects,
            algorithms::Costmap::Parameters{.binaryGrid = false, .cellSize = 100},
            "Platform-navigation-colmodel");

        const auto costmap = costmapBuilder.create();

        // ARMARX_INFO << "Storing costmap in memory";
        // costmapWriterPlugin->get().store(
        //     costmap, "distance_to_obstacles", getName(), armarx::Clock::Now());

        ARMARX_INFO << "Done";

        ARMARX_TRACE;

        return {.objects = objects, .costmap = std::make_unique<algorithms::Costmap>(costmap)};
    }

    core::DynamicScene
    SceneProvider::getDynamicScene(const DateTime& timestamp) const
    {
        return {}; // FIXME implement
    }

    core::SceneGraph
    SceneProvider::getSceneGraph(const DateTime& /*timestamp*/) const
    {
        ARMARX_CHECK_NOT_NULL(srv.graphReader);
        return {.subgraphs = srv.graphReader->graphs()};
    }

} // namespace armarx::navigation::server::scene_provider
