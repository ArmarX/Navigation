/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/VirtualRobot.h>

#include <ArmarXCore/core/time/DateTime.h>

#include <RobotAPI/libraries/ArmarXObjects/ObjectPoseClient.h>
#include <RobotAPI/libraries/armem_robot/types.h>
#include <RobotAPI/libraries/armem_robot_state/client/common/VirtualRobotReader.h>

#include <armarx/navigation/core/DynamicScene.h>
#include <armarx/navigation/core/StaticScene.h>
#include <armarx/navigation/memory/client/costmap/Reader.h>
#include <armarx/navigation/memory/client/graph/Reader.h>
#include <armarx/navigation/core/types.h>
#include <armarx/navigation/server/scene_provider/SceneProviderInterface.h>


namespace armarx::navigation::server::scene_provider
{

    class SceneProvider : virtual public SceneProviderInterface
    {
    public:
        struct InjectedServices
        {

            memory::client::graph::Reader* graphReader;

            memory::client::costmap::Reader* costmapReader;
            // armem::vision::occupancy_grid::client::Reader occupancyGridReader;

            // `robot_state` memory reader and writer
            armem::robot_state::VirtualRobotReader* virtualRobotReader;

            objpose::ObjectPoseClient objectPoseClient;
        };

        struct Config
        {
            std::string robotName;
        };

        SceneProvider(const InjectedServices& srv, const Config& config);

        const core::Scene& scene() const override;

        bool initialize(const DateTime& timestamp) override;
        bool synchronize(const DateTime& timestamp) override;


    private:
        VirtualRobot::RobotPtr getRobot(const DateTime& timestamp) const;
        core::StaticScene getStaticScene(const DateTime& timestamp) const;
        core::DynamicScene getDynamicScene(const DateTime& timestamp) const;
        core::SceneGraph getSceneGraph(const DateTime& timestamp) const;

        InjectedServices srv;
        Config config;

        core::Scene scn;
    };
} // namespace armarx::navigation::server::scene_provider
