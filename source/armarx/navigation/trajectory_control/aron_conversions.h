/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

namespace armarx::navigation::traj_ctrl
{
    struct PIDParams;

    struct TrajectoryControllerParams;
    struct TrajectoryFollowingControllerParams;
    struct WaypointControllerParams;

    namespace arondto
    {
        class PIDParams;

        class TrajectoryControllerParams;
        class TrajectoryFollowingControllerParams;
        class WaypointControllerParams;
    } // namespace arondto


    void toAron(arondto::TrajectoryControllerParams& dto, const TrajectoryControllerParams& bo);
    void fromAron(const arondto::TrajectoryControllerParams& dto, TrajectoryControllerParams& bo);

    void toAron(arondto::TrajectoryFollowingControllerParams& dto,
                const TrajectoryFollowingControllerParams& bo);
    void fromAron(const arondto::TrajectoryFollowingControllerParams& dto,
                  TrajectoryFollowingControllerParams& bo);

    void toAron(arondto::WaypointControllerParams& dto, const WaypointControllerParams& bo);
    void fromAron(const arondto::WaypointControllerParams& dto, WaypointControllerParams& bo);

}  // namespace armarx::navigation::traj_ctrl
