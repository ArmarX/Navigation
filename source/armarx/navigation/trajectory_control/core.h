/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <SimoxUtility/meta/enum/EnumNames.hpp>

namespace armarx::navigation::traj_ctrl
{

    enum class Algorithms
    {
        WaypointController,
        TrajectoryFollowingController
    };

    const inline simox::meta::EnumNames<Algorithms> AlgorithmNames{
        {Algorithms::WaypointController, "WaypointController"},
        {Algorithms::TrajectoryFollowingController, "TrajectoryFollowingController"}};

} // namespace armarx::navigation::traj_ctrl
