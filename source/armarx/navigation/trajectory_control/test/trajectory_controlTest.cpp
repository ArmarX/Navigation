/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Navigation::ArmarXObjects::trajectory_control
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <limits>
#include <thread>

#include <boost/test/tools/old/interface.hpp>

#include <Eigen/Geometry>

#include <SimoxUtility/math/convert/deg_to_rad.h>
#include <VirtualRobot/Robot.h>

#include <armarx/navigation/core/Trajectory.h>
#define BOOST_TEST_MODULE Navigation::ArmarXLibraries::trajectory_control

#define ARMARX_BOOST_TEST

#include <iostream>

#include "../TrajectoryFollowingController.h"
#include <armarx/navigation/Test.h>
#include <armarx/navigation/client/NavigationStackConfig.h>
#include <armarx/navigation/core/types.h>
#include <armarx/navigation/trajectory_control/TrajectoryFollowingController.h>

using namespace armarx::navigation;

BOOST_AUTO_TEST_CASE(testTrajectoryFollowingControllerOnTrajectory)
{
    core::Scene scene;
    scene.robot = std::make_shared<VirtualRobot::LocalRobot>("foo", "bar");
    scene.robot->setGlobalPose(Eigen::Matrix4f::Identity(), false);

    traj_ctrl::TrajectoryFollowingControllerParams params;
    params.pidPos.Kp = 1;
    params.pidPos.Ki = 0;
    params.pidPos.Kd = 0;

    params.pidOri.Kp = 1;
    params.pidOri.Ki = 0;
    params.pidOri.Kd = 0;

    params.limits.linear = std::numeric_limits<float>::max();
    params.limits.angular = std::numeric_limits<float>::max();

    traj_ctrl::TrajectoryFollowingController controller(scene.robot, params);

    constexpr float ffVelocity = 100.0; // ground truth

    core::TrajectoryPoint tpStart;
    tpStart.waypoint.pose = core::Pose::Identity();
    tpStart.waypoint.pose.translation().x() -= 100.F;
    tpStart.velocity = ffVelocity;

    core::TrajectoryPoint waypoint = tpStart;
    waypoint.waypoint.pose.translation().x() += 1000.F;

    core::TrajectoryPoint tpGoal = tpStart;
    tpGoal.waypoint.pose.translation().x() += 2000.F;

    const core::Trajectory traj({tpStart, tpGoal});

    auto res = controller.control(traj);

    ARMARX_DEBUG << "Control val " << res.twist.linear;

    // only feed-forward velocity needs to be considered
    BOOST_CHECK_EQUAL(res.twist.linear.x(), ffVelocity);
}

/**
 * @brief Tests the control
 *
 */
BOOST_AUTO_TEST_CASE(testTrajectoryFollowingControllerNextToTrajectory)
{
    core::Scene scene;
    scene.robot = std::make_shared<VirtualRobot::LocalRobot>("foo", "bar");
    scene.robot->setGlobalPose(
        Eigen::Matrix4f::Identity() * core::Pose(Eigen::Translation3f(0, 100, 0)).matrix(), false);

    traj_ctrl::TrajectoryFollowingControllerParams params;
    params.pidPos = {1, 0, 0};
    params.pidOri = {1, 0, 0};
    params.limits.linear = std::numeric_limits<float>::max();
    params.limits.angular = std::numeric_limits<float>::max();

    traj_ctrl::TrajectoryFollowingController controller(scene.robot, params);

    constexpr float ffVelocity = 100.0; // ground truth


    core::TrajectoryPoint tpStart;
    tpStart.waypoint.pose = core::Pose::Identity();
    tpStart.waypoint.pose.translation().x() -= 100.F;
    tpStart.velocity = ffVelocity;

    core::TrajectoryPoint waypoint = tpStart;
    waypoint.waypoint.pose.translation().x() += 1000.F;

    core::TrajectoryPoint wpAfter = tpStart;
    wpAfter.waypoint.pose.translation().x() += 2000.F;

    core::Trajectory traj({tpStart, waypoint, wpAfter});

    const auto res = controller.control(traj);

    ARMARX_DEBUG << "Control val " << res.twist.linear;

    // only feed-forward velocity needs to be considered
    BOOST_CHECK_EQUAL(res.twist.linear.x(), ffVelocity);
    BOOST_CHECK_LT(res.twist.linear.y(), -0.1); // negative control signal to approach trajectory
}

class TargetReachedCondition
{
public:
    struct Params
    {
        struct
        {
            float linear;
            float angular;
        } distance;
    };

    TargetReachedCondition(const core::TrajectoryPoint& point, const Params& params) :
        point(point), params(params)
    {
    }

    bool
    check(const core::Pose& pose) const
    {
        const float dp = (pose.translation() - point.waypoint.pose.translation()).norm();
        const float dr =
            Eigen::AngleAxisf(pose.linear().inverse() * point.waypoint.pose.linear()).angle();

        return (dp <= params.distance.linear) and (dr <= params.distance.angular);
    }

private:
    core::TrajectoryPoint point;
    const Params params;
};

class TimeAwareRobot : virtual public VirtualRobot::LocalRobot
{
};

/**
 * @brief Tests the control
 *
 */
BOOST_AUTO_TEST_CASE(testTrajectoryFollowingControllerReachGoal)
{
    return; // This test runs too long. TODO: use time provider!

    core::Scene scene;
    scene.robot = std::make_shared<VirtualRobot::LocalRobot>("foo", "bar");
    scene.robot->setGlobalPose(
        Eigen::Matrix4f::Identity() * core::Pose(Eigen::Translation3f(0, 100, 0)).matrix(), false);

    traj_ctrl::TrajectoryFollowingControllerParams params;
    params.pidPos = {1, 0, 0};
    params.pidOri = {1, 0, 0};
    params.limits.linear = std::numeric_limits<float>::max();
    params.limits.angular = std::numeric_limits<float>::max();

    traj_ctrl::TrajectoryFollowingController controller(scene.robot, params);

    core::TrajectoryPoint tpStart;
    tpStart.waypoint.pose = core::Pose::Identity();
    // tpStart.waypoint.pose.translation().x() -= 100.F;
    tpStart.velocity = 100.0;

    core::TrajectoryPoint wpAfter = tpStart;
    wpAfter.waypoint.pose.translation().x() += 2000.F;

    const core::Trajectory traj({tpStart, wpAfter});

    auto timestamp = std::chrono::steady_clock::now();

    TargetReachedCondition condition(
        wpAfter,
        TargetReachedCondition::Params{
            .distance{.linear = 50.F, .angular = simox::math::deg_to_rad(10.F)}});

    const auto isFinite = [](const auto& x)
    {
        return ((x - x).array() == (x - x).array()).all();
        // return (x.array() != 0.F).any();
    };

    for (int i = 0; i < 10'000; i++)
    {
        const auto res = controller.control(traj);
        BOOST_CHECK(isFinite(res.twist.linear));
        BOOST_CHECK(isFinite(res.twist.angular));

        if (not isFinite(res.twist.linear) or not isFinite(res.twist.angular))
        {
            return;
        }

        ARMARX_INFO << "Control val " << res.twist.linear;

        auto now = std::chrono::steady_clock::now();

        const float dt = std::chrono::duration<float>(now - timestamp).count();

        core::Pose dp = res.twist.poseDiff(dt);

        const core::Pose newPose = core::Pose(scene.robot->getGlobalPose()) * dp;
        scene.robot->setGlobalPose(newPose.matrix(), false);

        if (condition.check(newPose))
        {
            break;
        }

        ARMARX_INFO << "new pos " << newPose.translation();

        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    BOOST_CHECK(condition.check(core::Pose(scene.robot->getGlobalPose())));
}


// TODO this should be part of the TrajectoryController base class
BOOST_AUTO_TEST_CASE(testTrajectoryFollowingControllerApplyLimitsExceedLinear)
{

    core::Scene scene;
    scene.robot = std::make_shared<VirtualRobot::LocalRobot>("foo", "bar");
    scene.robot->setGlobalPose(Eigen::Matrix4f::Identity(), false);

    traj_ctrl::TrajectoryFollowingControllerParams params;
    params.pidPos.Kp = 1;
    params.pidPos.Ki = 0;
    params.pidPos.Kd = 0;

    params.pidOri.Kp = 1;
    params.pidOri.Ki = 0;
    params.pidOri.Kd = 0;

    params.limits.linear = 500.F;                 // [mm/s]
    params.limits.angular = 2.F * M_PIf32 / 10.F; // [rad/s]

    traj_ctrl::TrajectoryFollowingController controller(scene.robot, params);

    const core::Twist unclippedTwist{
        .linear = 600 * Eigen::Vector3f::UnitX(), // 600 vs 500
        .angular = 0.1 * Eigen::Vector3f::UnitZ() // below limit
    };

    const core::Twist clippedTwist = controller.applyTwistLimits(unclippedTwist);

    ARMARX_DEBUG << "Clipped twist linear " << clippedTwist.linear;
    ARMARX_DEBUG << "Clipped twist angular " << clippedTwist.angular;

    // check that velocity is clipped correctly
    BOOST_CHECK_LE(std::abs(clippedTwist.linear.x()), params.limits.linear);
    BOOST_CHECK_LE(std::abs(clippedTwist.linear.y()), params.limits.linear);
    BOOST_CHECK_LE(std::abs(clippedTwist.linear.z()), params.limits.linear);

    BOOST_CHECK_LE(std::abs(clippedTwist.angular.x()), params.limits.angular);
    BOOST_CHECK_LE(std::abs(clippedTwist.angular.y()), params.limits.angular);
    BOOST_CHECK_LE(std::abs(clippedTwist.angular.z()), params.limits.angular);

    ARMARX_DEBUG << std::abs(clippedTwist.angular.z());
    ARMARX_DEBUG << "GT " << params.limits.angular;
}


// TODO this should be part of the TrajectoryController base class
BOOST_AUTO_TEST_CASE(testTrajectoryFollowingControllerApplyLimitsExceedLinear2)
{

    core::Scene scene;
    scene.robot = std::make_shared<VirtualRobot::LocalRobot>("foo", "bar");
    scene.robot->setGlobalPose(Eigen::Matrix4f::Identity(), false);

    traj_ctrl::TrajectoryFollowingControllerParams params;
    params.pidPos.Kp = 1;
    params.pidPos.Ki = 0;
    params.pidPos.Kd = 0;

    params.pidOri.Kp = 1;
    params.pidOri.Ki = 0;
    params.pidOri.Kd = 0;

    params.limits.linear = 500.F;                 // [mm/s]
    params.limits.angular = 2.F * M_PIf32 / 10.F; // [rad/s]

    traj_ctrl::TrajectoryFollowingController controller(scene.robot, params);

    const core::Twist unclippedTwist{
        .linear = 600 * Eigen::Vector3f::Ones(),  // 600 vs 500
        .angular = 0.1 * Eigen::Vector3f::UnitZ() // below limit
    };

    const core::Twist clippedTwist = controller.applyTwistLimits(unclippedTwist);

    ARMARX_DEBUG << "Clipped twist linear " << clippedTwist.linear;
    ARMARX_DEBUG << "Clipped twist angular " << clippedTwist.angular;

    // check that velocity is clipped correctly
    BOOST_CHECK_LE(std::abs(clippedTwist.linear.x()), params.limits.linear);
    BOOST_CHECK_LE(std::abs(clippedTwist.linear.y()), params.limits.linear);
    BOOST_CHECK_LE(std::abs(clippedTwist.linear.z()), params.limits.linear);

    BOOST_CHECK_LE(std::abs(clippedTwist.angular.x()), params.limits.angular);
    BOOST_CHECK_LE(std::abs(clippedTwist.angular.y()), params.limits.angular);
    BOOST_CHECK_LE(std::abs(clippedTwist.angular.z()), params.limits.angular);

    ARMARX_DEBUG << std::abs(clippedTwist.angular.z());
    ARMARX_DEBUG << "GT" << params.limits.angular;
}

// TODO this should be part of the TrajectoryController base class
BOOST_AUTO_TEST_CASE(testTrajectoryFollowingControllerApplyLimitsExceedAngular)
{

    core::Scene scene;
    scene.robot = std::make_shared<VirtualRobot::LocalRobot>("foo", "bar");
    scene.robot->setGlobalPose(Eigen::Matrix4f::Identity(), false);

    traj_ctrl::TrajectoryFollowingControllerParams params;
    params.pidPos.Kp = 1;
    params.pidPos.Ki = 0;
    params.pidPos.Kd = 0;

    params.pidOri.Kp = 1;
    params.pidOri.Ki = 0;
    params.pidOri.Kd = 0;

    params.limits.linear = 500.F;                 // [mm/s]
    params.limits.angular = 2.F * M_PIf32 / 10.F; // [rad/s]

    traj_ctrl::TrajectoryFollowingController controller(scene.robot, params);

    const core::Twist unclippedTwist{
        .linear = 400 * Eigen::Vector3f::UnitX(),           // below limit
        .angular = 2.F * M_PIf32 * Eigen::Vector3f::UnitZ() // 10 times the limit
    };

    const core::Twist clippedTwist = controller.applyTwistLimits(unclippedTwist);

    ARMARX_DEBUG << "Clipped twist linear " << clippedTwist.linear;
    ARMARX_DEBUG << "Clipped twist angular " << clippedTwist.angular;

    // check that velocity is clipped correctly
    BOOST_CHECK_LE(std::abs(clippedTwist.linear.x()), params.limits.linear);
    BOOST_CHECK_LE(std::abs(clippedTwist.linear.y()), params.limits.linear);
    BOOST_CHECK_LE(std::abs(clippedTwist.linear.z()), params.limits.linear);

    BOOST_CHECK_LE(std::abs(clippedTwist.angular.x()), params.limits.angular);
    BOOST_CHECK_LE(std::abs(clippedTwist.angular.y()), params.limits.angular);
    BOOST_CHECK_LE(std::abs(clippedTwist.angular.z()), params.limits.angular);

    ARMARX_DEBUG << std::abs(clippedTwist.angular.z());
    ARMARX_DEBUG << "GT" << params.limits.angular;
}


// TODO this should be part of the TrajectoryController base class
BOOST_AUTO_TEST_CASE(testTrajectoryFollowingControllerApplyLimitsExceedBoth)
{

    core::Scene scene;
    scene.robot = std::make_shared<VirtualRobot::LocalRobot>("foo", "bar");
    scene.robot->setGlobalPose(Eigen::Matrix4f::Identity(), false);

    traj_ctrl::TrajectoryFollowingControllerParams params;
    params.pidPos.Kp = 1;
    params.pidPos.Ki = 0;
    params.pidPos.Kd = 0;

    params.pidOri.Kp = 1;
    params.pidOri.Ki = 0;
    params.pidOri.Kd = 0;

    params.limits.linear = 500.F;                 // [mm/s]
    params.limits.angular = 2.F * M_PIf32 / 10.F; // [rad/s]

    traj_ctrl::TrajectoryFollowingController controller(scene.robot, params);

    const core::Twist unclippedTwist{
        .linear = 600 * Eigen::Vector3f::UnitX(),           // 600 vs 500
        .angular = 2.F * M_PIf32 * Eigen::Vector3f::UnitZ() // 10 times the limit
    };

    const core::Twist clippedTwist = controller.applyTwistLimits(unclippedTwist);

    ARMARX_DEBUG << "Clipped twist linear " << clippedTwist.linear;
    ARMARX_DEBUG << "Clipped twist angular " << clippedTwist.angular;

    // check that velocity is clipped correctly
    BOOST_CHECK_LE(std::abs(clippedTwist.linear.x()), params.limits.linear);
    BOOST_CHECK_LE(std::abs(clippedTwist.linear.y()), params.limits.linear);
    BOOST_CHECK_LE(std::abs(clippedTwist.linear.z()), params.limits.linear);

    BOOST_CHECK_LE(std::abs(clippedTwist.angular.x()), params.limits.angular);
    BOOST_CHECK_LE(std::abs(clippedTwist.angular.y()), params.limits.angular);
    BOOST_CHECK_LE(std::abs(clippedTwist.angular.z()), params.limits.angular);

    ARMARX_DEBUG << std::abs(clippedTwist.angular.z());
    ARMARX_DEBUG << "GT " << params.limits.angular;
}
