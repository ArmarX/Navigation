#pragma once

#include <atomic>
#include <mutex>

#include <Eigen/Core>

#include <RobotAPI/components/ArViz/Client/Client.h>
#include <RobotAPI/components/ArViz/Client/ScopedClient.h>

#include <armarx/navigation/core/Trajectory.h>
#include <armarx/navigation/core/basic_types.h>

namespace armarx::navigation::util
{

    class Visualization
    {

    public:
        struct Params
        {
            std::string robotModelFileName;

            bool enableVisualization = true;
            bool enableTrajectoryVisualization = false;

            int hideDelaySeconds = 2; // [s]

            int alphaValue = 30;
        };

        Visualization(armarx::viz::Client& arviz, const Params& params);

        Visualization(Visualization&& other) noexcept :
            params(std::move(other.params)), arviz(other.arviz), robotVis(other.robotVis)
        {
        }


        void setTarget(const core::Pose& m);

        void success();
        void failed();


        void visualize(const core::Trajectory& trajectory);

        void
        updateParams(const Params& params)
        {
            std::lock_guard g{paramsMutex};
            this->params = params;
        }

    private:
        void visualizeTrajectory(const core::Trajectory& trajectory);

        void asyncHideDelayed();

        std::atomic<std::int64_t> lastUpdate = 0;

        // Params
        std::mutex paramsMutex;
        Params params;

        // ArViz
        std::mutex mutex;
        armarx::viz::ScopedClient arviz;

        viz::Robot robotVis{""};
    };

} // namespace armarx::navigation::util
