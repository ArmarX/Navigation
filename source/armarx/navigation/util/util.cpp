/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Navigation::ArmarXObjects::util
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Christian R. G. Dreher ( c dot dreher at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "util.h"

#include <string>

#include <Eigen/Geometry>

#include <VirtualRobot/CollisionDetection/CollisionModel.h>
#include <VirtualRobot/ManipulationObject.h>
#include <VirtualRobot/Primitive.h>
#include <VirtualRobot/SceneObjectSet.h>
#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>
#include <VirtualRobot/Visualization/VisualizationNode.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <RobotAPI/libraries/ArmarXObjects/ObjectFinder.h>
#include <RobotAPI/libraries/ArmarXObjects/forward_declarations.h>
#include <RobotAPI/libraries/armem_vision/OccupancyGridHelper.h>
#include <RobotAPI/libraries/core/FramedPose.h>

namespace armarx::navigation::util
{

    objpose::ObjectPoseSeq
    filterObjects(objpose::ObjectPoseSeq objects, const std::vector<std::string>& datasetBlacklist)
    {
        const auto isBlacklisted = [&datasetBlacklist](const objpose::ObjectPose& objectPose)
        {
            const auto dataset = objectPose.objectID.dataset();

            return std::find(datasetBlacklist.begin(), datasetBlacklist.end(), dataset) !=
                   datasetBlacklist.end();
        };

        objects.erase(std::remove_if(objects.begin(), objects.end(), isBlacklisted), objects.end());
        return objects;
    }


    std::optional<objpose::ObjectPose>
    findObject(const objpose::ObjectPoseSeq& objectPoses, const armarx::ObjectID& objectID)
    {
        const auto matchesId = [&objectID](const objpose::ObjectPose& objectPose) -> bool
        { return objectPose.objectID == objectID; };

        const auto it = std::find_if(objectPoses.begin(), objectPoses.end(), matchesId);
        if (it != objectPoses.end())
        {
            return *it;
        }

        return std::nullopt;
    }

    VirtualRobot::ManipulationObjectPtr
    asManipulationObject(const objpose::ObjectPose& objectPose)
    {
        ObjectFinder finder;

        VirtualRobot::SceneObjectSetPtr sceneObjects(new VirtualRobot::SceneObjectSet);
        if (auto obstacle = finder.loadManipulationObject(objectPose))
        {
            obstacle->setGlobalPose(objectPose.objectPoseGlobal);
            return obstacle;
        }

        ARMARX_WARNING << "Failed to load scene object `" << objectPose.objectID << "`";
        return nullptr;
    }


    VirtualRobot::SceneObjectSetPtr
    asSceneObjects(const objpose::ObjectPoseSeq& objectPoses)
    {
        ObjectFinder finder;

        VirtualRobot::SceneObjectSetPtr sceneObjects(new VirtualRobot::SceneObjectSet);
        for (const auto& objectPose : objectPoses)
        {
            if (auto obstacle = finder.loadManipulationObject(objectPose))
            {
                obstacle->setGlobalPose(objectPose.objectPoseGlobal);
                sceneObjects->addSceneObject(obstacle);
            }
        }

        return sceneObjects;
    }

    VirtualRobot::SceneObjectSetPtr
    asSceneObjects(const armem::vision::OccupancyGrid& occupancyGrid,
                   const OccupancyGridHelper::Params& params)
    {
        const OccupancyGridHelper ocHelper(occupancyGrid, params);
        const auto obstacles = ocHelper.obstacles();

        const float boxSize = occupancyGrid.resolution;
        const float resolution = occupancyGrid.resolution;

        VirtualRobot::SceneObjectSetPtr sceneObjects(new VirtualRobot::SceneObjectSet);

        ARMARX_CHECK_EQUAL(occupancyGrid.frame, GlobalFrame)
            << "Only occupancy grid in global frame supported.";

        VirtualRobot::CoinVisualizationFactory factory;

        const auto& world_T_map = occupancyGrid.pose;

        for (int x = 0; x < obstacles.rows(); x++)
        {
            for (int y = 0; y < obstacles.cols(); y++)
            {
                if (obstacles(x, y))
                {
                    const Eigen::Vector3f pos{
                        static_cast<float>(x * resolution), static_cast<float>(y * resolution), 0};

                    // FIXME: change to Isometry3f
                    Eigen::Affine3f map_T_obj = Eigen::Affine3f::Identity();
                    map_T_obj.translation() = pos;

                    Eigen::Affine3f world_T_obj = world_T_map * map_T_obj;

                    // ARMARX_INFO << world_T_obj.translation();

                    auto cube = factory.createBox(boxSize, boxSize, boxSize);

                    VirtualRobot::CollisionModelPtr collisionModel(
                        new VirtualRobot::CollisionModel(cube));

                    VirtualRobot::SceneObjectPtr sceneObject(new VirtualRobot::SceneObject(
                        "box_" + std::to_string(sceneObjects->getSize()), cube, collisionModel));
                    sceneObject->setGlobalPose(world_T_obj.matrix());

                    sceneObjects->addSceneObject(sceneObject);
                }
            }
        }

        return sceneObjects;
    }
} // namespace armarx::navigation::util
